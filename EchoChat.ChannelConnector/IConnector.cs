﻿using EchoChat.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.ChannelConnector
{
    public interface IConnector
    {
        ConcurrentBag<ChannelOutput> CallBroadcaster(MessageDetails messageDetails, List<string> channels, List<UserChannelData> userChannelDatas, List<AppSettingsVM> settings, List<WhatsAppParameterData> whatsAppParameters = null);
    }
}
