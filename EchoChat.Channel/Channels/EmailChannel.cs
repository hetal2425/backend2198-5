﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Amazon;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using EchoChat.Channel.Factory;
using EchoChat.Constants;
using EchoChat.Helper;
using EchoChat.Models;
using log4net;
using MimeKit;

namespace EchoChat.Channel
{
    public class EmailChannel : BaseChannel
    {
        private ILog logger = LogManager.GetLogger(typeof(EmailChannel));

        public override Task Process()
        {
            try
            {
                List<UserChannelData> userChannelDatas = Essentials.userChannelDatas.Where(u => !string.IsNullOrWhiteSpace(u.EmailID)).Select(u => u).Distinct().ToList();

                string configSet = Essentials.settings.Where(a => a.Key.Equals(SettingMaster.SESConfigSet)).Select(a => a.Value).FirstOrDefault();

                if (userChannelDatas != null && userChannelDatas.Count > 0)
                {
                    string subject = Essentials.messageDetails.Title;


                    foreach (UserChannelData ucd in userChannelDatas)
                    {
                        if (!string.IsNullOrEmpty(ucd.EmailID))
                        {
                            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                            Match match = regex.Match(ucd.EmailID);
                            if (match.Success)
                            {
                                var emailBody = BuildEmailBody(!string.IsNullOrWhiteSpace(Essentials.messageDetails.CustomDetails_2) && Essentials.messageDetails.CustomDetails_2.Equals("Notice") ? BuildEmailContent() : Essentials.messageDetails.HTMLMessage, true);
                                if (Essentials.messageDetails.Files != null && Essentials.messageDetails.Files.Count > 0)
                                {
                                    foreach (EchoFile echoFile in Essentials.messageDetails.Files)
                                    {
                                        emailBody.Attachments.Add(echoFile.FileName, GetFile(echoFile.FileName));
                                    }
                                }
                                var emailMessage = BuildEmailHeaders(Essentials.messageDetails.SenderDetails, ucd.EmailID, subject);
                                if (!string.IsNullOrEmpty(ucd.DisplayName))
                                {
                                    emailBody.HtmlBody = emailBody.HtmlBody.Replace(@"{UserName}", ucd.DisplayName);
                                }
                                else if (!string.IsNullOrEmpty(ucd.UserName))
                                {
                                    emailBody.HtmlBody = emailBody.HtmlBody.Replace(@"{UserName}", ucd.UserName);
                                }
                                emailMessage.Body = emailBody.ToMessageBody();
                                //DelayTesting(ChannelMaster.EmailChannel, ucd.UserID);
                                Send(ucd.UserID, emailMessage, configSet);
                            }
                        }
                    }
                }

                return Task.Delay(0);
            }
            catch (Exception ex)
            {
                logger.Error("Process: " + ex.Message);
                throw ex;
            }
        }

        private string BuildEmailContent()
        {
            return $"{Essentials.messageDetails.HTMLMessage} <br/>";
        }

        private byte[] GetFile(string fileName)
        {
            try
            {
                var myfile = new WebClient().DownloadDataTaskAsync(new Uri(AWSHelper.GetPresignedFileURL(fileName)));
                myfile.Wait();
                return myfile.Result;
            }
            catch (Exception ex)
            {
                logger.Error("GetFile: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// builds email message body 
        /// </summary>
        /// <param name="body"></param>
        /// <param name="isHtmlBody"></param>
        /// <returns></returns>
        private BodyBuilder BuildEmailBody(string body, bool isHtmlBody = true)
        {
            var bodyBuilder = new BodyBuilder();
            if (isHtmlBody)
            {
                bodyBuilder.HtmlBody = $@"<html>
                                     <head>
                                         <title>Email</title>
                                     </head>
                                     <body>
                                         {body}
                                     </body>
                                 </html>";
            }
            else
            {
                bodyBuilder.TextBody = body;
            }
            return bodyBuilder;
        }

        /// <summary>
        /// builds email message headers 
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="cc"></param>
        /// <param name="bcc"></param>
        /// <param name="subject"></param>
        /// <returns></returns>
        private static MimeMessage BuildEmailHeaders(string from, string to, string subject)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(string.Empty, from));
            if (to != null && to != string.Empty)
            {
                message.To.Add(new MailboxAddress(string.Empty, to));
            }
            message.Subject = subject;

            return message;
        }

        /// <summary>
        /// sends email using AWS SES Api - using  SendRawEmail method.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public void Send(string userID, MimeMessage message, string configSet)
        {
            try
            {
                using (var memoryStream = new MemoryStream())
                {
                    var ms = message.WriteToAsync(memoryStream);
                    ms.Wait();
                    var sendRequest = new SendRawEmailRequest { RawMessage = new RawMessage(memoryStream) };
                    sendRequest.ConfigurationSetName = configSet;
                    AmazonSimpleEmailServiceClient _emailService = new AmazonSimpleEmailServiceClient(AWSHelper.awsKey, AWSHelper.awsSecret, RegionEndpoint.USWest2);

                    var response = _emailService.SendRawEmailAsync(sendRequest);
                    response.Wait();
                    if (response.Result.HttpStatusCode == HttpStatusCode.OK)
                    {
                        Essentials.PushInOutput(ChannelMaster.EmailChannel, userID, response.Result.MessageId, string.Empty);
                        logger.Info($"The email with message Id {response.Result.MessageId} sent successfully to {message.To} on {DateTime.UtcNow:O}");
                    }
                    else
                    {
                        Essentials.PushInOutput(ChannelMaster.EmailChannel, userID, response.Result.MessageId, response.Result.HttpStatusCode.ToString(), false);
                        logger.Info($"Failed to send email with message Id {response.Result.MessageId} to {message.To} on {DateTime.UtcNow:O} due to {response.Result.HttpStatusCode}.");
                    }
                }
            }
            catch (Exception ex)
            {
                Essentials.PushInOutput(ChannelMaster.EmailChannel, userID, string.Empty, ex.Message, false);
                logger.Error($"Send: {ex}");
                throw ex;
            }
        }
    }
}
