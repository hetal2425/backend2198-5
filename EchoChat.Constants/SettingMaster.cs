﻿namespace EchoChat.Constants
{
    public class SettingMaster
    {
        public static string WhatsAppAPI = "WhatsAppAPI";
        public static string WhatsAppTemplate = "WhatsAppTemplate";
        public static string WhatsAppTemplateButton = "WhatsAppTemplateButton";
        public static string SESConfigSet = "SESConfigSet";
        public static string PushNotificationAndroidKey = "PushNotificationAndroidKey";
        public static string PushNotificationAndroidURL = "PushNotificationAndroidURL";
        public static string SMSSettings = "SMSSettings";
        public static string WhatsAppAPIKarix = "WhatsAppAPIKarix";
        public static string WhatsAppTemplateKarix = "WhatsAppTemplateKarix";
        public static string SlackAppSettings = "SlackAppSettings";
    }
}
