﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChat.Models
{
    public class UserModel
    {
        public Guid UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailID { get; set; }
        public string Password { get; set; }
        public bool? IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public string CreateDate { get; set; }
        public string MobileNo { get; set; }
        public string ProfilePhoto { get; set; }
        public bool IsAdmin { get; set; }
    }

    public class ListOrGroupWiseUsers
    {        
        public int Total { get; set; }
        public List<UserModel> UserList { get; set; }
    }

    public class UserData
    {
        public Guid UserID { get; set; }
        public string UserName { get; set; }
        public string EmailID { get; set; }
        public string Token { get; set; }
        public string ChatUrl { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public string MobileNo { get; set; }
        public int ClientID { get; set; }
        public string ClientLogo { get; set; }
        public bool? IsAdmin { get; set; }
        public string ClientName { get; set; }
        public string ProfilePic { get; set; }
        public bool? IsPasswordUpdated { get; set; }
        public string ClientEmail { get; set; }
        public bool IsIntroHappened { get; set; }
    }

    public class WebhookLogin
    {
        public UserData user { get; set; }
    }

    public class APIAccessData
    {
        public Guid UserID { get; set; }
        public string UserName { get; set; }
        public string EmailID { get; set; }
        public string Token { get; set; }
        public string MobileNo { get; set; }
    }
}