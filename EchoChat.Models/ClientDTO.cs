﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChat.Models
{
    public class ClientDTO
    {
        public string OrganizationName { get; set; }
        public string ContactPersonName { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ReferredBy { get; set; }
        public string ClientLogo { get; set; }
        public IFormFile ClientLogoFile { get; set; }
    }
}
