﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChat.Models
{
    public class GetNoticeByID
    {

        public int GroupID { get; set; }
        public int ParentID { get; set; }
        public int PageSize { get; set; }
        public int PageNo { get; set; }

    }

    public class UserGroupVM
    {
        public int AttendanceOption { get; set; }
        public int ClientID { get; set; }
        public string Description { get; set; }
        public int FlowType { get; set; }
        public string Group_Name { get; set; }
        public string Owner { get; set; }
        public int GraceTime { get; set; }
        public int NoOfClasses { get; set; }
        public string AttendanceOptionValue { get; set; }
        public string Department { get; set; }

    }


    public class DeleteNotice
    {
        public int NoticeID { get; set; }
    }


    public class CreateUser
    {
        public Guid? UserID { get; set; }
        public string FirstName { get; set; }
        public string EmailID { get; set; }
        public string MobileNo { get; set; }
        public int ListID { get; set; }
        public bool IsAdmin { get; set; }
        public string Session { get; set; }
        public string ListName { get; set; }
        public bool? IsOnWhatsApp { get; set; }
        public bool? IsOnSms { get; set; }
        public bool? IsOnEmail { get; set; }
        public bool? IsOnTelegram { get; set; }
        public bool? IsOnSlack { get; set; }
        public string SlackId { get; set; }
        public string ProfilePhoto { get; set; }
        public IFormFile ProfilePhotoFile { get; set; }
        public Guid? CreatedBy { get; set; }
    }

    public class UserSmallModel
    {
        public Guid UserID { get; set; }
        public string FirstName { get; set; }
        public bool IsAdmin { get; set; }
    }

    public class UserClaimsModel
    {
        public Guid UserID { get; set; }
        public int ClientID { get; set; }
    }
    public class UserPersonalInfoModel
    {
        public string FirstName { get; set; }
        public string EmailID { get; set; }
        public string MobileNo { get; set; }
    }
    public class UserPasswordModel
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public bool IsPasswordUpdated { get; set; }
    }

    public partial class AppSettingModel
    {
        public int AppSettingId { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string Details { get; set; }
        public string Value1 { get; set; }
        public string Value2 { get; set; }
        public string Value3 { get; set; }
        public int? EntityId { get; set; }
        public string EntityName { get; set; }
        public string Value4 { get; set; }
        public string Value5 { get; set; }
        public string Value6 { get; set; }
        public string Value7 { get; set; }
    }
    public class SlackInvitationModel
    {
        public string Group { get; set; }
        public string Message { get; set; }
        public string Email { get; set; }

    }
}
