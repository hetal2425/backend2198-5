﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.Models
{
    public class NoticeRead
    {
        public int NoticeID { get; set; }
        public bool IsRead { get; set; }
        public string FirstName { get; set; }
        public string MobileNo { get; set; }
    }
}
