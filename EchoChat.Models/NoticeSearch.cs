﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.Models
{
    public class NoticeSearch
    {
        public string Search { get; set; }
        public Nullable<DateTime> DateFrom { get; set; }
        public Nullable<DateTime> DateTo { get; set; }
        public int ClientID { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
