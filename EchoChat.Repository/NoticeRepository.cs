﻿using EchoChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using EchoChat.DB;
using AutoMapper;
using System.Collections.Concurrent;
using EchoChat.Constants;

namespace EchoChat.Repository
{
    public class NoticeRepository : INoticeRepository
    {
        IRepositoryBase _repositoryBase = null;
        private readonly IMapper _mapper;
        public NoticeRepository(IRepositoryBase repositoryBase, IMapper mapper)
        {
            _repositoryBase = repositoryBase;
            _mapper = mapper;
        }
        public bool SaveNoticeOutput(int noticeID, int clientID, ConcurrentBag<ChannelOutput> output)
        {
            try
            {
                List<UserNotice> notices = _repositoryBase.Get<UserNotice>(n => n.NoticeId == noticeID).ToList();
                int smsCnt = 0; int emailCnt = 0; int waCnt = 0, slackCnt = 0;
                foreach (ChannelOutput co in output)
                {
                    if (!string.IsNullOrEmpty(co.UserID))
                    {
                        Guid.TryParse(co.UserID, out Guid userID);
                        if (userID != null && userID != Guid.Empty)
                        {
                            UserNotice n = notices.Where(n => n.UserId == userID).FirstOrDefault();
                            if (n != null)
                            {
                                if (co.IsSuccessful)
                                {
                                    switch (co.ChannelName)
                                    {
                                        case ChannelMaster.SMSChannel:
                                            n.SmsmsgId = co.MsgID;
                                            smsCnt++;
                                            break;
                                        case ChannelMaster.EmailChannel:
                                            n.EmailMsgId = co.MsgID;
                                            emailCnt++;
                                            break;
                                        case ChannelMaster.WhatsappChannel:
                                            n.WamsgId = co.MsgID;
                                            waCnt++;
                                            break;
                                        case ChannelMaster.WhatsappKarixChannel:
                                            n.WamsgId = co.MsgID;
                                            waCnt++;
                                            break;
                                        case ChannelMaster.SlackChannel:
                                            n.SlackMsgID = co.MsgID;
                                            slackCnt++;
                                            break;
                                    }

                                    _repositoryBase.Update<UserNotice>(n);
                                }
                                else
                                {
                                    FailedNotice fn = new FailedNotice();
                                    fn.Channel = co.ChannelName;
                                    fn.Unid = n.UserNoticeId;
                                    fn.ClientId = clientID;
                                    fn.UserId = userID;
                                    fn.Error = co.Error;
                                    fn.CreatedDate = DateTime.UtcNow;
                                    _repositoryBase.Add<FailedNotice>(fn);
                                }
                            }
                        }
                    }
                }

                if (smsCnt > 0)
                {
                    SaveQuotaInfo(ChannelMaster.SMSChannel, smsCnt, clientID);
                }
                if (emailCnt > 0)
                {
                    SaveQuotaInfo(ChannelMaster.EmailChannel, emailCnt, clientID);
                }
                if (waCnt > 0)
                {
                    SaveQuotaInfo(ChannelMaster.WhatsappChannel, waCnt, clientID);
                }
                if (slackCnt > 0)
                {
                    SaveQuotaInfo(ChannelMaster.SlackChannel, slackCnt, clientID);
                }
                return _repositoryBase.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public NoticeDetailDTO GetNoticeDetailsByNoticeID(int noticeID)
        {
            try
            {
                Notice nd = _repositoryBase.Get<Notice>(n => n.NoticeId == noticeID).FirstOrDefault();
                return _mapper.Map<Notice, NoticeDetailDTO>(nd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SeenNotice(int noticeID, Guid userID, string type)
        {
            try
            {
                _repositoryBase.ExecuteSqlCommand(string.Format("exec usp_UpdateIsRead {0}, '{1}', '{2}'", noticeID, userID, type));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public NoticeSmallDataModel GetNoticeListByGroupID(int groupID, int pageNo, int pageSize, Guid userID, string freeText, int timeZoneOffset)
        {
            try
            {
                int? total = 0;
                List<usp_GetNoticesListResult> data = _repositoryBase._Context.usp_GetNoticesListAsync(groupID, userID, pageSize, pageNo, freeText, timeZoneOffset, ref total);

                List<NoticeSmallData> list = _mapper.Map<List<usp_GetNoticesListResult>, List<NoticeSmallData>>(data);

                NoticeSmallDataModel model = new NoticeSmallDataModel();
                model.Total = total ?? 0;
                model.NoticeSmallDatas = list;
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public NoticeSmallDataModel GetNoticeRepliesListByGroupID(int parentID, int pageNo, int pageSize, Guid userID, string freeText, int timeZoneOffset)
        {
            try
            {
                int? total = 0;
                List<usp_GetNoticesList_RepliesResult> data = _repositoryBase._Context.usp_GetNoticesList_RepliesAsync(parentID, userID, pageSize, pageNo, freeText, timeZoneOffset, ref total);
                List<NoticeSmallData> list = _mapper.Map<List<usp_GetNoticesList_RepliesResult>, List<NoticeSmallData>>(data);

                NoticeSmallDataModel model = new NoticeSmallDataModel();
                model.Total = total ?? 0;
                model.NoticeSmallDatas = list;
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DeletedNotice> DeleteNotice(int noticeID)
        {
            try
            {
                Notice noticeDetail = _repositoryBase.Get<Notice>(n => n.NoticeId == noticeID).FirstOrDefault();
                noticeDetail.IsDeleted = true;
                _repositoryBase.Update<Notice>(noticeDetail);
                _repositoryBase.AddLog(LogType.Delete, ModuleType.Notice, noticeID, $"Notice {noticeDetail.NoticeTitle} deleted successfully", noticeDetail.CreatedBy ?? Guid.NewGuid());
                //_repositoryBase.SaveChanges();

                List<DeletedNotice> deletedNotice = (from ul in _repositoryBase.Get<UserList>(l => l.UserId == noticeDetail.CreatedBy)
                                                     join udt in _repositoryBase.Get<UserDeviceTokenMapping>().DefaultIfEmpty() on ul.UserId equals udt.UserId
                                                     select new DeletedNotice()
                                                     {
                                                         DeviceToken = udt.DeviceToken,
                                                         Platform = udt.Platform
                                                     }).ToList();
                return deletedNotice;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveNoticeDetails(NoticeDetailDTO Notice)
        {
            try
            {
                Notice n = _mapper.Map<NoticeDetailDTO, Notice>(Notice);
                n.NoticeDate = DateTime.UtcNow;
                n.IsActive = true;
                n.IsDeleted = false;
                _repositoryBase.Add<Notice>(n);
                _repositoryBase.AddLog(LogType.Add, ModuleType.Notice, n.NoticeId, $"Notice {n.NoticeTitle} created successfully", n.CreatedBy ?? Guid.NewGuid());
                bool result = _repositoryBase.SaveChanges();
                Notice.NoticeID = n.NoticeId;
                if (Notice.Date.HasValue && Notice.Date.Value != DateTime.MinValue)
                {

                    NoticeScheduler noticeScheduler = new NoticeScheduler();
                    noticeScheduler.NoticeId = n.NoticeId;
                    noticeScheduler.Date = Notice.Date;
                    noticeScheduler.Time = Notice.Time;
                    noticeScheduler.IsRecursive = Notice.IsRecursive;
                    noticeScheduler.Days = Notice.Days;

                    _repositoryBase.Add<NoticeScheduler>(noticeScheduler);

                    return _repositoryBase.SaveChanges();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveNotice(List<UserChannelInfo> userChannelInfos, int noticeID, int groupID, Guid createdBy)
        {
            try
            {
                foreach (UserChannelInfo user in userChannelInfos)
                {
                    UserNotice n = new UserNotice();
                    n.NoticeId = noticeID;
                    n.GroupId = groupID;
                    n.UserId = user.UserID;
                    n.CreateDate = DateTime.UtcNow;
                    if (user.UserID == createdBy)
                    {
                        n.IsRead = true;
                        n.IsNotified = true;
                    }

                    _repositoryBase.Add<UserNotice>(n);
                }

                return _repositoryBase.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ScheduledNotices> GetScheduledNotices(DateTime fromDate, DateTime toDate, int timeZoneOffset)
        {
            try
            {
                var result = _repositoryBase._Context.usp_GetScheduledNoticeListAsync(fromDate, toDate, timeZoneOffset);
                result.Wait();
                List<usp_GetScheduledNoticeListResult> data = result.Result;
                return _mapper.Map<List<usp_GetScheduledNoticeListResult>, List<ScheduledNotices>>(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveQuotaInfo(string channel, int usage, int clientID)
        {
            try
            {
                Quotum q = new Quotum();
                q.Channel = channel;
                q.Usage = usage;
                q.ClientId = clientID;
                q.CreatedDate = DateTime.UtcNow;

                return _repositoryBase.Add<Quotum>(q);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool SaveIntegrationNoticeDetails(NoticeDetailDTO Notice)
        {
            try
            {
                Notice n = _mapper.Map<NoticeDetailDTO, Notice>(Notice);
                n.NoticeDate = DateTime.UtcNow;
                n.IsActive = true;
                n.IsDeleted = false;
                _repositoryBase.Add<Notice>(n);
                _repositoryBase.AddLog(LogType.Add, ModuleType.Notice, n.NoticeId, $"Integration notice {n.NoticeTitle} created successfully", n.CreatedBy ?? Guid.NewGuid());
                bool result = _repositoryBase.SaveChanges();
                Notice.NoticeID = n.NoticeId;
                if (Notice.Date.HasValue && Notice.Date.Value != DateTime.MinValue)
                {

                    NoticeScheduler noticeScheduler = new NoticeScheduler();
                    noticeScheduler.NoticeId = n.NoticeId;
                    noticeScheduler.Date = Notice.Date;
                    noticeScheduler.Time = Notice.Time;
                    noticeScheduler.IsRecursive = Notice.IsRecursive;
                    noticeScheduler.Days = Notice.Days;

                    _repositoryBase.Add<NoticeScheduler>(noticeScheduler);

                    return _repositoryBase.SaveChanges();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveIntegrationNotice(List<UserChannelInfo> userChannelInfos, int noticeID, int groupID, Guid createdBy)
        {
            try
            {
                foreach (UserChannelInfo user in userChannelInfos)
                {
                    UserNotice n = new UserNotice();
                    n.NoticeId = noticeID;
                    n.GroupId = groupID;
                    n.UserId = user.UserID;
                    n.CreateDate = DateTime.UtcNow;
                    if (user.UserID == createdBy)
                    {
                        n.IsRead = true;
                        n.IsNotified = true;
                    }

                    _repositoryBase.Add<UserNotice>(n);
                }

                return _repositoryBase.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SavePrivateNoticeDetails(NoticeDetailDTO Notice)
        {
            try
            {
                Notice n = _mapper.Map<NoticeDetailDTO, Notice>(Notice);
                n.NoticeDate = DateTime.UtcNow;
                n.IsActive = true;
                n.IsDeleted = false;
                _repositoryBase.Add<Notice>(n);
                _repositoryBase.AddLog(LogType.Add, ModuleType.Notice, n.NoticeId, $"Private notice {n.NoticeTitle} created successfully", n.CreatedBy ?? Guid.NewGuid());
                bool result = _repositoryBase.SaveChanges();
                Notice.NoticeID = n.NoticeId;
                if (Notice.Date.HasValue && Notice.Date.Value != DateTime.MinValue)
                {

                    NoticeScheduler noticeScheduler = new NoticeScheduler();
                    noticeScheduler.NoticeId = n.NoticeId;
                    noticeScheduler.Date = Notice.Date;
                    noticeScheduler.Time = Notice.Time;
                    noticeScheduler.IsRecursive = Notice.IsRecursive;
                    noticeScheduler.Days = Notice.Days;

                    _repositoryBase.Add<NoticeScheduler>(noticeScheduler);

                    return _repositoryBase.SaveChanges();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SavePrivateNotice(List<UserChannelInfo> userChannelInfos, int noticeID, int groupID, Guid createdBy)
        {
            try
            {
                foreach (UserChannelInfo user in userChannelInfos)
                {
                    UserNotice n = new UserNotice();
                    n.NoticeId = noticeID;
                    n.GroupId = groupID;
                    n.UserId = user.UserID;
                    n.CreateDate = DateTime.UtcNow;
                    if (user.UserID == createdBy)
                    {
                        n.IsRead = true;
                        n.IsNotified = true;
                    }

                    _repositoryBase.Add<UserNotice>(n);
                }

                return _repositoryBase.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<string> GetConfigWhatsAppTemplate()
        {
            try
            {
                List<string> keyList = new List<string>();
                List<AppSetting> appSettings = _repositoryBase.Get<AppSetting>(x => x.Key == SettingMaster.WhatsAppTemplate).ToList();
                foreach (AppSetting appSetting in appSettings)
                {
                    keyList.Add(appSetting.Value);
                }

                return keyList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HubNotice GetNoticeDetailsForHub(int noticeID)
        {
            try
            {
                var result = _repositoryBase._Context.usp_GetNoticeDetailsForHubAsync(noticeID);
                result.Wait();
                if (result.Result != null)
                {
                    usp_GetNoticeDetailsForHubResult data = result.Result.FirstOrDefault();
                    return _mapper.Map<usp_GetNoticeDetailsForHubResult, HubNotice>(data);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}