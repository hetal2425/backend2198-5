﻿using AutoMapper;
using EchoChat.DB;
using EchoChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchoChat.Repository
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<usp_GetUserGroupsResult, UserGroupModel>().ReverseMap();
            CreateMap<usp_User_SelectByGroupIDResult, UserModel>().ReverseMap();
            CreateMap<usp_User_SelectByListIDResult, UserModel>().ReverseMap();
            CreateMap<usp_GetGroupMappingUsersResult, UserChannelInfo>().ReverseMap();
            CreateMap<usp_GetUsersForPendingNoticeResult, UserChannelInfo>().ReverseMap();
            CreateMap<usp_GetNoticesListResult, NoticeSmallData>().ReverseMap();
            CreateMap<usp_GetNoticesList_RepliesResult, NoticeSmallData>().ReverseMap();
            CreateMap<usp_GetDashboardResult, Dashboard>().ReverseMap();
            CreateMap<usp_GetUserByNoticeReadResult, NoticeRead>().ReverseMap();
            CreateMap<usp_GetUserLoginResult, UserData>().ReverseMap();
            CreateMap<usp_GetScheduledNoticeListResult, ScheduledNotices>().ReverseMap();
            CreateMap<usp_GetListByClientIDResult, ListDTO>().ReverseMap();
            CreateMap<usp_GetUserGroupsResult, UserGroups>().ReverseMap();

            CreateMap<usp_Report_GetGroupReportHeadingResult, GroupReportHeading>().ReverseMap();
            CreateMap<usp_Report_GetGroupReportDetailsResult, GroupReportDetails>().ReverseMap();
            CreateMap<usp_Report_GetCampaignReportHeadingResult, CampaignReportHeading>().ReverseMap();
            CreateMap<usp_Report_GetCampaignReportDetailsResult, CampaignReportDetails>().ReverseMap();
            CreateMap<usp_Report_GetReadReportDetailsResult, ReadReportDetails>().ReverseMap();
            CreateMap<usp_Report_ChannelVerificationResult, ChannelVerification>().ReverseMap();

            CreateMap<usp_Report_GetCampaignSummaryReportResult, CampaignSummaryModel>().ReverseMap();
            CreateMap<usp_Report_GetChannelInsightReportResult, ChannelInsightModel>().ReverseMap();
            CreateMap<usp_Report_GetChannelSavingReportResult, ChannelSavingModel>().ReverseMap();
            CreateMap<usp_Report_GetChannelTrendsReportResult, ChannelProgressModel>().ReverseMap();
            CreateMap<usp_Report_GetWeekdaysEngagementReportResult, WeekdaysEngagementModel>().ReverseMap();

            CreateMap<usp_GetSchedularDataResult, SchedularDTO>().ReverseMap();
            CreateMap<usp_GetTemplateDataResult, TemplateDTO>().ReverseMap();
            CreateMap<usp_GetUserIdsFromGroupResult, UserListDTO>().ReverseMap();
            CreateMap<usp_GetNoticeDetailsForHubResult, HubNotice>().ReverseMap();
            
            CreateMap<AppVersionDTO, AppVersion>().ReverseMap();
            CreateMap<DemoRequestDTO, DemoRequest>().ReverseMap();
            CreateMap<GroupDTO, Group>().ReverseMap();
            CreateMap<NoticeDetailDTO, Notice>().ReverseMap();
            CreateMap<User, UserSmallModel>().ReverseMap();
            CreateMap<User, UserProfile>().ReverseMap();
            CreateMap<CreateUser, User>().ReverseMap();
            CreateMap<ListDTO, List>().ReverseMap();
            CreateMap<AppSettingModel, AppSetting>().ReverseMap();
            CreateMap<WhatsAppParameterData, WhatsAppParameter>().ReverseMap();
        }
    }
}
