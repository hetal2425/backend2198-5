﻿using AutoMapper;
using EchoChat.DB;
using EchoChat.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EchoChat.Repository
{
    public class SchedularRepository : ISchedularRepository
    {
        IRepositoryBase _repositoryBase = null;
        private readonly IMapper _mapper;
        public SchedularRepository(IRepositoryBase repositoryBase, IMapper mapper)
        {
            _repositoryBase = repositoryBase;
            _mapper = mapper;
        }
        public void AddSchedular(SchedularDTO schedular)
        {
            try
            {
                _repositoryBase._Context.usp_InsertSchedularData(schedular.NoticeTitle, schedular.NoticeDetailHTML, schedular.IsSms, schedular.IsEmail, schedular.IsWhatsapp, schedular.IsFacebook, schedular.IsSlack, schedular.IsInstagram, schedular.IsViber, schedular.IsLine, schedular.IsWeChat, schedular.IsPushNotification, schedular.IsBroadcastToAllChannel, schedular.GroupId, schedular.ParentId, schedular.ScheduleTime, schedular.UserName, schedular.Password, schedular.FileName, schedular.FileType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SchedularDTO> GetSchedular()
        {
            try
            {
                var result = _repositoryBase._Context.usp_GetSchedularDataResultAsync();
                result.Wait();
                List<usp_GetSchedularDataResult> data = result.Result;
                return _mapper.Map<List<usp_GetSchedularDataResult>, List<SchedularDTO>>(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveSchedule(string schedularIds)
        {
            try
            {
                _repositoryBase._Context.usp_RemoveSchedularData(schedularIds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListOfSchedular GetSchedulePagedData(DateTime? startDate, DateTime? endDate, int pageSize, int pageNo)
        {
            try
            {

                int? total = 0;
                var result = _repositoryBase._Context.usp_GetSchedulePagedDataResultAsync(startDate, endDate, pageSize, pageNo, ref total);
                List<usp_GetSchedularDataResult> data = result;
                List<SchedularDTO> schedularData = _mapper.Map<List<usp_GetSchedularDataResult>, List<SchedularDTO>>(data);

                ListOfSchedular schedularList = new ListOfSchedular();
                schedularList.SchedularList = schedularData;
                schedularList.Total = total ?? 0;

                return schedularList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
