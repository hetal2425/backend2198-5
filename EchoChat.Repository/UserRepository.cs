﻿using AutoMapper;
using EchoChat.Constants;
using EchoChat.DB;
using EchoChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EchoChat.Repository
{
    public class UserRepository : IUserRepository
    {
        IRepositoryBase _repositoryBase = null;
        private readonly IMapper _mapper;
        public UserRepository(IRepositoryBase repositoryBase, IMapper mapper)
        {
            _repositoryBase = repositoryBase;
            _mapper = mapper;
        }
        public void AddUserConnection(string connectionID, Guid userID)
        {
            try
            {
                _repositoryBase.ExecuteSqlCommand(string.Format("exec usp_InsertUpdateUserConnection '{0}', '{1}' ", connectionID, userID));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetUserConnection(Guid userID)
        {
            try
            {
                return _repositoryBase.Get<ConnectedUser>(c => c.UserId == userID).Select(c => c.ConnectionId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CreateUpdateUser(CreateUser user, int clientID, Guid createdBy)
        {
            try
            {
                User u = _mapper.Map<CreateUser, User>(user);
                List l = new List();
                if (user.UserID != null && user.UserID.ToString() != string.Empty && user.UserID != Guid.Empty)
                {
                    User _user = _repositoryBase.Get<User>(x => x.UserId == user.UserID).FirstOrDefault();
                    string oldName = new string(user.FirstName);
                    _user.FirstName = user.FirstName;
                    _user.EmailId = user.EmailID;
                    _user.MobileNo = user.MobileNo;
                    _user.SlackId = user.SlackId;
                    _user.IsOnSlack = user.IsOnSlack;
                    _repositoryBase.Update<User>(_user);
                    _repositoryBase.AddLog(LogType.Update, ModuleType.User, 0, $"User {oldName} renamed as {user.FirstName} successfully", createdBy);
                }
                else
                {
                    User _user = _repositoryBase.Get<User>(x => x.MobileNo == user.MobileNo).FirstOrDefault();
                    if (_user == null || string.IsNullOrWhiteSpace(user.MobileNo))
                    {
                        u.CreatedBy = createdBy;
                        u.CreateDate = DateTime.UtcNow;
                        u.ClientId = clientID;
                        u.UserId = Guid.NewGuid();
                        u.Password = "lkLi2AjNn34=";
                        u.IsDeleted = false;
                        _repositoryBase.Add<User>(u);
                        _repositoryBase.AddLog(LogType.Add, ModuleType.User, 0, $"User {u.FirstName} created successfully", createdBy);
                    }
                    else
                    {
                        u.UserId = _user.UserId;
                    }

                    if (user.ListID == 0 && user.ListName != string.Empty)
                    {
                        l.ClientId = clientID;
                        l.CreatedBy = createdBy;
                        l.Name = user.ListName;
                        l.IsActive = true;
                        l.IsDeleted = false;
                        _repositoryBase.Add<List>(l);
                    }


                    _repositoryBase.SaveChanges();
                    if (user.ListID == 0 && user.ListName != string.Empty)
                    {
                        _repositoryBase.AddLog(LogType.Add, ModuleType.List, l.ListId, $"List {l.Name} created successfully", l.CreatedBy);
                    }
                    user.UserID = u.UserId;

                    if (user.ListID == 0 && user.ListName != string.Empty)
                    {
                        user.ListID = l.ListId;
                    }

                    UserList userList = new UserList();
                    userList.UserId = user.UserID ?? Guid.Empty;
                    userList.ListId = user.ListID;
                    _repositoryBase.Add<UserList>(userList);
                }

                return _repositoryBase.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteUser(Guid userID)
        {
            try
            {
                User u = _repositoryBase.Get<User>(x => x.UserId == userID).FirstOrDefault();
                u.IsDeleted = true;
                _repositoryBase.Update<User>(u);
                _repositoryBase.AddLog(LogType.Delete, ModuleType.User, 0, $"User {u.FirstName} deleted successfully", u.CreatedBy ?? Guid.NewGuid());
                return _repositoryBase.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateIntroHappened(Guid userID)
        {
            try
            {
                User u = _repositoryBase.Get<User>(x => x.UserId == userID).FirstOrDefault();
                u.IsIntroHappened = true;
                _repositoryBase.Update<User>(u);
                return _repositoryBase.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool FirstTimeUpdatePassword(UpdatePassword update)
        {
            throw new NotImplementedException();
        }

        public CreateUser GetUserByUsername(string username)
        {
            try
            {
                User u = _repositoryBase.Get<User>(x => x.EmailId == username || x.MobileNo == username).FirstOrDefault();
                return _mapper.Map<User, CreateUser>(u);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveUserConnection(string ConnectionID)
        {
            try
            {
                _repositoryBase.ExecuteSqlCommand(string.Format("exec usp_InsertUpdateUserConnection '{0}'", ConnectionID));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SubmitUsers(ExcelUsers list, Guid userID, int clientID)
        {
            using (var dbContextTransaction = _repositoryBase._Context.Database.BeginTransaction())
            {
                try
                {
                    foreach (var user in list.UserList)
                    {
                        if (user.MobileNo != null && user.IsValid)
                        {
                            User _user = _repositoryBase.Get<User>(x => x.MobileNo == user.MobileNo).FirstOrDefault();

                            if (_user == null)
                            {
                                _user = new User();
                                _user.UserId = Guid.NewGuid();
                                _user.FirstName = user.FirstName;
                                _user.MobileNo = user.MobileNo;
                                _user.EmailId = user.Email;
                                _user.Password = "lkLi2AjNn34=";
                                _user.CreatedBy = userID;
                                _user.ClientId = clientID;
                                _user.IsDeleted = false;
                                _user.FacebookId = string.Empty;
                                _repositoryBase.Add<User>(_user);
                            }
                        }
                    }

                    _repositoryBase.SaveChanges();

                    foreach (var user in list.UserList)
                    {
                        if (user.MobileNo != null && user.IsValid)
                        {
                            Guid userId = _repositoryBase.Get<User>(x => x.MobileNo == user.MobileNo).Select(u => u.UserId).FirstOrDefault();

                            List l = new List();
                            if (list.ListID == 0 && list.ListName != string.Empty)
                            {
                                l.ClientId = clientID;
                                l.Name = list.ListName;
                                l.IsActive = true;
                                l.IsDeleted = false;
                                l.CreatedBy = userID;
                                _repositoryBase.Add<List>(l);
                                _repositoryBase.SaveChanges();
                            }
                            if (list.ListID == 0)
                            {
                                list.ListID = l.ListId;
                            }

                            UserList userList = new UserList();
                            userList.UserId = userId;
                            userList.ListId = list.ListID;
                            _repositoryBase.Add<UserList>(userList);
                        }
                    }

                    _repositoryBase.SaveChanges();
                    dbContextTransaction.Commit();
                    return list.ListID;
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    throw ex;
                }
            }
        }

        public bool UpdatePassword(UpdatePassword update)
        {
            try
            {
                User u = _repositoryBase.Get<User>(x => x.UserId == Guid.Parse(update.UserID)).FirstOrDefault();
                u.Password = update.Password;
                u.IsPasswordUpdated = true;
                _repositoryBase.Add<User>(u);
                return _repositoryBase.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SetGroupAdmin(GroupAdminMapping mapping)
        {
            try
            {
                //Delete old mappings

                _repositoryBase.ExecuteSqlCommand(string.Format(" delete from GroupUserAdmin where GroupID = {0}", mapping.GroupID));
                _repositoryBase.SaveChanges();

                foreach (Guid u in mapping.UserIDs)
                {
                    GroupUserAdmin gua = new GroupUserAdmin();
                    gua.GroupId = mapping.GroupID;
                    gua.UserId = u;
                    gua.CreatedBy = mapping.CreatedBy;
                    _repositoryBase.Add<GroupUserAdmin>(gua);
                }
                return _repositoryBase.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UserSmallModel> GetUsersByListIDs(List<int> listIDs)
        {
            try
            {
                List<UserSmallModel> users = (from ul in _repositoryBase.Get<UserList>(l => listIDs.Contains(l.ListId))
                                              join u in
                                              _repositoryBase.Get<User>() on ul.UserId equals u.UserId
                                              join gua in
                                              _repositoryBase.Get<GroupUserAdmin>() on u.UserId equals gua.UserId into adminUsers
                                              from au in adminUsers.DefaultIfEmpty()
                                              select new UserSmallModel()
                                              {
                                                  FirstName = u.FirstName,
                                                  IsAdmin = au != null ? true : false,
                                                  UserID = u.UserId
                                              }).ToList();

                return users;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserProfile GetUserByID(Guid userID)
        {
            try
            {
                User user = _repositoryBase.Get<User>(u => u.UserId == userID).FirstOrDefault();
                return _mapper.Map<User, UserProfile>(user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public UserProfile GetUserBySlackID(string slackID)
        {
            try
            {
                User user = _repositoryBase.Get<User>(u => u.SlackId == slackID).FirstOrDefault();
                return _mapper.Map<User, UserProfile>(user);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public UserClaimsModel GetUserClaimsByID(Guid userID)
        {
            try
            {
                UserClaimsModel user = _repositoryBase.Get<User>(u => u.UserId == userID).Select(u => new UserClaimsModel() { UserID = u.UserId, ClientID = u.ClientId }).FirstOrDefault();
                return user;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool UpdateUserPersonalInfo(UserPersonalInfoModel user, Guid userID)
        {
            try
            {
                _repositoryBase.ExecuteSqlCommand(string.Format("exec usp_UpdateUserPersonalInfo '{0}', '{1}', '{2}','{3}'", userID, user.FirstName, user.MobileNo, user.EmailID));
                _repositoryBase.AddLog(LogType.Update, ModuleType.User, 0, $"User personal Info updated successfully: Name: {user.FirstName}, Email: {user.EmailID}, Mobile No: {user.MobileNo}", userID);
                return _repositoryBase.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetUserPasswordByID(Guid userID)
        {
            try
            {
                return _repositoryBase.Get<User>(u => u.UserId == userID).Select(u => u.Password).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool UpdateUserPassword(UserPasswordModel user, Guid userID)
        {
            try
            {
                _repositoryBase.ExecuteSqlCommand(string.Format("exec usp_UpdateUserPassword '{0}', '{1}', '{2}'", userID, user.ConfirmPassword, user.IsPasswordUpdated));
                _repositoryBase.AddLog(LogType.Update, ModuleType.User, 0, $"User password updated successfully", userID);
                return _repositoryBase.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveUpdateSetting(AppSettingModel appSetting)
        {
            try
            {
                AppSetting a = _mapper.Map<AppSettingModel, AppSetting>(appSetting);
                if (appSetting.AppSettingId > 0)
                {
                    AppSetting _appSetting = _repositoryBase.Get<AppSetting>(x => x.AppSettingId == appSetting.AppSettingId).FirstOrDefault();
                    _appSetting.Key = appSetting.Key;
                    _appSetting.Value = appSetting.Value;
                    _appSetting.Value1 = appSetting.Value1;
                    _appSetting.Value2 = appSetting.Value2;
                    _appSetting.Value3 = appSetting.Value3;
                    _appSetting.Value4 = appSetting.Value4;
                    _appSetting.Value5 = appSetting.Value5;
                    _appSetting.Value6 = appSetting.Value6;
                    _appSetting.Value7 = appSetting.Value7;
                    _appSetting.Details = appSetting.Details;
                    _repositoryBase.Update<AppSetting>(_appSetting);
                }
                else
                {
                    a.Key = appSetting.Key;
                    a.Value = appSetting.Value;
                    a.Details = appSetting.Details;
                    a.Value1 = appSetting.Value1;
                    a.Value2 = appSetting.Value2;
                    a.Value3 = appSetting.Value3;
                    a.Value4 = appSetting.Value4;
                    a.Value5 = appSetting.Value5;
                    a.Value6 = appSetting.Value6;
                    a.Value7 = appSetting.Value7;
                    a.EntityId = 0;
                    a.EntityName = "Client";
                    _repositoryBase.Add<AppSetting>(a);
                }

                return _repositoryBase.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteSetting(int appSettingId)
        {
            try
            {
                _repositoryBase.ExecuteSqlCommand(string.Format("delete from AppSettings where AppSettingId = {0}", appSettingId));

                return _repositoryBase.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AppSettingModel> GetAppSettingByChannel(String channel)
        {
            try
            {
                List<string> keyList = new List<string>();

                switch (channel)
                {
                    case ChannelMaster.SMSChannel:
                        keyList.Add(SettingMaster.SMSSettings);
                        break;
                    case ChannelMaster.WhatsappChannel:
                        keyList.Add(SettingMaster.WhatsAppAPI);
                        keyList.Add(SettingMaster.WhatsAppTemplate);
                        keyList.Add(SettingMaster.WhatsAppTemplateButton);
                        break;
                    case ChannelMaster.PushNotificationChannel:
                        keyList.Add(SettingMaster.PushNotificationAndroidKey);
                        keyList.Add(SettingMaster.PushNotificationAndroidURL);
                        break;
                    case ChannelMaster.EmailChannel:
                        keyList.Add(SettingMaster.SESConfigSet);
                        break;
                    case ChannelMaster.WhatsappKarixChannel:
                        keyList.Add(SettingMaster.WhatsAppAPIKarix);
                        keyList.Add(SettingMaster.WhatsAppTemplateKarix);
                        break;
                }

                List<AppSetting> appSettings = _repositoryBase.Get<AppSetting>(x => keyList.Any(k => k == x.Key)).ToList();
                return _mapper.Map<List<AppSetting>, List<AppSettingModel>>(appSettings);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<string> GetSettingKeys(bool isKarix)
        {
            try
            {
                List<string> settingChannels = new List<string>();
                if (isKarix)
                {
                    settingChannels.Add(SettingMaster.WhatsAppAPIKarix);
                    settingChannels.Add(SettingMaster.WhatsAppTemplateKarix);
                }
                else
                {
                    settingChannels.Add(SettingMaster.WhatsAppAPI);
                    settingChannels.Add(SettingMaster.WhatsAppTemplate);
                    settingChannels.Add(SettingMaster.WhatsAppTemplateButton);
                }
                settingChannels.Add(SettingMaster.SESConfigSet);
                settingChannels.Add(SettingMaster.PushNotificationAndroidKey);
                settingChannels.Add(SettingMaster.PushNotificationAndroidURL);
                settingChannels.Add(SettingMaster.SMSSettings);
                return settingChannels;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CreateUser CreateIntegrationUser(CreateUser user, int clientID, Guid createdBy)
        {
            try
            {
                User u = _mapper.Map<CreateUser, User>(user);
                List l = new List();

                User _user = _repositoryBase.Get<User>(x => x.MobileNo == user.MobileNo).FirstOrDefault();
                if (_user == null)
                {
                    u.CreatedBy = createdBy;
                    u.CreateDate = DateTime.UtcNow;
                    u.ClientId = clientID;
                    u.UserId = Guid.NewGuid();
                    u.Password = "lkLi2AjNn34=";
                    u.IsDeleted = false;
                    _repositoryBase.Add<User>(u);
                    user.UserID = u.UserId;
                    _repositoryBase.AddLog(LogType.Add, ModuleType.User, 0, $"Integration user {u.FirstName} created successfully", createdBy);

                }
                else
                {
                    user.UserID = _user.UserId;
                }

                return user;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UserListDTO> GetUserIdsFromGroup(int groupId)
        {
            try
            {
                var result = _repositoryBase._Context.usp_GetUserIdsFromGroupResultAsync(groupId);
                List<usp_GetUserIdsFromGroupResult> data = result;
                return _mapper.Map<List<usp_GetUserIdsFromGroupResult>, List<UserListDTO>>(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SubmitWhatsAppParameters(List<ExcelWhatsAppData> list, Guid userID, int clientID)
        {
            try
            {
                foreach (var user in list)
                {
                    if (user.MobileNo != null && user.IsValid)
                    {
                        WhatsAppParameter _user = new WhatsAppParameter();
                        _user.MobileNo = user.MobileNo;
                        _user.TemplateName = user.TemplateName;
                        _user.Value1 = user.Value1;
                        _user.Value2 = user.Value2;
                        _user.Value3 = user.Value3;
                        _user.Value4 = user.Value4;
                        _user.Value5 = user.Value5;
                        _user.Value6 = user.Value6;
                        _user.Value7 = user.Value7;
                        _user.Value8 = user.Value8;
                        _user.Value9 = user.Value9;
                        _user.Value10 = user.Value10;
                        _repositoryBase.Add<WhatsAppParameter>(_user);
                    }
                }
                return _repositoryBase.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<WhatsAppParameterData> GetWhatsAppParameters(string Template)
        {
            List<WhatsAppParameter> result = _repositoryBase.Get<WhatsAppParameter>(u => u.TemplateName == Template).ToList();
            List<WhatsAppParameterData> whatsAppParameterData = _mapper.Map<List<WhatsAppParameter>, List<WhatsAppParameterData>>(result);

            return whatsAppParameterData;
        }
    }
}
