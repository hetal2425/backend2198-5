﻿using AutoMapper;
using EchoChat.DB;
using EchoChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EchoChat.Repository
{
    public class CommonRepository : ICommonRepository
    {
        IRepositoryBase _repositoryBase = null;
        private readonly IMapper _mapper;
        public CommonRepository(IRepositoryBase repositoryBase, IMapper mapper)
        {
            _repositoryBase = repositoryBase;
            _mapper = mapper;
        }
    }
}
