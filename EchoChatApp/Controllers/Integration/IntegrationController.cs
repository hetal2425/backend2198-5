﻿using EchoChat.Channel;
using EchoChat.ChannelConnector;
using EchoChat.ChannelVerification;
using EchoChat.Constants;
using EchoChat.Helper;
using EchoChat.Models;
using EchoChat.ServiceProvider;
using EchoChatApp.General;
using EchoChatApp.Helper;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChatApp.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class IntegrationController : BaseController
    {
        private IAppProvider appProvider;
        private ILog logger;
        private IAppSetting appSetting;
        private IConnector connector;
        private IGroupProvider groupProvider;
        private INoticeProvider noticeProvider;
        private IUserProvider userProvider;
        public IntegrationController(IAppProvider oAppProvider, IConnector oConnector, IAppSetting oAppSetting, IGroupProvider oGroupProvider, INoticeProvider oNoticeProvider, IUserProvider oUserProvider)
        {
            appProvider = oAppProvider;
            connector = oConnector;
            appSetting = oAppSetting;
            logger = Logger.GetLogger(typeof(IntegrationController));
            groupProvider = oGroupProvider;
            noticeProvider = oNoticeProvider;
            userProvider = oUserProvider;
        }

        [Route("SendNoticeOne")]
        [HttpPost]
        public IActionResult SendNoticeOne([FromForm] IntegrationOne integrationOne)
        {
            try
            {
                string echoFileType = EchoFileType.Document;
                string fileName = string.Empty;
                if (integrationOne.FileObject != null && integrationOne.FileObject.Length > 0)
                {
                    if (integrationOne.FileType == 0)
                    {
                        var fileExt = Path.GetExtension(integrationOne.FileObject.FileName);

                        var ImageExtensions = new[] { ".jpg", ".jpeg", ".png" };
                        var videosExtensions = new[] { ".mp4", ".3gp", ".avi", ".mov", ".wmv", ".webm", ".mkv" };

                        if (ImageExtensions.Contains(fileExt))
                        {
                            integrationOne.FileType = 1;
                            echoFileType = EchoFileType.Image;
                        }
                        else if (videosExtensions.Contains(fileExt))
                        {
                            integrationOne.FileType = 2;
                            echoFileType = EchoFileType.Video;
                        }
                        else
                        {
                            integrationOne.FileType = 3;
                            echoFileType = EchoFileType.Document;
                        }
                    }

                    string BinaryFileName = DateTime.UtcNow.ToString("mmddyyyyhhmmssfff") + integrationOne.FileObject.FileName.Replace("\r", string.Empty);
                    fileName = ExcelProcessing.GetFileName(BinaryFileName);
                    var filestream = integrationOne.FileObject.OpenReadStream();
                    AWSHelper.UploadFileToS3(filestream, null, fileName);
                }

                MessageDetails md = new MessageDetails
                {
                    Title = integrationOne.Title,
                    Message = integrationOne.Details,
                    HTMLMessage = integrationOne.DetailsInHTML,
                    GroupName = integrationOne.SenderName,             // this needs to discuss with Amit
                    Files = new List<EchoFile>(),
                    CustomDetails_1 = integrationOne.SenderEmail,
                    CustomDetails_2 = "Notice",
                    CustomDetails_3 = string.Empty,  //this is for push notifications
                    SenderName = integrationOne.SenderName,
                    SenderDetails = ClientEmail
                };
                if (!string.IsNullOrEmpty(fileName))
                {
                    md.Files.Add(new EchoFile() { FileName = fileName, FileType = echoFileType });
                }


                List<string> channels = new List<string>();
                if (integrationOne.IsSMS ?? false)
                {
                    channels.Add(ChannelMaster.SMSChannel);
                }
                if (integrationOne.IsEmail ?? false)
                {
                    channels.Add(ChannelMaster.EmailChannel);
                }
                if (integrationOne.IsWhatsApp ?? false)
                {
                    channels.Add(ChannelMaster.WhatsappChannel);
                }

                logger.Info("SendNoticeOne - Get and Fill appsettings: " + ClientID);
                List<AppSettingsVM> appSettings = appSetting.FillAndGetAppSettings(ClientID);


                if (integrationOne.MobileNo.Length == 12)
                {
                    integrationOne.CustomDetails_1 = integrationOne.MobileNo;
                }
                else if (integrationOne.MobileNo.Length == 10)
                {
                    integrationOne.CustomDetails_1 = $"91{integrationOne.MobileNo}";
                }
                bool isOnWhatsApp = false, isOnSMS = false, isOnEmail = false;
                if (appSettings != null && appSettings.Count > 0)
                {
                    AppSettingsVM setting = appSettings.Where(a => a.Key == SettingMaster.WhatsAppAPI).FirstOrDefault();

                    isOnWhatsApp = setting == null ? false : WhatsAppVerification.VerifyWhatsAppNumber(setting.Value, setting.Value2, integrationOne.MobileNo);
                }

                isOnSMS = MobileVerification.VerifyMobile(integrationOne.MobileNo);

                if (!string.IsNullOrEmpty(integrationOne.EmailID))
                {
                    isOnEmail = EmailVerification.VerifyEmailID(integrationOne.EmailID);
                }
                CreateUser user = new CreateUser();
                user.FirstName = integrationOne.UserName;
                user.EmailID = integrationOne.EmailID;
                user.MobileNo = integrationOne.MobileNo;
                user.IsOnEmail = isOnEmail;
                user.IsOnSms = isOnSMS;
                user.IsOnWhatsApp = isOnWhatsApp;
                userProvider.CreateIntegrationUser(user, ClientID, UserID);
                integrationOne.UserID = user.UserID.ToString();

                int groupID = groupProvider.CheckIntegrationGroupAlreadyExists(integrationOne.UserName);

                if (groupID == 0)
                {
                    GroupDTO groupDTO = new GroupDTO();
                    groupDTO.Name = integrationOne.UserName;
                    groupDTO.Description = integrationOne.CustomDetails_1;
                    groupDTO.IsPrivate = false;
                    groupDTO.CreatedBy = UserID;
                    groupDTO.ClientID = ClientID;
                    List<Guid> lstGUsers = new List<Guid>();
                    lstGUsers.Add(groupDTO.CreatedBy);
                    lstGUsers.Add(user.UserID ?? Guid.Empty);
                    groupProvider.CreateInteGrationGroup(groupDTO, lstGUsers);
                    groupID = groupDTO.GroupID;
                }

                NoticeDetailDTO _notice = new NoticeDetailDTO();
                _notice.NoticeTitle = integrationOne.Title;
                _notice.NoticeDetail = integrationOne.Details;
                _notice.NoticeDetailHTML = integrationOne.DetailsInHTML;
                _notice.GroupID = groupID;
                _notice.CreatedBy = UserID;
                _notice.IsReply = true;
                _notice.IsSms = integrationOne.IsSMS;
                _notice.ParentID = 0;
                _notice.FileType = 0;
                _notice.FileName = null;
                _notice.IsEmail = integrationOne.IsEmail;
                _notice.IsWhatsapp = integrationOne.IsWhatsApp;
                _notice.IsFacebook = false;

                noticeProvider.SaveIntegrationNoticeDetails(_notice);

                List<UserChannelInfo> userChannelInfos = new List<UserChannelInfo>();
                UserChannelDataWithGroup mapping = groupProvider.GetGroupMappingUsers(groupID.ToString(), _notice.ParentID ?? 0, _notice.NoticeDetail);
                userChannelInfos.AddRange(mapping.UserChannelInfoList);

                noticeProvider.SaveIntegrationNotice(userChannelInfos, _notice.NoticeID, groupID, UserID);

                List<UserChannelData> userChannelDatas = userChannelInfos.Select(u => new UserChannelData() { UserID = u.UserID.ToString(), EmailID = u.EmailID, MobileNo = u.MobileNo, UserName = u.UserName.Trim(), CustomDetails_1 = "91" + u.MobileNo, CustomDetails_2 = u.DeviceToken }).ToList();

                foreach (UserChannelData ucd in userChannelDatas)
                {
                    string[] arrName = ucd.UserName.Split(' ');
                    if (arrName != null && arrName.Length > 1)
                    {
                        ucd.DisplayName = arrName[0].ToString().Trim();
                    }
                    else
                    {
                        ucd.DisplayName = ucd.UserName.Trim();
                    }
                }

                logger.Info("SendNoticeOne - CallBroadcaster started");
                ConcurrentBag<ChannelOutput> output = connector.CallBroadcaster(md, channels, userChannelDatas, appSettings);
                return Ok(output);
            }
            catch (Exception ex)
            {
                logger.Error("SendNoticeOne" + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("SendNoticeMulltiple")]
        [HttpPost]
        public IActionResult SendNoticeMulltiple([FromBody] IntegrationMulltiple integrationMulltiple)
        {
            try
            {
                MessageDetails md = new MessageDetails
                {
                    Title = integrationMulltiple.Title,
                    Message = integrationMulltiple.Details,
                    HTMLMessage = integrationMulltiple.DetailsInHTML,
                    GroupName = integrationMulltiple.SenderName,             // this needs to discuss with Amit
                    Files = new List<EchoFile>(),
                    CustomDetails_1 = integrationMulltiple.SenderEmail,
                    CustomDetails_2 = "Notice",
                    CustomDetails_3 = string.Empty,  //this is for push notifications
                    SenderName = integrationMulltiple.SenderName,
                    SenderDetails = ClientEmail
                };

                List<string> channels = new List<string>();
                if (integrationMulltiple.IsSMS ?? false)
                {
                    channels.Add(ChannelMaster.SMSChannel);
                }
                if (integrationMulltiple.IsEmail ?? false)
                {
                    channels.Add(ChannelMaster.EmailChannel);
                }
                if (integrationMulltiple.IsWhatsApp ?? false)
                {
                    channels.Add(ChannelMaster.WhatsappChannel);
                }

                logger.Info("SendNoticeMulltiple - Get and Fill appsettings: " + ClientID);
                List<AppSettingsVM> appSettings = appSetting.FillAndGetAppSettings(ClientID);
                int index = 0;
                bool isOnWhatsApp = false, isOnSMS = false, isOnEmail = false;
                foreach (UserChannelData ucd in integrationMulltiple.userChannelDatas)
                {
                    if (ucd.MobileNo.Length == 12)
                    {
                        ucd.CustomDetails_1 = ucd.MobileNo;
                    }
                    else if (ucd.MobileNo.Length == 10)
                    {
                        ucd.CustomDetails_1 = $"91{ucd.MobileNo}";
                    }

                    if (appSettings != null && appSettings.Count > 0)
                    {
                        AppSettingsVM setting = appSettings.Where(a => a.Key == SettingMaster.WhatsAppAPI).FirstOrDefault();

                        isOnWhatsApp = setting == null ? false : WhatsAppVerification.VerifyWhatsAppNumber(setting.Value, setting.Value2, ucd.MobileNo);
                    }

                    isOnSMS = MobileVerification.VerifyMobile(ucd.MobileNo);

                    if (!string.IsNullOrEmpty(ucd.EmailID))
                    {
                        isOnEmail = EmailVerification.VerifyEmailID(ucd.EmailID);
                    }
                    CreateUser user = new CreateUser();
                    user.FirstName = ucd.UserName;
                    user.EmailID = ucd.EmailID;
                    user.MobileNo = ucd.MobileNo;
                    user.IsOnEmail = isOnEmail;
                    user.IsOnSms = isOnSMS;
                    user.IsOnWhatsApp = isOnWhatsApp;
                    userProvider.CreateIntegrationUser(user, ClientID, UserID);

                    integrationMulltiple.userChannelDatas[index].UserID = user.UserID.ToString();
                    int groupID = groupProvider.CheckIntegrationGroupAlreadyExists(ucd.UserName);
                    if (groupID == 0)
                    {
                        GroupDTO groupDTO = new GroupDTO();
                        groupDTO.Name = ucd.UserName;
                        groupDTO.Description = ucd.CustomDetails_1;
                        groupDTO.IsPrivate = false;
                        groupDTO.CreatedBy = UserID;
                        groupDTO.ClientID = ClientID;

                        List<Guid> lstGUsers = new List<Guid>();
                        lstGUsers.Add(UserID);
                        lstGUsers.Add(user.UserID ?? Guid.Empty);
                        groupProvider.CreateInteGrationGroup(groupDTO, lstGUsers);
                        groupID = groupDTO.GroupID;
                    }

                    NoticeDetailDTO _notice = new NoticeDetailDTO();
                    _notice.NoticeTitle = integrationMulltiple.Title;
                    _notice.NoticeDetail = integrationMulltiple.Details;
                    _notice.NoticeDetailHTML = integrationMulltiple.DetailsInHTML;
                    _notice.GroupID = groupID;
                    _notice.CreatedBy = UserID;
                    _notice.IsReply = true;
                    _notice.IsSms = integrationMulltiple.IsSMS;
                    _notice.ParentID = 0;
                    _notice.FileType = 0;
                    _notice.FileName = null;
                    _notice.IsEmail = integrationMulltiple.IsEmail;
                    _notice.IsWhatsapp = integrationMulltiple.IsWhatsApp;
                    _notice.IsFacebook = false;

                    noticeProvider.SaveIntegrationNoticeDetails(_notice);

                    List<UserChannelInfo> userChannelInfos = new List<UserChannelInfo>();
                    UserChannelDataWithGroup mapping = groupProvider.GetGroupMappingUsers(groupID.ToString(), _notice.ParentID ?? 0, _notice.NoticeDetail);
                    userChannelInfos.AddRange(mapping.UserChannelInfoList);

                    noticeProvider.SaveIntegrationNotice(userChannelInfos, _notice.NoticeID, groupID, UserID);
                    index++;

                    List<UserChannelData> userChannelDatas = userChannelInfos.Select(u => new UserChannelData() { UserID = u.UserID.ToString(), EmailID = u.EmailID, MobileNo = u.MobileNo, UserName = u.UserName.Trim(), CustomDetails_1 = "91" + u.MobileNo, CustomDetails_2 = u.DeviceToken }).ToList();

                    string[] arrName = ucd.UserName.Split(' ');
                    if (arrName != null && arrName.Length > 1)
                    {
                        ucd.DisplayName = arrName[0].ToString().Trim();
                    }
                    else
                    {
                        ucd.DisplayName = ucd.UserName.Trim();
                    }
                    logger.Info("SendNoticeMulltiple - CallBroadcaster started");

                    ConcurrentBag<ChannelOutput> output = connector.CallBroadcaster(md, channels, userChannelDatas, appSettings);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                logger.Error("SendNoticeMulltiple" + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }
    }
}