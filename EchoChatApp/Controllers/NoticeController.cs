﻿using EchoChat.Channel;
using EchoChat.ChannelConnector;
using EchoChat.Constants;
using EchoChat.Helper;
using EchoChat.Models;
using EchoChat.ServiceProvider;
using EchoChatApp.General;
using EchoChatApp.Helper;
using EchoChatApp.Hubs;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EchoChatApp.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class NoticeController : BaseController
    {
        private INoticeProvider noticeProvider;
        private IGroupProvider groupProvider;
        private IAppSetting appSetting;
        private IConnector connector;
        private IUserProvider userProvider;
        private readonly IHubContext<ChatHub> hubContext;
        private ILog logger;
        private readonly AppSettingsFromFile settingsFromFile;
        public NoticeController(IHubContext<ChatHub> oHubContext, INoticeProvider oNoticeProvider, IGroupProvider oGroupProvider, IConnector oConnector, IAppSetting oAppSetting, IUserProvider oUserProvider, IOptions<AppSettingsFromFile> oSettingsFromFile)
        {
            noticeProvider = oNoticeProvider;
            groupProvider = oGroupProvider;
            connector = oConnector;
            hubContext = oHubContext;
            appSetting = oAppSetting;
            userProvider = oUserProvider;
            logger = Logger.GetLogger(typeof(NoticeController));
            settingsFromFile = oSettingsFromFile.Value;
        }

        [Route("AddNotice")]
        [HttpPost]
        public IActionResult AddNotice([FromForm] NoticeDTO notice)
        {
            try
            {
                if (string.IsNullOrEmpty(notice.DeviceID))
                {
                    notice.DeviceID = string.Empty;
                }

                int IsReply = 0, IsSms = 0, ParentID = 0, FileType = 0, GroupID = 0, IsEmail = 0, IsWhatsapp = 0, IsFacebook = 0, BroadcastToAllChannel = 0, IsSlack = 0;
                string echoFileType = EchoFileType.Document;

                if (!string.IsNullOrEmpty(notice.IsReply))
                {
                    IsReply = Convert.ToInt32(notice.IsReply);
                }
                if (!string.IsNullOrEmpty(notice.IsSlack))
                {
                    IsSlack = Convert.ToInt32(notice.IsSlack);
                }
                if (!string.IsNullOrEmpty(notice.IsSms))
                {
                    IsSms = Convert.ToInt32(notice.IsSms);
                }
                if (!string.IsNullOrEmpty(notice.ParentID))
                {
                    ParentID = Convert.ToInt32(notice.ParentID);
                }
                if (!string.IsNullOrEmpty(notice.FileType))
                {
                    FileType = Convert.ToInt32(notice.FileType);
                }
                if (!string.IsNullOrEmpty(notice.GroupID))
                {
                    GroupID = Convert.ToInt32(notice.GroupID);
                }
                if (!string.IsNullOrEmpty(notice.IsEmail))
                {
                    IsEmail = Convert.ToInt32(notice.IsEmail);
                }
                if (!string.IsNullOrEmpty(notice.IsWhatsapp))
                {
                    IsWhatsapp = Convert.ToInt32(notice.IsWhatsapp);
                }
                if (!string.IsNullOrEmpty(notice.IsFacebook))
                {
                    IsFacebook = Convert.ToInt32(notice.IsFacebook);
                }

                if (!string.IsNullOrEmpty(notice.BroadcastToAllChannel))
                {
                    BroadcastToAllChannel = Convert.ToInt32(notice.BroadcastToAllChannel);
                    if (ParentID != 0 && BroadcastToAllChannel == 1)
                    {
                        NoticeDetailDTO nd = noticeProvider.GetNoticeDetailsByNoticeID(ParentID);
                        IsSms = nd.IsSms.HasValue && nd.IsSms.Value ? 1 : 0;
                        IsEmail = nd.IsEmail.HasValue && nd.IsEmail.Value ? 1 : 0;
                        IsWhatsapp = nd.IsWhatsapp.HasValue && nd.IsWhatsapp.Value ? 1 : 0;
                        IsSlack = nd.IsSlack.HasValue && nd.IsSlack.Value ? 1 : 0;
                        notice.NoticeTitle = "RE: " + nd.NoticeTitle;
                    }
                }

                if (string.IsNullOrEmpty(notice.NoticeDetail) && !string.IsNullOrEmpty(notice.NoticeDetailHTML))
                {
                    notice.NoticeDetail = Regex.Replace(notice.NoticeDetailHTML, "<.*?>", String.Empty);
                    notice.NoticeDetail = notice.NoticeDetail.Replace("&nbsp;", " ");
                }

                if (ParentID > 0 && BroadcastToAllChannel == 1 && IsEmail == 1)
                {
                    notice.NoticeDetailHTML = notice.NoticeDetail;
                }

                Guid createdBy = UserID;
                int clientID = ClientID;

                if (ParentID > 0 && UserName.Equals("WebHook"))
                {
                    createdBy = Guid.Parse(notice.Createdby);
                    UserProfile uModel = userProvider.GetUserByID(createdBy);
                    clientID = uModel.ClientID;
                }

                string filePath = string.Empty;
                List<UserChannelInfo> userChannelInfos = new List<UserChannelInfo>();
                string groupName = null;
                NoticeDetailDTO _notice = new NoticeDetailDTO();

                try
                {
                    _notice = new NoticeDetailDTO();
                    _notice.NoticeTitle = notice.NoticeTitle;
                    _notice.NoticeDetail = notice.NoticeDetail;
                    _notice.NoticeDetailHTML = notice.NoticeDetailHTML;
                    _notice.GroupID = GroupID;
                    _notice.CreatedBy = createdBy;
                    _notice.IsReply = IsReply == 1;
                    _notice.IsSms = IsSms == 1;
                    _notice.ParentID = ParentID;
                    _notice.FileType = FileType;
                    _notice.FileName = notice.FileName;
                    _notice.IsEmail = IsEmail == 1;
                    _notice.IsWhatsapp = IsWhatsapp == 1;
                    _notice.IsFacebook = IsFacebook == 1;
                     _notice.IsSlack = IsSlack == 1;
                    _notice.Date = notice.Date;
                    _notice.EndDate = notice.EndDate;
                    _notice.Time = notice.Time;
                    _notice.Days = notice.Days;
                    _notice.IsRecursive = notice.IsRecursive;

                    if (!string.IsNullOrEmpty(_notice.FileName) && _notice.FileType > 0)
                    {
                        if (_notice.FileType == 1)
                        {
                            echoFileType = EchoFileType.Image;
                        }
                        else if (_notice.FileType == 2)
                        {
                            echoFileType = EchoFileType.Video;
                        }
                        else if (_notice.FileType == 3)
                        {
                            echoFileType = EchoFileType.Document;
                        }
                    }
                    else
                    {
                        if (notice.StreamWithData != null && notice.StreamWithData.Length > 0)
                        {
                            if (_notice.FileType == 0)
                            {
                                var fileExt = Path.GetExtension(notice.StreamWithData.FileName);

                                var ImageExtensions = new[] { ".jpg", ".jpeg", ".png" };
                                var videosExtensions = new[] { ".mp4", ".3gp", ".avi", ".mov", ".wmv", ".webm", ".mkv" };

                                if (ImageExtensions.Contains(fileExt))
                                {
                                    _notice.FileType = 1;
                                    echoFileType = EchoFileType.Image;
                                }
                                else if (videosExtensions.Contains(fileExt))
                                {
                                    _notice.FileType = 2;
                                    echoFileType = EchoFileType.Video;
                                }
                                else
                                {
                                    _notice.FileType = 3;
                                    echoFileType = EchoFileType.Document;
                                }
                            }

                            string BinaryFileName = DateTime.UtcNow.ToString("mmddyyyyhhmmssfff") + notice.StreamWithData.FileName.Replace("\r", string.Empty);
                            _notice.FileName = ExcelProcessing.GetFileName(BinaryFileName);
                            var filestream = notice.StreamWithData.OpenReadStream();
                            AWSHelper.UploadFileToS3(filestream, null, _notice.FileName);
                        }
                    }

                    logger.Info("_notice save start");
                    noticeProvider.SaveNoticeDetails(_notice);
                    logger.Info("_notice save end");

                    UserChannelDataWithGroup mapping = groupProvider.GetGroupMappingUsers(notice.GroupID, _notice.ParentID ?? 0, _notice.NoticeDetail);
                    userChannelInfos.AddRange(mapping.UserChannelInfoList);
                    groupName = mapping.GroupName;

                    logger.Info("signalR AddToGroupAsync");

                    userChannelInfos = userChannelInfos.Distinct().ToList();
                    noticeProvider.SaveNotice(userChannelInfos, _notice.NoticeID, GroupID, createdBy);
                }
                catch (Exception ex)
                {
                    logger.Error("SaveNotice: " + ex.Message);
                    return BadRequest(new { Message = ex.Message });
                }


                NoticeSignalRData _noti = new NoticeSignalRData();
                _noti.NoticeID = _notice.NoticeID;
                _noti.FileName = _notice.FileName;
                _noti.GroupID = GroupID;
                _noti.NoticeTitle = _notice.NoticeTitle;
                _noti.NoticeDetail = _notice.NoticeDetail;
                _noti.ParentID = _notice.ParentID;
                _noti.UserID = createdBy;

                _noti.FilePath = AWSHelper.GetPresignedFileURL(_noti.FileName);
                _noti.NoticeDate = DateTime.UtcNow.AddMinutes(TimeZoneOffset).ToString("dd-MM-yyyy hh:mm:ss tt");
                string noticeData = JsonConvert.SerializeObject(new { notice = _noti });

                if (_notice.ParentID == 0 || BroadcastToAllChannel == 1)
                {
                    try
                    {
                        if (!notice.IsScheduled.HasValue || !notice.IsScheduled.Value)
                        {
                            logger.Info("Get and Fill appsettings: " + clientID);
                            List<AppSettingsVM> appSettings = appSetting.FillAndGetAppSettings(clientID);
                            logger.Info("SendOnChannelAsync started");

                            MessageDetails md = new MessageDetails
                            {
                                Title = _notice.NoticeTitle,
                                Message = _notice.NoticeDetail,
                                HTMLMessage = _notice.NoticeDetailHTML,
                                GroupName = groupName,
                                Files = new List<EchoFile>(),
                                CustomDetails_1 = ClientEmail,
                                CustomDetails_2 = "Notice",
                                CustomDetails_3 = noticeData,
                                SenderName = ClientName,
                                SenderDetails = ClientEmail,
                                TemplateID = notice.TemplateID
                            };
                            if (!string.IsNullOrEmpty(_notice.FileName))
                            {
                                md.Files.Add(new EchoFile() { FileName = _notice.FileName, FileType = echoFileType });
                            }

                            List<string> channels = new List<string>();
                            if (IsSms == 1)
                            {
                                channels.Add(ChannelMaster.SMSChannel);
                            }
                            if (IsEmail == 1)
                            {
                                channels.Add(ChannelMaster.EmailChannel);
                            }
                            if (IsWhatsapp == 1)
                            {
                                if (string.IsNullOrEmpty(settingsFromFile.WhatsAppProvider))
                                {
                                    channels.Add(ChannelMaster.WhatsappChannel);
                                }
                                else
                                {
                                    channels.Add(ChannelMaster.WhatsappKarixChannel);
                                }
                            }
                            if (IsSlack == 1)
                            {
                                channels.Add(ChannelMaster.SlackChannel);
                            }
                            //if (IsSlack == 1)
                            //{
                            //    channels.Add(ChannelMaster.SlackChannel);
                            //}
                            List<UserChannelData> userChannelDatas = userChannelInfos.Select(u => new UserChannelData() { UserID = u.UserID.ToString(), EmailID = u.EmailID, MobileNo = u.MobileNo, UserName = u.UserName.Trim(), CustomDetails_1 = "91" + u.MobileNo, CustomDetails_2 = u.DeviceToken, SlackID = u.SlackID }).ToList();

                            foreach (UserChannelData ucd in userChannelDatas)
                            {
                                string[] arrName = ucd.UserName.Split(' ');
                                if (arrName != null && arrName.Length > 1)
                                {
                                    ucd.DisplayName = arrName[0].ToString().Trim();
                                }
                                else
                                {
                                    ucd.DisplayName = ucd.UserName.Trim();
                                }
                            }
                            List<WhatsAppParameterData> whatsAppParameters = userProvider.GetWhatsAppParameters(notice.TemplateID);
                            
                            if (channels.Count > 0)
                            {
                                ConcurrentBag<ChannelOutput> output = connector.CallBroadcaster(md, channels, userChannelDatas, appSettings, whatsAppParameters);
                                noticeProvider.SaveNoticeOutput(_notice.NoticeID, clientID, output);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error("SendOnChannelAsync: " + ex.Message);
                        return BadRequest(new { Message = ex.Message });
                    }
                }

                return Ok(new { _noti });
            }
            catch (Exception ex)
            {
                logger.Error("AddNotice: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("SendPendingNotices")]
        [HttpPost]
        public IActionResult SendPendingNotices(int groupID, int noticeID, bool isSMS = false, bool isEmail = false, bool isWA = false)
        {
            try
            {
                string echoFileType = EchoFileType.Document;
                NoticeDetailDTO _notice = noticeProvider.GetNoticeDetailsByNoticeID(noticeID);
                GroupDTO group = groupProvider.GetGroupByID(groupID);
                string noticeData = JsonConvert.SerializeObject(new { notice = _notice });

                List<UserChannelInfo> userChannelInfos = groupProvider.GetUsersForPendingNotice(noticeID, isSMS, isEmail, isWA);

                try
                {
                    logger.Info("Get and Fill appsettings: " + ClientID);
                    List<AppSettingsVM> appSettings = appSetting.FillAndGetAppSettings(ClientID);
                    logger.Info("SendOnChannelAsync started");

                    MessageDetails md = new MessageDetails
                    {
                        Title = _notice.NoticeTitle,
                        Message = _notice.NoticeDetail,
                        HTMLMessage = _notice.NoticeDetailHTML,
                        GroupName = group.Name,
                        Files = new List<EchoFile>(),
                        CustomDetails_1 = ClientEmail,
                        CustomDetails_2 = "Notice",
                        CustomDetails_3 = noticeData,
                        SenderName = ClientName,
                        SenderDetails = ClientEmail
                    };
                    if (!string.IsNullOrEmpty(_notice.FileName))
                    {
                        md.Files.Add(new EchoFile() { FileName = _notice.FileName, FileType = echoFileType });
                    }

                    List<string> channels = new List<string>();
                    if (_notice.IsSms ?? false)
                    {
                        channels.Add(ChannelMaster.SMSChannel);
                    }
                    if (_notice.IsEmail ?? false)
                    {
                        channels.Add(ChannelMaster.EmailChannel);
                    }
                    if (_notice.IsWhatsapp ?? false)
                    {
                        channels.Add(ChannelMaster.WhatsappChannel);
                    }

                    List<UserChannelData> userChannelDatas = userChannelInfos.Select(u => new UserChannelData() { UserID = u.UserID.ToString(), EmailID = u.EmailID, MobileNo = u.MobileNo, UserName = u.UserName.Trim(), CustomDetails_1 = "91" + u.MobileNo, CustomDetails_2 = u.DeviceToken }).ToList();

                    foreach (UserChannelData ucd in userChannelDatas)
                    {
                        string[] arrName = ucd.UserName.Split(' ');
                        if (arrName != null && arrName.Length > 1)
                        {
                            ucd.DisplayName = arrName[0].ToString().Trim();
                        }
                        else
                        {
                            ucd.DisplayName = ucd.UserName.Trim();
                        }
                    }

                    if (channels.Count > 0)
                    {
                        ConcurrentBag<ChannelOutput> output = connector.CallBroadcaster(md, channels, userChannelDatas, appSettings);
                        noticeProvider.SaveNoticeOutput(noticeID, ClientID, output);
                    }

                    return Ok();
                }
                catch (Exception ex)
                {
                    logger.Error("SendOnChannelAsync: " + ex.Message);
                    return BadRequest(new { Message = ex.Message });
                }
            }
            catch (Exception ex)
            {
                logger.Error("AddNotice: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("GetNoticeListByGroupID")]
        [HttpGet]
        public IActionResult GetNoticeListByGroupID(int groupID, int parentID, int pageNo = 1, int pageSize = 50, string freeText = null)
        {
            try
            {
                NoticeSmallDataModel result = noticeProvider.GetNoticeListByGroupID(groupID, parentID, pageNo, pageSize, UserID, freeText, TimeZoneOffset);

                result.NoticeSmallDatas.ForEach(n => n.FilePath = AWSHelper.GetPresignedFileURL(n.FileName));

                return Ok(new { result });
            }
            catch (Exception ex)
            {
                logger.Error("GetNoticeListByGroupID: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("DeleteNotice")]
        [HttpPost]
        public IActionResult DeleteNotice(int noticeID)
        {
            try
            {
                List<DeletedNotice> notice = noticeProvider.DeleteNotice(noticeID);

                IDictionary<string, int> dict = new Dictionary<string, int>()
                        {
                            {"DeletedNoticeId", noticeID }
                        };
                foreach (DeletedNotice dn in notice)
                {
                    //PushNotificationHelper.SendPushDataAndroid(dn.DeviceToken, dict);
                }

                return Ok(new { DeleteNoticeResult = 1 });

            }
            catch (Exception ex)
            {
                logger.Error("DeleteNotice: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetFile")]
        public async Task<IActionResult> GetFile(string fileName)
        {
            try
            {
                byte[] myfile = await new WebClient().DownloadDataTaskAsync(new Uri(AWSHelper.GetPresignedFileURL(fileName)));
                return File(myfile, "image/png", fileName);
            }
            catch (Exception ex)
            {
                logger.Error("GetFile: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("DownloadFile")]
        public async Task<IActionResult> DownloadFile(string fileName)
        {
            try
            {
                byte[] myfile = await new WebClient().DownloadDataTaskAsync(new Uri(AWSHelper.GetPresignedFileURL(fileName)));
                string mimeType = MimeKit.MimeTypes.GetMimeType(fileName);
                return File(myfile, "image/png", fileName);
            }
            catch (Exception ex)
            {
                logger.Error("DownloadFile: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetFileURL")]
        public IActionResult GetFileURL(string fileName)
        {
            try
            {
                string fileURL = AWSHelper.GetPresignedFileURL(fileName);
                return Ok(fileURL);
            }
            catch (Exception ex)
            {
                logger.Error("GetFileURL: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("SeenNotice")]
        [HttpPost]
        public IActionResult SeenNotice(SeenNotice seen)
        {
            try
            {
                noticeProvider.SeenNotice(seen.NoticeID, UserID, "Read");
                return Ok();
            }
            catch (Exception ex)
            {
                logger.Error("SeenNotice: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("GetScheduledNotices")]
        [HttpGet]
        public IActionResult GetScheduledNotices(DateTime fromDate, DateTime toDate)
        {
            try
            {
                List<ScheduledNotices> scheduledNotices = noticeProvider.GetScheduledNotices(fromDate, toDate, TimeZoneOffset);
                return Ok(new { scheduledNotices });
            }
            catch (Exception ex)
            {
                logger.Error("GetScheduledNotices: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("GetNoticeDetailsByNoticeID")]
        [HttpGet]
        public IActionResult GetNoticeDetailsByNoticeID(int noticeID)
        {
            try
            {
                NoticeDetailDTO noticeDetails = noticeProvider.GetNoticeDetailsByNoticeID(noticeID);
                return Ok(new { noticeDetails });
            }
            catch (Exception ex)
            {
                logger.Error("GetNoticeDetailsByNoticeID: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetMemberUploadFileFormat")]
        public async Task<IActionResult> GetMemberUploadFileFormat()
        {
            try
            {
                byte[] myfile = await new WebClient().DownloadDataTaskAsync(new Uri(AWSHelper.GetPresignedFileURL("MemberUploadFormat.xlsx")));
                return File(myfile, "image/png", "MemberUploadFormat.xlsx");
            }
            catch (Exception ex)
            {
                logger.Error("GetNoticeDetailsByNoticeID: " + ex.Message);
                return BadRequest(new { error = ex.Message });
            }
        }


        [Route("AnalyzeNoticeDetails")]
        [HttpPost]
        public IActionResult AnalyzeNoticeDetails([FromBody] NoticeAnalyzer noticeAnalyzer)
        {
            try
            {
                string body = string.Empty;
                if (!string.IsNullOrEmpty(noticeAnalyzer.Text))
                {
                    body = Regex.Replace(noticeAnalyzer.Text, "<.*?>", String.Empty);
                }
                MemoryStream pdfStream = PDFHelper.GetPdf(noticeAnalyzer.Title, noticeAnalyzer.Text, body, DateTime.UtcNow.AddMinutes(TimeZoneOffset).ToString("dd MMM yyyy hh:mm tt"));
                byte[] output = pdfStream.ToArray();
                return File(output, "application/pdf", "EchoAppTextAnalyzerReport.pdf");
            }
            catch (Exception ex)
            {
                logger.Error("AnalyzeNoticeDetails: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("GetConfigWhatsAppTemplate")]
        [HttpGet]
        public IActionResult GetConfigWhatsAppTemplate()
        {
            try
            {
                List<string> result = noticeProvider.GetConfigWhatsAppTemplate();
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetConfigWhatsAppTemplate" + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }
    }
}