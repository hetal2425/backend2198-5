﻿using EchoChat.ChannelConnector;
using EchoChat.ChannelVerification;
using EchoChat.Constants;
using EchoChat.Helper;
using EchoChat.Models;
using EchoChat.ServiceProvider;
using EchoChatApp.General;
using EchoChatApp.Helper;
using EchoChatApp.Hubs;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EchoChatApp.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class UserController : BaseController
    {
        private IUserProvider userProvider;
        private IConnector connector;
        private IAppSetting appSetting;
        private ILog logger;
        private readonly AppSettingsFromFile settingsFromFile;
        private IClientProvider clientProvider;
        private IGroupProvider groupProvider;
        private readonly IHubContext<ChatHub> _hubContext;
        ChatHub _chatHub;
        public UserController(IHubContext<ChatHub> hubContext, IUserProvider oUserProvider, IConnector oConnector, IAppSetting oAppSetting, IOptions<AppSettingsFromFile> oSettingsFromFile, IClientProvider clientProvider, IGroupProvider oGroupProvider)
        {
            userProvider = oUserProvider;
            connector = oConnector;
            appSetting = oAppSetting;
            _hubContext = hubContext;
            logger = Logger.GetLogger(typeof(UserController));
            settingsFromFile = oSettingsFromFile.Value;
            this.clientProvider = clientProvider;
            groupProvider = oGroupProvider;
        }

        [HttpPost("UploadExcelFileUsers")]
        public async Task<IActionResult> UploadExcelFileUsersAsync([FromForm] ExcelUpload excel)
        {
            try
            {
                if (excel.ExcelFile != null && excel.ExcelFile.Length > 0)
                {
                    string BinaryFileName = DateTime.UtcNow.AddHours(5.5).ToString("mmddyyyyhhmmss") + excel.ExcelFile.FileName.Replace("\r", "");
                    string FileName = ExcelProcessing.GetFileName(BinaryFileName);

                    var filestream = excel.ExcelFile.OpenReadStream();
                    AWSHelper.UploadFileToS3(filestream, null, FileName);

                    ExcelProcessing file = new ExcelProcessing();
                    int failedCount = 0;
                    var list = file.ReadAndProcessExcel(FileName, out failedCount);

                    if (list != null && list.Count > 0)
                    {
                        int cnt = 0;
                        decimal totChunks = Math.Ceiling((decimal)list.Count / 500);

                        for (int i = 0; i < totChunks; i++)
                        {
                            List<ExcelUserData> userList = list.Skip(cnt).Take(list.Skip(cnt).Count() > 500 ? 500 : list.Skip(cnt).Count()).ToList();

                            ExcelUsers excelUsers = new ExcelUsers
                            {
                                UserList = userList,
                                ListID = excel.ListID,
                                ListName = excel.ListName
                            };
                            //Thread.Sleep(5000);
                            excel.ListID = userProvider.SubmitUsers(excelUsers, UserID, ClientID);
                            cnt += userList.Count;
                            decimal percentage = Math.Ceiling((decimal)cnt * 100 / list.Count);

                            _chatHub = new ChatHub();
                            await _chatHub.UserUploadProgress(_hubContext, UserID.ToString(), percentage);
                        }
                        int successCount = list.Count - failedCount;
                        return Ok(new { SuccessCount = successCount, FailedCount = failedCount });
                    }
                    else
                    {
                        return BadRequest(new { Message = "FAILED" });
                    }
                }
                else
                {
                    logger.Info("UploadExcelFileUsers: Invalid File Format");
                    return BadRequest(new { messagev = "Invalid File Format" });
                }
            }
            catch (Exception ex)
            {
                logger.Error("UploadExcelFileUsers: " + ex.Message);
                return BadRequest(new
                {
                    msg = ex.Message
                });
            }
        }

        //[HttpPost("SubmitUsers")]
        //public IActionResult SubmitUsers(ExcelUsers list)
        //{
        //    try
        //    {
        //        int AddedUser = 0;
        //        if (list != null && list.UserList != null && list.UserList.Count > 0)
        //        {
        //            bool result = userProvider.SubmitUsers(list);
        //            if (result)
        //            {
        //                return Ok(new { Message = "SUCCESS", AddedUser = AddedUser });
        //            }
        //            else
        //            {
        //                return Ok(new { Message = "FAILED" });
        //            }
        //        }
        //        else
        //        {
        //            return Ok(new { Message = "FAILED" });
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error("SubmitUsers: " + ex.Message);
        //        return BadRequest(new { Message = ex.Message });
        //    }
        //}

        [Route("CreateUpdateUser")]
        [HttpPost]
        public IActionResult CreateUpdateUser(CreateUser user)
        {
            try
            {
                if (string.IsNullOrEmpty(user.FirstName) || string.IsNullOrEmpty(user.FirstName.Trim()))
                {
                    return BadRequest(new { Message = "Please enter user name" });
                }

                if (string.IsNullOrEmpty(user.MobileNo) == true)
                {
                    return BadRequest(new { Message = "Please enter mobile number" });
                }

                List<AppSettingsVM> appSettings = appSetting.FillAndGetAppSettings(ClientID);
                if (appSettings != null && appSettings.Count > 0)
                {
                    AppSettingsVM setting = appSettings.Where(a => a.Key == SettingMaster.WhatsAppAPI).FirstOrDefault();

                    bool isOnWhatsApp = setting == null ? false : WhatsAppVerification.VerifyWhatsAppNumber(setting.Value, setting.Value2, user.MobileNo);
                    user.IsOnWhatsApp = isOnWhatsApp;
                }
                bool isOnSMS = MobileVerification.VerifyMobile(user.MobileNo);
                user.IsOnSms = isOnSMS;

                if (!string.IsNullOrEmpty(user.EmailID))
                {
                    user.IsOnEmail = EmailVerification.VerifyEmailID(user.EmailID);
                }
                if (user.ProfilePhotoFile != null)
                {
                    string BinaryFileName = DateTime.UtcNow.ToString("mmddyyyyhhmmssfff") + user.ProfilePhotoFile.FileName.Replace("\r", string.Empty);
                    user.ProfilePhoto = BinaryFileName;
                    var filestream = user.ProfilePhotoFile.OpenReadStream();
                    AWSHelper.AddClientLogo(filestream, null, BinaryFileName);
                }

                bool result = userProvider.CreateUpdateUser(user, ClientID, UserID);
                return Ok(new { user });
            }
            catch (Exception ex)
            {
                logger.Error("CreateUpdateUser: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpPost("DeleteUser")]
        public IActionResult DeleteUser(Guid userID)
        {
            try
            {
                bool result = userProvider.DeleteUser(userID);
                return Ok(new { result });
            }
            catch (Exception ex)
            {
                logger.Error("DeleteUser: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [AllowAnonymous]
        [Route("GetOTPForgotPassword")]
        [HttpPost]
        public IActionResult GetOTPForgotPassword(string userName, string recoveryNo)
        {
            try
            {
                CreateUser user = userProvider.GetUserByUsername(userName);

                if (user != null)
                {
                    Random random = new Random();
                    string OTP = random.Next(1001, 9999).ToString();
                    SendOTPSMS(userName, "The OTP is " + OTP.ToString());
                    return Ok(new { GetOTPForgotPasswordResult = OTP + " " + user.UserID });//OTP Send successful
                }

                return BadRequest();
            }
            catch (Exception ex)
            {
                logger.Error("GetOTPForgotPassword: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }
        private void SendOTPSMS(string mobileNo, string msg)
        {
            MessageDetails md = new MessageDetails();
            md.Message = msg;
            md.SenderDetails = AWSHelper.sender;

            List<string> channels = new List<string>();
            channels.Add(ChannelMaster.SMSChannel);

            List<UserChannelData> userChannelDatas = new List<UserChannelData>();
            userChannelDatas.Add(new UserChannelData() { UserID = string.Empty, MobileNo = mobileNo });

            connector.CallBroadcaster(md, channels, userChannelDatas, new List<AppSettingsVM>());
        }

        [Route("FirstTimeUpdatePassword")]
        [HttpPost]
        public IActionResult FirstTimeUpdatePassword(UpdatePassword update)
        {
            try
            {
                update.Password = Encryption.Encrypt(update.Password);
                bool result = userProvider.FirstTimeUpdatePassword(update);
                return Ok(new { result });
            }
            catch (Exception ex)
            {
                logger.Error("FirstTimeUpdatePassword: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [AllowAnonymous]
        [Route("UpdatePassword")]
        [HttpPost]
        public IActionResult UpdatePassword(UpdatePassword update)
        {
            try
            {
                update.Password = Encryption.Encrypt(update.Password);
                bool result = userProvider.UpdatePassword(update);
                return Ok(new { result });
            }
            catch (Exception ex)
            {
                logger.Error("UpdatePassword: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("SetGroupAdmin")]
        [HttpPost]
        public IActionResult SetGroupAdmin(GroupAdminMapping mapping)
        {
            try
            {
                mapping.CreatedBy = UserID;
                bool result = userProvider.SetGroupAdmin(mapping);
                return Ok(new { result });
            }
            catch (Exception ex)
            {
                logger.Error("SetGroupAdmin: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("GetUsersByListIDs")]
        [HttpPost]
        public IActionResult GetUsersByListIDs(List<int> listIDs)
        {
            try
            {
                List<UserSmallModel> result = userProvider.GetUsersByListIDs(listIDs);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetUsersByListIDs: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("UpdateIntroHappened")]
        [HttpPost]
        public IActionResult UpdateIntroHappened()
        {
            try
            {
                bool result = userProvider.UpdateIntroHappened(UserID);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("UpdateIntroHappened: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("UpdateUserPersonalInfo")]
        [HttpPost]
        public IActionResult UpdateUserPersonalInfo(UserPersonalInfoModel user)
        {
            try
            {
                if (string.IsNullOrEmpty(user.FirstName) || string.IsNullOrEmpty(user.FirstName.Trim()))
                {
                    return BadRequest(new { Message = "Please enter user name" });
                }
                else if (string.IsNullOrEmpty(user.MobileNo))
                {
                    return BadRequest(new { Message = "Please enter mobile number" });
                }
                else if (string.IsNullOrEmpty(user.EmailID))
                {
                    return BadRequest(new { Message = "Please enter email ID" });
                }

                bool result = userProvider.UpdateUserPersonalInfo(user, UserID);
                return Ok(new { result });
            }
            catch (Exception ex)
            {
                logger.Error("UpdateUserPersonalInfo: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("UpdateUserPassword")]
        [HttpPost]
        public IActionResult UpdateUserPassword(UserPasswordModel user)
        {
            try
            {
                if (string.IsNullOrEmpty(user.OldPassword))
                {
                    return BadRequest(new { Message = "Old Password is required" });
                }
                else if (string.IsNullOrEmpty(user.NewPassword))
                {
                    return BadRequest(new { Message = "New Password is required" });
                }
                else if (string.IsNullOrEmpty(user.ConfirmPassword))
                {
                    return BadRequest(new { Message = "Confirm Password is required" });
                }
                string oldPassword = Encryption.Decrypt(userProvider.GetUserPasswordByID(UserID));

                if (oldPassword != user.OldPassword)
                {
                    return BadRequest(new { Message = "Old Password is incorrect" });
                }
                else if (user.NewPassword != user.ConfirmPassword)
                {
                    return BadRequest(new { Message = "New password & Confirm Password should be same" });
                }

                user.ConfirmPassword = Encryption.Encrypt(user.ConfirmPassword);
                user.IsPasswordUpdated = true;
                bool result = userProvider.UpdateUserPassword(user, UserID);
                return Ok(new { result });
            }
            catch (Exception ex)
            {
                logger.Error("UpdateUserPassword: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("SaveUpdateSetting")]
        [HttpPost]
        public IActionResult SaveUpdateSetting(AppSettingModel appSetting)
        {
            try
            {
                bool result = userProvider.SaveUpdateSetting(appSetting);
                if (appSetting.EntityName == "Client" && appSetting.EntityId != 0 && appSetting.Key == SettingMaster.SlackAppSettings)
                    SaveClientSlackId(appSetting.EntityId ?? 0, appSetting.Value1);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("SaveUpdateSetting" + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }
        private bool SaveClientSlackId(int ClientId, string SlackToken)
        {
            var email = (clientProvider.GetClientEmailById(ClientId) ?? "");
            var dict = new Dictionary<string, string>();
            dict.Add("email", email);
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", SlackToken);
            var req = new HttpRequestMessage(HttpMethod.Post, "https://slack.com/api/users.lookupByEmail") { Content = new FormUrlEncodedContent(dict) };
            var result = client.SendAsync(req).Result;
            if (result.IsSuccessStatusCode)
            {
                var resultString = result.Content.ReadAsStringAsync().Result;
                dynamic returnedUser = JObject.Parse(resultString);
                var slackId = Convert.ToString(returnedUser.user.id);
                CreateUser user = userProvider.GetUserByUsername(email);
                if (user != null && (!string.IsNullOrWhiteSpace(slackId)))
                {
                    user.IsOnSlack = true;
                    user.SlackId = slackId;
                    userProvider.CreateUpdateUser(user, ClientId, user.UserID ?? Guid.NewGuid());
                }
            }
            return true;
        }
        [Route("DeleteSetting")]
        [HttpPost]
        public IActionResult DeleteSetting(int appSettingId)
        {
            try
            {
                bool result = userProvider.DeleteSetting(appSettingId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("DeleteSetting" + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("GetAppSettingByChannel")]
        [HttpGet]
        public IActionResult GetAppSettingByChannel(String channel)
        {
            try
            {
                if (!string.IsNullOrEmpty(settingsFromFile.WhatsAppProvider) && channel == ChannelMaster.WhatsappChannel)
                {
                    channel = "WhatsappKarixChannel";
                }
                List<AppSettingModel> result = userProvider.GetAppSettingByChannel(channel);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetAppSettingByChannel" + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("GetSettingKeys")]
        [HttpGet]
        public IActionResult GetSettingKeys()
        {
            try
            {
                bool IsKarix = false;
                if (!string.IsNullOrEmpty(settingsFromFile.WhatsAppProvider))
                {
                    IsKarix = true;
                }
                List<string> result = userProvider.GetSettingKeys(IsKarix);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetSettingKeys" + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("SendSlackInvitation")]
        [HttpPost]
        public IActionResult SendSlackInvitation([FromBody] SlackInvitationModel invitationModel)
        {
            Dictionary<string, string> deliveryStatus = new Dictionary<string, string>();
            List<AppSettingsVM> appSettings = appSetting.FillAndGetAppSettings(ClientID);
            var slackAppSetting = appSettings.FirstOrDefault(d => d.Key == SettingMaster.SlackAppSettings);
            if (!string.IsNullOrWhiteSpace(invitationModel.Email))
            {
                var mainMailResult = SendSlackInvite(invitationModel.Email, invitationModel.Message, slackAppSetting);
                if (mainMailResult.IsSuccessStatusCode)
                    deliveryStatus.Add(invitationModel.Email, "Sent");
                else
                    deliveryStatus.Add(invitationModel.Email, "Failed");
                if (string.IsNullOrWhiteSpace(invitationModel.Group))
                    return Ok(deliveryStatus);
            }

            var allGroups = groupProvider.GetAllGroupsByClientID(ClientID);
            if (allGroups == null || allGroups.Count() == 0) return BadRequest("Invalid client");
            var clientGroup = allGroups.FirstOrDefault(d => d.Name == invitationModel.Group);
            if (clientGroup == null) return BadRequest($"No group with name: {invitationModel.Group} found");

            var userData = groupProvider.GetGroupMappingUsers(clientGroup.GroupID.ToString(), 0, string.Empty);
            if (userData == null || userData.UserChannelInfoList == null || userData.UserChannelInfoList.Count() == 0) return BadRequest($"No users found in group: {invitationModel.Group}");
            foreach (var user in userData.UserChannelInfoList)
            {
                var result = SendSlackInvite(user.EmailID, invitationModel.Message, slackAppSetting);
                if (result.IsSuccessStatusCode)
                    deliveryStatus.Add(user.EmailID, "Sent");
                else
                    deliveryStatus.Add(user.EmailID, "Failed");
            }
            return Ok(deliveryStatus);

        }

        private HttpResponseMessage SendSlackInvite(string email, string message, AppSettingsVM slackAppSetting)
        {
            var dict = new Dictionary<string, string>();
            dict.Add("email", email);
            dict.Add("team_id", slackAppSetting.Value3);
            dict.Add("channel_ids", slackAppSetting.Value4);
            if (!string.IsNullOrWhiteSpace(message))
                dict.Add("custom_message", message);
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", slackAppSetting.Value1);
            var req = new HttpRequestMessage(HttpMethod.Post, slackAppSetting.Value2) { Content = new FormUrlEncodedContent(dict) };
            return client.SendAsync(req).Result;

        }
        [HttpPost("UploadExcelFileWhatsAppParameters")]
        public async Task<IActionResult> UploadExcelFileWhatsAppParameters([FromForm] WhatsAppUsersExcelUpload excel)
        {
            try
            {
                if (excel.ExcelFile != null && excel.ExcelFile.Length > 0)
                {
                    string BinaryFileName = DateTime.UtcNow.AddHours(5.5).ToString("mmddyyyyhhmmss") + excel.ExcelFile.FileName.Replace("\r", "");
                    string FileName = ExcelProcessing.GetFileName(BinaryFileName);

                    var filestream = excel.ExcelFile.OpenReadStream();
                    AWSHelper.UploadFileToS3(filestream, null, FileName);

                    ExcelProcessing file = new ExcelProcessing();
                    int failedCount = 0;
                    var list = file.ReadAndProcessWhatsappExcel(FileName, out failedCount);

                    if (list != null && list.Count > 0)
                    {
                        int cnt = 0;
                        decimal totChunks = Math.Ceiling((decimal)list.Count / 500);

                        for (int i = 0; i < totChunks; i++)
                        {
                            List<ExcelWhatsAppData> userList = list.Skip(cnt).Take(list.Skip(cnt).Count() > 500 ? 500 : list.Skip(cnt).Count()).ToList();
                            Thread.Sleep(5000);
                            userProvider.SubmitWhatsAppParameters(userList, UserID, ClientID);
                            cnt += userList.Count;
                            decimal percentage = Math.Ceiling((decimal)cnt * 100 / list.Count);

                            _chatHub = new ChatHub();
                            await _chatHub.UserUploadProgress(_hubContext, UserID.ToString(), percentage);
                        }
                        int successCount = list.Count - failedCount;
                        return Ok(new { SuccessCount = successCount, FailedCount = failedCount });
                    }
                    else
                    {
                        return BadRequest(new { Message = "FAILED" });
                    }
                }
                else
                {
                    logger.Info("UploadExcelFileWhatsAppParameters: Invalid File Format");
                    return BadRequest(new { messagev = "Invalid File Format" });
                }
            }
            catch (Exception ex)
            {
                logger.Error("UploadExcelFileWhatsAppParameters: " + ex.Message);
                return BadRequest(new
                {
                    msg = ex.Message
                });
            }
        }
    }
}