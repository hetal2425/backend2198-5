﻿using EchoChat.Channel;
using EchoChat.ChannelConnector;
using EchoChat.ServiceProvider;
using EchoChatApp.General;
using log4net;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EchoChat.Models.WebHookModels;
using System.IO;
using Newtonsoft.Json;
using System.Text;
using EchoChatApp.Helper;
using Microsoft.Extensions.Options;
using System.Web;
using EchoChat.Models;
using EchoChat.Constants;
using System.Text.RegularExpressions;
using EchoChat.Helper;
using EchoChat.ChannelVerification;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.SignalR;
using EchoChatApp.Hubs;

namespace EchoChatApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HookController : ControllerBase
    {
        private IHookProvider hookProvider;
        private IConnector connector;
        private INoticeProvider noticeProvider;
        private IUserProvider userProvider;
        private readonly AppSettingsFromFile appSetting;
        private ILog logger;
        private readonly IHubContext<ChatHub> _hubContext;
        ChatHub _chatHub;
        public HookController(IHookProvider oHookProvider, IConnector oConnector, IOptions<AppSettingsFromFile> oAppSettings, INoticeProvider oNoticeProvider, IUserProvider oUserProvider, IHubContext<ChatHub> hubContext)
        {
            hookProvider = oHookProvider;
            connector = oConnector;
            noticeProvider = oNoticeProvider;
            userProvider = oUserProvider;
            appSetting = oAppSettings.Value;
            logger = Logger.GetLogger(typeof(HookController));
            _hubContext = hubContext;
        }

        [Route("SESEvents")]
        [HttpPost]
        public async Task<string> PushSESEvents()
        {
            try
            {
                using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
                {
                    string _str = await reader.ReadToEndAsync();
                    logger.Info("SES Event Data : " + _str);
                    AWSSESEvent eventData = JsonConvert.DeserializeObject<AWSSESEvent>(_str);
                    AWSSESEventMessege message = JsonConvert.DeserializeObject<AWSSESEventMessege>(eventData.Message.Replace("\n", string.Empty));

                    hookProvider.InsertSESEvents(message);

                    return _str;
                }
            }
            catch (Exception ex)
            {
                logger.Error("PushSESEvents: " + ex.Message);
                return "";
            }
        }
        //[Route("SESEvents")]
        //[HttpPost]
        //public async Task<string> PushSESEvents()
        //{
        //    try
        //    {
        //        using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
        //        {
        //            string _str = await reader.ReadToEndAsync();
        //            logger.Info("SES Event Data : " + _str);
        //            List<TwilioSESEvent> eventData = JsonConvert.DeserializeObject<List<TwilioSESEvent>>(_str);
        //            // AWSSESEventMessege message = JsonConvert.DeserializeObject<AWSSESEventMessege>(eventData.Message.Replace("\n", string.Empty));
        //            foreach(TwilioSESEvent e in eventData)
        //            {
        //                hookProvider.InsertTwilioSESEvents(e);
        //            }                   

        //            return _str;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error("PushSESEvents: " + ex.Message);
        //        return "";
        //    }
        //}

        [Route("SESReceiveEmail")]
        [HttpPost]
        public void PushSESReceiveEmail()
        {
            try
            {
                using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
                {
                    var readerOut = reader.ReadToEndAsync();
                    readerOut.Wait();
                    string _str = readerOut.Result;
                    logger.Info("SES ReceiveEmail Data : " + _str);
                    AWSReceiveEmailData message = JsonConvert.DeserializeObject<AWSReceiveEmailData>(_str);
                    if (message != null && message.mail != null && message.mail.headers != null && message.mail.headers.Count > 0)
                    {
                        NoticeDTO notice = hookProvider.InsertReceiveEmail(message, appSetting.BaseAPI);
                        if (notice != null)
                        {
                            ReceiveNotice(notice);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("PushSESReceiveEmail: " + ex.Message);
            }
        }

        [Route("AmeyoSMSEvents")]
        [HttpPost]
        public IActionResult PushAmeyoSMSEvents([FromServices] string sid, string dest, string stime, string dtime, string status, string reason)
        {
            try
            {
                using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
                {
                    var readerOut = reader.ReadToEndAsync();
                    readerOut.Wait();
                    string _str = readerOut.Result;
                    logger.Info("AmeyoSMSEventsAmeyoSMSEventsAmeyoSMSEvents UTF8 : " + _str);
                }

                using (StreamReader reader = new StreamReader(Request.Body, Encoding.ASCII))
                {
                    var readerOut = reader.ReadToEndAsync();
                    readerOut.Wait();
                    string _str = readerOut.Result;
                    logger.Info("AmeyoSMSEventsAmeyoSMSEventsAmeyoSMSEvents ASCII: " + _str);
                }

                string param1 = Request.QueryString.HasValue ? Request.QueryString.Value : "";
                logger.Info($"param1: {param1}");

                string param2 = Request.Query["sid"].ToString();
                logger.Info($"param2: {param2}");

                logger.Info($"sid: {sid} dest: {dest} status: {status}");
                //hookProvider.InsertAmeyoSMSEvents(sSender, sMobileNo, sStatus, sMessageId, iErrCode, sTagName, sUdf1, sUdf2);
                return Ok();
            }
            catch (Exception ex)
            {
                logger.Error("PushAmeyoSMSEvents: " + ex.Message);
                return BadRequest();
            }
        }

        [Route("SmsEventsKarix")]
        [HttpGet]
        public IActionResult SmsEventsKarix([FromQuery] string Status, string Operator, string Circle, string Dest, string Dtime, string Stime, string MID, string Reason, string Send)
        {
            try
            {

                string paramss = Request.QueryString.HasValue ? Request.QueryString.Value : "";
                logger.Info($"Parameters: {paramss}");

                hookProvider.InsertSMSEvents(Status, Operator, Circle, Dest, Dtime, Stime, MID, Reason, Send);
                return Ok();
            }
            catch (Exception ex)
            {
                logger.Error("PushAmeyoSMSEvents: " + ex.Message);
                return BadRequest();
            }
        }

        [Route("ReceiveSMSKarix")]
        [HttpGet]
        public IActionResult ReceiveSMSKarix([FromQuery] string send, string msg, string dest, DateTime stime)
        {
            try
            {

                string param1 = Request.QueryString.HasValue ? Request.QueryString.Value : "";
                logger.Info($"param1: {param1}");

                string param2 = Request.Query["msg"].ToString();
                logger.Info($"param2: {param2}");

                logger.Info($"sSender: {send} msg: {msg} dest: {dest} stime: {stime}");
                //hookProvider.InsertAmeyoSMSEvents(sSender, sMobileNo, sStatus, sMessageId, iErrCode, sTagName, sUdf1, sUdf2);
                return Ok();
            }
            catch (Exception ex)
            {
                logger.Error("ReceiveSMSKarix: " + ex.Message);
                return BadRequest();
            }
        }

        // For What's App Messages
        [Route("WhatsAppEvents")]
        [HttpPost]
        public async Task PushWhatsAppEventsAsync([FromBody] dynamic obj)
        {
            try
            {
                string _str = string.Empty;
                if (string.IsNullOrEmpty(appSetting.WhatsAppProvider))
                {
                    _str = obj.ToString();
                    logger.Info("_str: " + _str);
                    WAStatus Status = null;
                    WAMessages message = null;
                    if (_str.Contains("statuses"))
                    {
                        logger.Info("statuses");
                        Status = JsonConvert.DeserializeObject<WAStatus>(_str);
                    }
                    else if (_str.Contains("messages"))
                    {
                        logger.Info("messages");
                        message = JsonConvert.DeserializeObject<WAMessages>(_str);
                    }
                    if (Status != null)
                    {
                        if (Status != null && Status.statuses.Count > 0)
                        {
                            string error = "";
                            foreach (var sts in Status.statuses)
                            {
                                if (sts.status.Equals("sent"))
                                {

                                }
                                else if (sts.status.Equals("delivered"))
                                {

                                }
                                else if (sts.status.Equals("read"))
                                {
                                    try
                                    {
                                        if (sts?.errors != null)
                                        {
                                            foreach (Error e in sts.errors)
                                            {
                                                error += "Code " + e.code + " title" + e.title;
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Error("Error In WA JSON Parsing" + ex.Message);
                                    }

                                }
                                else if (sts.status.Equals("failed"))
                                {

                                }
                                else if (sts.status.Equals("deleted"))
                                {

                                }
                                DateTime _timeStamp = DateTimeOffset.FromUnixTimeSeconds(Convert.ToInt32(sts.timestamp)).DateTime;

                                hookProvider.InsertWhatsAppEvents(sts.recipient_id, sts.id, sts.status, _timeStamp);


                            }
                        }
                    }
                    else if (message != null)
                    {
                        if (message.messages.Count > 0)
                        {
                            foreach (var msg in message.messages)
                            {
                                DateTime _timeStamp = DateTimeOffset.FromUnixTimeSeconds(Convert.ToInt32(msg.timestamp)).DateTime;
                                msg.Date = _timeStamp;
                                NoticeDTO notice = hookProvider.InsertIncomingWhatsApp(msg, appSetting.BaseAPI);
                                if (notice != null)
                                {
                                    var noticeDetails = ReceiveNotice(notice);

                                    HubNotice result = noticeProvider.GetNoticeDetailsForHub(noticeDetails.NoticeID);
                                    result.FilePath = AWSHelper.GetPresignedFileURL(result.FileName);
                                    _chatHub = new ChatHub();

                                    logger.Info("SendToCaller function start");
                                    await _chatHub.SendToCaller(_hubContext, result);
                                    logger.Info("SendToCaller function end");
                                }
                                //string _msg = "From " + msg.from + "msg id: " + msg.id + " msg body : " + msg.text.body + "date:  " + _timeStamp + " " + msg.type;
                            }
                        }
                    }
                }
                else
                {
                    _str = obj.ToString();

                    logger.Info("_str: " + _str);

                    WASMessageKarix Status = null;
                    WARMessageKarix message = null;
                    if (_str.Contains("notificationAttributes"))
                    {
                        logger.Info("notificationAttributes");
                        Status = JsonConvert.DeserializeObject<WASMessageKarix>(_str);
                    }
                    else if (_str.Contains("message"))
                    {
                        logger.Info("message");
                        message = JsonConvert.DeserializeObject<WARMessageKarix>(_str);
                    }
                    if (Status != null)
                    {
                        string error = "";
                        if (Status.notificationAttributes.status.Equals("sent"))
                        {

                        }
                        else if (Status.notificationAttributes.status.Equals("delivered"))
                        {

                        }
                        else if (Status.notificationAttributes.status.Equals("read"))
                        {
                            try
                            {
                                if (Status?.notificationAttributes != null)
                                {
                                    error += "Code " + Status.notificationAttributes.code + " title" + Status.notificationAttributes.reason;
                                }
                            }
                            catch (Exception ex)
                            {
                                logger.Error("Error In WA JSON Parsing" + ex.Message);
                            }

                        }
                        else if (Status.notificationAttributes.status.Equals("failed"))
                        {

                        }
                        else if (Status.notificationAttributes.status.Equals("deleted"))
                        {

                        }
                        DateTime _timeStamp = DateTimeOffset.FromUnixTimeMilliseconds(Convert.ToInt64(Status.events.timestamp)).DateTime;

                        hookProvider.InsertWhatsAppEvents(Status.recipient.to, Status.events.mid, Status.notificationAttributes.status, _timeStamp);

                    }
                    else if (message != null)
                    {
                        WAMessage wAMessage = new WAMessage();
                        DateTime _timeStamp = DateTimeOffset.FromUnixTimeSeconds(Convert.ToInt64(message.events.timestamp)).DateTime;
                        wAMessage.Date = _timeStamp;
                        wAMessage.from = message.eventContent.message.from;
                        wAMessage.id = message.eventContent.message.id;
                        wAMessage.text = message.eventContent.message.text;
                        wAMessage.timestamp = message.events.timestamp;
                        wAMessage.type = message.eventContent.message.contentType;

                        NoticeDTO notice = hookProvider.InsertIncomingWhatsApp(wAMessage, appSetting.BaseAPI);
                        if (notice != null)
                        {
                            var noticeDetails = ReceiveNotice(notice);

                            HubNotice result = noticeProvider.GetNoticeDetailsForHub(noticeDetails.NoticeID);
                            result.FilePath = AWSHelper.GetPresignedFileURL(result.FileName);
                            _chatHub = new ChatHub();
                            
                            logger.Info("SendToCaller function start");
                            await _chatHub.SendToCaller(_hubContext, result);
                            logger.Info("SendToCaller function end");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error In WA: " + ex.Message);
            }
        }

        private NoticeDetailDTO ReceiveNotice(NoticeDTO notice)
        {
            try
            {
                if (string.IsNullOrEmpty(notice.DeviceID))
                {
                    notice.DeviceID = string.Empty;
                }

                int IsReply = 0, IsSms = 0, ParentID = 0, FileType = 0, GroupID = 0, IsEmail = 0, IsWhatsapp = 0, IsFacebook = 0, BroadcastToAllChannel = 0, IsSlack = 0;
                string echoFileType = EchoFileType.Document;

                if (!string.IsNullOrEmpty(notice.IsReply))
                {
                    IsReply = Convert.ToInt32(notice.IsReply);
                }
                if (!string.IsNullOrEmpty(notice.IsSms))
                {
                    IsSms = Convert.ToInt32(notice.IsSms);
                }
                if (!string.IsNullOrEmpty(notice.ParentID))
                {
                    ParentID = Convert.ToInt32(notice.ParentID);
                }
                if (!string.IsNullOrEmpty(notice.FileType))
                {
                    FileType = Convert.ToInt32(notice.FileType);
                }
                if (!string.IsNullOrEmpty(notice.GroupID))
                {
                    GroupID = Convert.ToInt32(notice.GroupID);
                }
                if (!string.IsNullOrEmpty(notice.IsEmail))
                {
                    IsEmail = Convert.ToInt32(notice.IsEmail);
                }
                if (!string.IsNullOrEmpty(notice.IsWhatsapp))
                {
                    IsWhatsapp = Convert.ToInt32(notice.IsWhatsapp);
                }
                if (!string.IsNullOrEmpty(notice.IsFacebook))
                {
                    IsFacebook = Convert.ToInt32(notice.IsFacebook);
                }
                if (!string.IsNullOrEmpty(notice.IsSlack))
                {
                    IsSlack = Convert.ToInt32(notice.IsSlack);
                }

                if (string.IsNullOrEmpty(notice.NoticeDetail) && !string.IsNullOrEmpty(notice.NoticeDetailHTML))
                {
                    notice.NoticeDetail = Regex.Replace(notice.NoticeDetailHTML, "<.*?>", String.Empty);
                }

                if (ParentID > 0 && BroadcastToAllChannel == 1 && IsEmail == 1)
                {
                    notice.NoticeDetailHTML = notice.NoticeDetail;
                }

                Guid createdBy = Guid.Empty;
                int clientID = 0;

                createdBy = Guid.Parse(notice.Createdby);
                UserProfile uModel = userProvider.GetUserByID(createdBy);
                clientID = uModel.ClientID;

                string filePath = string.Empty;
                List<UserChannelInfo> userChannelInfos = new List<UserChannelInfo>();
                NoticeDetailDTO _notice = new NoticeDetailDTO();
                try
                {
                    _notice = new NoticeDetailDTO();
                    _notice.NoticeTitle = notice.NoticeTitle;
                    _notice.NoticeDetail = notice.NoticeDetail;
                    _notice.NoticeDetailHTML = notice.NoticeDetailHTML;
                    _notice.GroupID = GroupID;
                    _notice.CreatedBy = createdBy;
                    _notice.IsReply = IsReply == 1;
                    _notice.IsSms = IsSms == 1;
                    _notice.IsSlack = IsSlack == 1;
                    _notice.ParentID = ParentID;
                    _notice.FileType = FileType;
                    _notice.FileName = notice.FileName;
                    _notice.IsEmail = IsEmail == 1;
                    _notice.IsWhatsapp = IsWhatsapp == 1;
                    _notice.IsFacebook = IsFacebook == 1;
                    _notice.Date = notice.Date;
                    _notice.EndDate = notice.EndDate;
                    _notice.Time = notice.Time;
                    _notice.Days = notice.Days;
                    _notice.IsRecursive = notice.IsRecursive;
                    if (notice.StreamWithData != null && notice.StreamWithData.Length > 0)
                    {
                        if (_notice.FileType == 0)
                        {
                            var fileExt = Path.GetExtension(notice.StreamWithData.FileName);

                            var ImageExtensions = new[] { ".jpg", ".jpeg", ".png" };
                            var videosExtensions = new[] { ".mp4", ".3gp", ".avi", ".mov", ".wmv", ".webm", ".mkv" };

                            if (ImageExtensions.Contains(fileExt))
                            {
                                _notice.FileType = 1;
                                echoFileType = EchoFileType.Image;
                            }
                            else if (videosExtensions.Contains(fileExt))
                            {
                                _notice.FileType = 2;
                                echoFileType = EchoFileType.Video;
                            }
                            else
                            {
                                _notice.FileType = 3;
                                echoFileType = EchoFileType.Document;
                            }
                        }

                        string BinaryFileName = DateTime.UtcNow.ToString("mmddyyyyhhmmssfff") + notice.StreamWithData.FileName.Replace("\r", string.Empty);
                        _notice.FileName = ExcelProcessing.GetFileName(BinaryFileName);
                        var filestream = notice.StreamWithData.OpenReadStream();
                        AWSHelper.UploadFileToS3(filestream, null, _notice.FileName);
                    }
                    logger.Info("_notice save start");
                    noticeProvider.SaveNoticeDetails(_notice);
                    logger.Info("_notice save end");
                    noticeProvider.SaveNotice(userChannelInfos, _notice.NoticeID, GroupID, createdBy);
                    return _notice;
                }
                catch (Exception ex)
                {
                    logger.Error("SaveNotice: " + ex.Message);
                    return null;
                }
            }
            catch (Exception ex)
            {
                logger.Error("AddNotice: " + ex.Message);
                return null;
            }
        }

        [Route("LinkedInData")]
        [HttpPost]
        public void PushLinkedHelperData([FromBody] dynamic obj)
        {
            try
            {
                string _str = obj.ToString();

                //logger.Error("Data : " + _str);

                LinkedHelperData data = JsonConvert.DeserializeObject<LinkedHelperData>(_str);
                hookProvider.InsertLinkedHelperData(data);
            }
            catch (Exception ex)
            {
                logger.Error("Push InsertLinkedHelperData: " + ex.Message);
            }
        }

        [Route("JoinUserEvent")]
        [HttpPost]
        public IActionResult JoinUserEvent(dynamic request)
        {
            var requestJson = JObject.Parse(request.ToString());
            if (requestJson != null)
            {
                if (Convert.ToString(requestJson.type).Equals("url_verification"))
                    return Content(Convert.ToString(requestJson.challenge));
                else if (Convert.ToString(requestJson["type"]).Equals("event_callback"))
                {
                    EventsRequest eventsRequest = JsonConvert.DeserializeObject<EventsRequest>(requestJson.ToString());
                    if (eventsRequest.Event.type == "team_join" && eventsRequest.authorizations != null && eventsRequest.authorizations[0] != null)
                    {
                        AcceptInvitation(requestJson, eventsRequest);
                    }
                    else if (eventsRequest.Event.type == "message")
                    {
                        ReceiveSlackMessage(requestJson, eventsRequest);
                    }
                }
            }

            return BadRequest();
        }

        private void ReceiveSlackMessage(dynamic requestJson, EventsRequest eventsRequest)
        {
            EventRequestUserMessage userMessage = JsonConvert.DeserializeObject<EventRequestUserMessage>(requestJson.ToString());
            //var userProfile = userProvider.GetUserBySlackID(userMessage.Event.user);
            try
            {
                var message = userMessage.Event.text;
                if (!string.IsNullOrWhiteSpace(message))
                {
                    var notice = hookProvider.InsertReceiveSlack("Recieved", userMessage.Event.client_msg_id, "", userMessage.Event.user, eventsRequest.authorizations[0].user_id, DateTime.UtcNow, message);
                    if (notice != null && !notice.IsAlreadyExists)
                    {
                        var accountOwner = userProvider.GetUserBySlackID(eventsRequest.authorizations[0].user_id);
                        notice.IsSlack = "1";
                        ReceiveNotice(notice);
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Error("PushSESReceiveSlack: " + ex.Message);
            }
        }

        private void AcceptInvitation(dynamic requestJson, EventsRequest eventsRequest)
        {
            EventRequestUserJoin userJoinedEvent = JsonConvert.DeserializeObject<EventRequestUserJoin>(requestJson.ToString());
            var userObj = userJoinedEvent.Event.user;
            var email = userObj.profile.email;
            var slackAccountOwnerSlackId = eventsRequest.authorizations[0].user_id;
            if (!string.IsNullOrEmpty(email))
            {
                var accountOwner = userProvider.GetUserBySlackID(slackAccountOwnerSlackId);
                if (accountOwner != null)
                {
                    CreateUser user = userProvider.GetUserByUsername(email);
                    if (user != null)
                    {
                        user.IsOnSlack = true;
                        user.SlackId = userObj.id;
                        userProvider.CreateUpdateUser(user, accountOwner.ClientID, new Guid(accountOwner.UserID));
                    }
                }
            }
        }

        public class EventsRequest
        {
            public string token { get; set; }
            public string challenge { get; set; }
            public string team_id { get; set; }
            public string type { get; set; }
            [JsonProperty(PropertyName = "event")]
            public SlackEvent Event { get; set; }
            public List<AuthorizationData> authorizations { get; set; }
        }
        public class AuthorizationData
        {
            public string user_id { get; set; }
            public string team_id { get; set; }
        }
        public class SlackEvent
        {
            public string type { get; set; }
        }

        public class EventRequestUserMessage
        {
            [JsonProperty(PropertyName = "event")]
            public SlackEventMessage Event { get; set; }
        }
        public class EventRequestUserJoin
        {
            [JsonProperty(PropertyName = "event")]
            public SlackEventJoin Event { get; set; }
        }
        public class SlackEventMessage
        {
            public string user { get; set; }
            public string channel { get; set; }
            public string client_msg_id { get; set; }
            public string text { get; set; }
        }
        public class SlackEventJoin
        {
            public SlackUser user { get; set; }
        }
        public class SlackUser
        {
            public string id { get; set; }
            public SlackUserProfile profile { get; set; }
            public bool? is_email_confirmed { get; set; }
        }

        public class SlackUserProfile
        {
            public string phone { get; set; }
            public string email { get; set; }
        }
    }
}