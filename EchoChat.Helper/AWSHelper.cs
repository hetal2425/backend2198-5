﻿using Amazon;
using Amazon.Comprehend;
using Amazon.Comprehend.Model;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using log4net;
using System;
using System.IO;
using System.Text;

namespace EchoChat.Helper
{
    public class AWSHelper
    {
        private static readonly log4net.ILog logger = LogManager.GetLogger(typeof(AWSHelper));

        public readonly static string awsKey = @"AKIAYDD4JBKGSEDFAKPU";
        public readonly static string awsSecret = @"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";
        public readonly static string awsBucket = @"elasticbeanstalk-ap-south-1-556458904205";
        public readonly static string sender = @"support@echocommunicator.com";
        public static void UploadFileToS3(System.IO.Stream localFile, string subDirectoryInBucket, string fileNameInS3)
        {
            IAmazonS3 client = new AmazonS3Client(awsKey, awsSecret, RegionEndpoint.APSouth1);
            TransferUtility utility = new TransferUtility(client);
            TransferUtilityUploadRequest request = new TransferUtilityUploadRequest();
            request.BucketName = awsBucket + @"/NoticeData";
            request.Key = fileNameInS3;
            request.InputStream = localFile;
            utility.Upload(request);
        }

        public static void AddClientLogo(System.IO.Stream localFile, string subDirectoryInBucket, string fileNameInS3)
        {
            IAmazonS3 client = new AmazonS3Client(awsKey, awsSecret, RegionEndpoint.APSouth1);
            TransferUtility utility = new TransferUtility(client);
            TransferUtilityUploadRequest request = new TransferUtilityUploadRequest();
            request.BucketName = awsBucket + @"/ClientLogo";
            request.Key = fileNameInS3;
            request.InputStream = localFile;
            utility.Upload(request);
        }

        public static string GetPresignedFileURL(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                return string.Empty;
            }
            try
            {
                fileName = @"NoticeData/" + fileName;
                // Create a client
                AmazonS3Client client = new AmazonS3Client(awsKey, awsSecret, RegionEndpoint.APSouth1);

                // Create a CopyObject request
                GetPreSignedUrlRequest request = new GetPreSignedUrlRequest
                {
                    BucketName = awsBucket,
                    Key = fileName,
                    Expires = DateTime.Now.AddDays(7)
                };

                // Get path for request
                return client.GetPreSignedURL(request);
            }
            catch (Exception ex)
            {
                logger.Error("GetPresignedFileURL: " + ex.Message);
                return null;
            }
        }

        public static string GetPresignedFileURL_Logo(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                return null;
            }
            try
            {
                fileName = @"ClientLogo/" + fileName;
                // Create a client
                AmazonS3Client client = new AmazonS3Client(awsKey, awsSecret, RegionEndpoint.APSouth1);

                // Create a CopyObject request
                GetPreSignedUrlRequest request = new GetPreSignedUrlRequest
                {
                    BucketName = awsBucket,
                    Key = fileName,
                    Expires = DateTime.Now.AddDays(7)
                };

                // Get path for request
                return client.GetPreSignedURL(request);
            }
            catch (Exception ex)
            {
                logger.Error("GetPresignedFileURL_Logo: " + ex.Message);
                return null;
            }
        }


        public static string AnalyzeText(string title, string htmlDetails, string text, string date)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return string.Empty;
            }
            try
            {
                var filePath = Path.Combine("MyStaticFiles", "ComprehendAnalyzer.html");


                var awsCredentials = new BasicAWSCredentials(awsKey, awsSecret);
                AmazonComprehendClient comprehendClient = new AmazonComprehendClient(awsCredentials, RegionEndpoint.APSouth1);

                DetectEntitiesRequest detectEntitiesRequest = new DetectEntitiesRequest()
                {
                    Text = text,
                    LanguageCode = "en"
                };
                var response = comprehendClient.DetectEntitiesAsync(detectEntitiesRequest);
                response.Wait();
                DetectEntitiesResponse entitiesResponse = response.Result;


                string t = File.ReadAllText(filePath);


                StringBuilder sb = new StringBuilder();

                sb.Append("<table id='testt'><tr><th>Entity</th><th>Type</th><th>Confidence</th></tr>");
                foreach (Entity e in entitiesResponse.Entities)
                {
                    sb.Append($"<tr><td>{e.Text}</td><td>{e.Type}</td><td>{e.Score}</td></tr>");
                }
                sb.Append("</table>");

                t = t.Replace("{EntityTable}", sb.ToString());



                DetectDominantLanguageRequest detectDominantLanguageRequest = new DetectDominantLanguageRequest()
                {
                    Text = text
                };

                var dmlr = comprehendClient.DetectDominantLanguageAsync(detectDominantLanguageRequest);
                dmlr.Wait();
                sb.Clear();
                DetectDominantLanguageResponse detectDominantLanguageResponse = dmlr.Result;
                sb.Append("<table id='testt'><tr><th>Language Code</th><th>Confidence</th></tr>");
                foreach (DominantLanguage e in detectDominantLanguageResponse.Languages)
                {
                    sb.Append($"<tr><td>{e.LanguageCode}</td><td>{e.Score}</td></tr>");
                }
                sb.Append("</table>");
                t = t.Replace("{LanguageTable}", sb.ToString());

                DetectSentimentRequest detectSentimentRequest = new DetectSentimentRequest()
                {
                    Text = text,
                    LanguageCode = "en"
                };
                var dsr = comprehendClient.DetectSentimentAsync(detectSentimentRequest);
                dsr.Wait();
                sb.Clear();
                DetectSentimentResponse detectSentimentResponse = dsr.Result;
                sb.Append("<table id='testt'><tr><th>Neutral</th><th>Positive</th><th>Negative</th><th>Mixed</th></tr>");
                sb.Append($"<tr><td>{detectSentimentResponse.SentimentScore.Neutral}</td><td>{detectSentimentResponse.SentimentScore.Positive}</td><td>{detectSentimentResponse.SentimentScore.Negative}</td><td>{detectSentimentResponse.SentimentScore.Mixed}</td></tr>");
                sb.Append("</table>");
                t = t.Replace("{SentimentTable}", sb.ToString());

                DetectSyntaxRequest detectSyntaxRequest = new DetectSyntaxRequest()
                {
                    Text = text,
                    LanguageCode = "en"
                };
                var dsrr = comprehendClient.DetectSyntaxAsync(detectSyntaxRequest);
                dsrr.Wait();
                sb.Clear();
                DetectSyntaxResponse detectSyntaxResponse = dsrr.Result;
                sb.Append("<table id='testt'><tr><th>Word</th><th>Part of speech</th><th>Confidence</th></tr>");
                foreach (SyntaxToken e in detectSyntaxResponse.SyntaxTokens)
                {
                    sb.Append($"<tr><td>{e.Text}</td><td>{e.PartOfSpeech.Tag}</td><td>{e.PartOfSpeech.Score}</td></tr>");
                }
                sb.Append("</table>");
                t = t.Replace("{SyntaxTable}", sb.ToString());


                DetectKeyPhrasesRequest detectKeyPhrasesRequest = new DetectKeyPhrasesRequest()
                {
                    Text = text,
                    LanguageCode = "en"
                };
                var dkpr = comprehendClient.DetectKeyPhrasesAsync(detectKeyPhrasesRequest);
                dkpr.Wait();
                sb.Clear();
                DetectKeyPhrasesResponse detectKeyPhrasesResponse = dkpr.Result;
                sb.Append("<table id='testt'><tr><th>Key phrases</th><th>Confidence</th></tr>");
                foreach (KeyPhrase e in detectKeyPhrasesResponse.KeyPhrases)
                {
                    sb.Append($"<tr><td>{e.Text}</td><td>{e.Score}</td></tr>");
                }
                sb.Append("</table>");
                t = t.Replace("{KeyPhrases}", sb.ToString());



                t = t.Replace("{NoticeTitle}", title);
                t = t.Replace("{NoticeDate}", date);
                t = t.Replace("{NoticeDetails}", htmlDetails);



                //DetectPiiEntitiesRequest detectPiiEntitiesRequest = new DetectPiiEntitiesRequest()
                //{
                //    Text = text,
                //    LanguageCode = "en"
                //};
                //var dper = comprehendClient.DetectPiiEntitiesAsync(detectPiiEntitiesRequest);
                //dper.Wait();
                //sb.Clear();
                //DetectPiiEntitiesResponse detectPiiEntitiesResponse = dper.Result;
                //sb.Append("<table id='testt'><tr><th>Word</th><th>Part of speech</th><th>Confidence</th></tr>");
                //foreach (PiiEntity e in detectPiiEntitiesResponse.Entities)
                //{
                //    sb.Append($"<tr><td>{e.Type}</td><td>{e.Score}</td><td>{e.PartOfSpeech.Score}</td></tr>");
                //}
                //sb.Append("</table>");
                //t = t.Replace("{SyntaxTable}", sb.ToString());




                return t;
            }
            catch (Exception ex)
            {
                logger.Error("AnalyzeText: " + ex.Message);
                throw ex;
            }
        }
    }
}