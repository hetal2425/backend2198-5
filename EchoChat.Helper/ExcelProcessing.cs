﻿using EchoChat.Models;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EchoChat.Helper
{
    public class ExcelProcessing
    {
        public List<ExcelUserData> ReadExcel(string fileName)
        {
            using (var excelPack = new ExcelPackage())
            {
                var fileData = new WebClient().DownloadDataTaskAsync(new Uri(AWSHelper.GetPresignedFileURL(fileName)));
                fileData.Wait();
                byte[] myfile = fileData.Result;
                Stream stream = new MemoryStream(myfile);
                excelPack.Load(stream);

                //Lets Deal with first worksheet.(You may iterate here if dealing with multiple sheets)
                var ws = excelPack.Workbook.Worksheets[0];

                List<ExcelUserData> userList = new List<ExcelUserData>();

                int colCount = ws.Dimension.End.Column;  //get Column Count
                int rowCount = ws.Dimension.End.Row;

                for (int row = 2; row <= rowCount; row++) // start from to 2 omit header
                {

                    bool IsValid = true;
                    ExcelUserData _user = new ExcelUserData();

                    for (int col = 1; col <= colCount; col++)
                    {
                        if (col == 1)
                        {
                            _user.FirstName = ws.Cells[row, col].Value?.ToString().Trim();
                            if (string.IsNullOrEmpty(_user.FirstName))
                            {

                                IsValid = false;
                            }
                        }
                        else if (col == 2)
                        {

                            _user.Email = ws.Cells[row, col].Value?.ToString().Trim();
                        }
                        else if (col == 3)
                        {
                            _user.MobileNo = ws.Cells[row, col].Value?.ToString().Trim();

                            if (string.IsNullOrEmpty(_user.MobileNo))
                            {
                                IsValid = false;
                            }
                            else if (_user.MobileNo.Length != 10)
                            {
                                IsValid = false;
                            }

                        }
                        _user.ErrorMessage = _user.IsValid == false ? "Invalid" : null;

                        _user.IsValid = IsValid;
                    }
                    userList.Add(_user);
                }
                return userList;
            }
        }

        public List<ExcelUserData> ReadAndProcessExcel(string fileName, out int failed)
        {
            failed = 0;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (var excelPack = new ExcelPackage())
            {
                var fileData = new WebClient().DownloadDataTaskAsync(new Uri(AWSHelper.GetPresignedFileURL(fileName)));
                fileData.Wait();
                byte[] myfile = fileData.Result;
                Stream stream = new MemoryStream(myfile);
                excelPack.Load(stream);

                //Lets Deal with first worksheet.(You may iterate here if dealing with multiple sheets)
                var ws = excelPack.Workbook.Worksheets[0];

                List<ExcelUserData> userList = new List<ExcelUserData>();

                int colCount = ws.Dimension.End.Column;  //get Column Count
                int rowCount = ws.Dimension.End.Row;

                for (int row = 2; row <= rowCount; row++) // start from to 2 omit header
                {

                    bool IsValid = true;
                    ExcelUserData _user = new ExcelUserData();

                    for (int col = 1; col <= colCount; col++)
                    {
                        if (col == 1)
                        {
                            _user.FirstName = ws.Cells[row, col].Value?.ToString().Trim();
                            if (string.IsNullOrEmpty(_user.FirstName))
                            {
                                failed++;
                                IsValid = false;
                            }
                        }
                        else if (col == 2)
                        {
                            _user.Email = ws.Cells[row, col].Value?.ToString().Trim();
                        }
                        else if (col == 3)
                        {
                            _user.MobileNo = ws.Cells[row, col].Value?.ToString().Trim();

                            if (string.IsNullOrEmpty(_user.MobileNo))
                            {
                                failed++;
                                IsValid = false;
                            }
                            else if (_user.MobileNo.Length != 10)
                            {
                                failed++;
                                IsValid = false;
                            }

                        }
                        _user.ErrorMessage = _user.IsValid == false ? "Invalid" : null;

                        _user.IsValid = IsValid;
                    }
                    userList.Add(_user);
                }
                return userList;
            }
        }

        public static bool IsValidEmail(string email)
        {
            Regex regex = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",
             RegexOptions.CultureInvariant | RegexOptions.Singleline);

            return regex.IsMatch(email);
        }

        public static string GetFileName(string fileName)
        {
            fileName = fileName.Replace(" ", "").Replace("\\//", "");
            fileName = fileName.Replace("//\\", "");
            fileName = fileName.Replace("\r", "");
            return fileName;

        }

        public static MemoryStream CreateExcel(ReadReportDetailsModel readReportDetailsModel)
        {
            var stream = new MemoryStream();
            ExcelPackage.LicenseContext = LicenseContext.Commercial;

            using (var package = new ExcelPackage(stream))
            {
                var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                workSheet.Cells.LoadFromCollection(readReportDetailsModel.ReadReportDetails, true);
                package.Save();
            }
            stream.Position = 0;

            return stream;
        }
        public List<ExcelWhatsAppData> ReadAndProcessWhatsappExcel(string fileName, out int failed)
        {
            failed = 0;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (var excelPack = new ExcelPackage())
            {
                var fileData = new WebClient().DownloadDataTaskAsync(new Uri(AWSHelper.GetPresignedFileURL(fileName)));
                fileData.Wait();
                byte[] myfile = fileData.Result;
                Stream stream = new MemoryStream(myfile);
                excelPack.Load(stream);

                //Lets Deal with first worksheet.(You may iterate here if dealing with multiple sheets)
                var ws = excelPack.Workbook.Worksheets[0];

                List<ExcelWhatsAppData> userList = new List<ExcelWhatsAppData>();

                int colCount = ws.Dimension.End.Column;  //get Column Count
                int rowCount = ws.Dimension.End.Row;

                for (int row = 2; row <= rowCount; row++) // start from to 2 omit header
                {

                    bool IsValid = true;
                    ExcelWhatsAppData _user = new ExcelWhatsAppData();

                    for (int col = 1; col <= colCount; col++)
                    {
                        if (col == 1)
                        {
                            _user.MobileNo = ws.Cells[row, col].Value?.ToString().Trim();
                            if (string.IsNullOrEmpty(_user.MobileNo))
                            {
                                failed++;
                                IsValid = false;
                            }
                            else if (_user.MobileNo.Length != 10)
                            {
                                failed++;
                                IsValid = false;
                            }

                        }
                        else if (col == 2)
                        {
                            _user.TemplateName = ws.Cells[row, col].Value?.ToString().Trim();
                        }
                        else if (col == 3)
                        {
                            _user.Value1 = ws.Cells[row, col].Value?.ToString().Trim();
                        }
                        else if (col == 4)
                        {
                            _user.Value2 = ws.Cells[row, col].Value?.ToString().Trim();
                        }
                        else if (col == 5)
                        {
                            _user.Value3 = ws.Cells[row, col].Value?.ToString().Trim();
                        }
                        else if (col == 6)
                        {
                            _user.Value4 = ws.Cells[row, col].Value?.ToString().Trim();
                        }
                        else if (col == 7)
                        {
                            _user.Value5 = ws.Cells[row, col].Value?.ToString().Trim();
                        }
                        else if (col == 8)
                        {
                            _user.Value6 = ws.Cells[row, col].Value?.ToString().Trim();
                        }
                        else if (col == 9)
                        {
                            _user.Value7 = ws.Cells[row, col].Value?.ToString().Trim();
                        }
                        else if (col == 10)
                        {
                            _user.Value8 = ws.Cells[row, col].Value?.ToString().Trim();
                        }
                        else if (col == 11)
                        {
                            _user.Value9 = ws.Cells[row, col].Value?.ToString().Trim();
                        }
                        else if (col == 12)
                        {
                            _user.Value10 = ws.Cells[row, col].Value?.ToString().Trim();
                        }

                        _user.ErrorMessage = _user.IsValid == false ? "Invalid" : null;

                        _user.IsValid = IsValid;
                    }
                    userList.Add(_user);
                }
                return userList;
            }
        }
    }
}