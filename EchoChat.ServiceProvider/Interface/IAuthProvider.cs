﻿using EchoChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EchoChat.ServiceProvider
{
    public interface IAppProvider
    {
        string GetAppVersion(string platform);
        bool SaveAppVersion(AppVersionDTO appRequest);
    }
}
