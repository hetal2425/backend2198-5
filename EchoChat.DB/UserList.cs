﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class UserList
    {
        public int UserListId { get; set; }
        public Guid UserId { get; set; }
        public int ListId { get; set; }
        public bool? IsAdmin { get; set; }
    }
}
