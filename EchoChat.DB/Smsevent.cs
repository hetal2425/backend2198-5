﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class Smsevent
    {
        public int SmseventId { get; set; }
        public string EventType { get; set; }
        public string MsgId { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string ErrCode { get; set; }
        public string TagName { get; set; }
        public string Udf1 { get; set; }
        public string Udf2 { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
