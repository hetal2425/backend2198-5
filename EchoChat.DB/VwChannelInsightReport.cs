﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class VwChannelInsightReport
    {
        public string ChannelName { get; set; }
        public int EventId { get; set; }
        public int ClientId { get; set; }
        public int GroupId { get; set; }
        public int? ParentId { get; set; }
        public DateTime? Timestamp { get; set; }
        public string EventType { get; set; }
    }
}
