﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace EchoChat.DB
{
    public partial class EchoDBContext : DbContext
    {
        public EchoDBContext()
        {
        }

        public EchoDBContext(DbContextOptions<EchoDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AppSetting> AppSettings { get; set; }
        public virtual DbSet<AppVersion> AppVersions { get; set; }
        public virtual DbSet<Channel> Channels { get; set; }
        public virtual DbSet<ChannelDatum> ChannelData { get; set; }
        public virtual DbSet<ChannelPrediction> ChannelPredictions { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<ClientDeviceMapping> ClientDeviceMappings { get; set; }
        public virtual DbSet<ConnectedUser> ConnectedUsers { get; set; }
        public virtual DbSet<DemoRequest> DemoRequests { get; set; }
        public virtual DbSet<EmailEvent> EmailEvents { get; set; }
        public virtual DbSet<FailedNotice> FailedNotices { get; set; }
        public virtual DbSet<FailedNoticesArchive> FailedNoticesArchives { get; set; }
        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<GroupList> GroupLists { get; set; }
        public virtual DbSet<GroupUserAdmin> GroupUserAdmins { get; set; }
        public virtual DbSet<LinkedHelperDatum> LinkedHelperData { get; set; }
        public virtual DbSet<List> Lists { get; set; }
        public virtual DbSet<Log> Logs { get; set; }
        public virtual DbSet<Notice> Notices { get; set; }
        public virtual DbSet<NoticeScheduler> NoticeSchedulers { get; set; }
        public virtual DbSet<Quotum> Quota { get; set; }
        public virtual DbSet<ReceiveEmail> ReceiveEmails { get; set; }
        public virtual DbSet<ReceiveWhatsApp> ReceiveWhatsApps { get; set; }
        public virtual DbSet<Smsevent> Smsevents { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserDeviceTokenMapping> UserDeviceTokenMappings { get; set; }
        public virtual DbSet<UserList> UserLists { get; set; }
        public virtual DbSet<UserNotice> UserNotices { get; set; }
        public virtual DbSet<UserRule> UserRules { get; set; }
        public virtual DbSet<VwChannelActivityLevel> VwChannelActivityLevels { get; set; }
        public virtual DbSet<VwChannelInsightReport> VwChannelInsightReports { get; set; }
        public virtual DbSet<VwChannelReadAndDeliveryPer> VwChannelReadAndDeliveryPers { get; set; }
        public virtual DbSet<WhatsAppEvent> WhatsAppEvents { get; set; }
        public virtual DbSet<WhatsAppParameter> WhatsAppParameters { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
//                optionsBuilder.UseSqlServer("Server=DESKTOP-F288SQK;Database=EchoDB;Trusted_Connection=True;");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<AppSetting>(entity =>
            {
                entity.Property(e => e.AppSettingId).HasColumnName("AppSettingID");

                entity.Property(e => e.Details).IsUnicode(false);

                entity.Property(e => e.EntityId).HasColumnName("EntityID");

                entity.Property(e => e.EntityName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Key)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Value).IsUnicode(false);

                entity.Property(e => e.Value1).IsUnicode(false);

                entity.Property(e => e.Value2).IsUnicode(false);

                entity.Property(e => e.Value3).IsUnicode(false);

                entity.Property(e => e.Value4).IsUnicode(false);

                entity.Property(e => e.Value5).IsUnicode(false);

                entity.Property(e => e.Value6).IsUnicode(false);

                entity.Property(e => e.Value7)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("value7");
            });

            modelBuilder.Entity<AppVersion>(entity =>
            {
                entity.ToTable("AppVersion");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Date)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Platform).HasMaxLength(50);

                entity.Property(e => e.Version).HasMaxLength(50);
            });

            modelBuilder.Entity<Channel>(entity =>
            {
                entity.ToTable("Channel");

                entity.Property(e => e.ChannelId).HasColumnName("ChannelID");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ChannelDatum>(entity =>
            {
                entity.HasKey(e => e.ChannelDataId)
                    .HasName("PK__ChannelD__AEDE0188D5955761");

                entity.Property(e => e.ChannelDataId).HasColumnName("ChannelDataID");

                entity.Property(e => e.MsgId)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("MsgID");

                entity.Property(e => e.MsgType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Timestamp).HasColumnType("datetime");

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("UserID");
            });

            modelBuilder.Entity<ChannelPrediction>(entity =>
            {
                entity.ToTable("ChannelPrediction");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Fr1218)
                    .HasMaxLength(130)
                    .HasColumnName("Fr_12_18");

                entity.Property(e => e.Fr1824)
                    .HasMaxLength(130)
                    .HasColumnName("Fr_18_24");

                entity.Property(e => e.Fr246)
                    .HasMaxLength(130)
                    .HasColumnName("Fr_24_6");

                entity.Property(e => e.Fr612)
                    .HasMaxLength(130)
                    .HasColumnName("Fr_6_12");

                entity.Property(e => e.Mo1218)
                    .HasMaxLength(130)
                    .HasColumnName("Mo_12_18");

                entity.Property(e => e.Mo1824)
                    .HasMaxLength(130)
                    .HasColumnName("Mo_18_24");

                entity.Property(e => e.Mo246)
                    .HasMaxLength(130)
                    .HasColumnName("Mo_24_6");

                entity.Property(e => e.Mo612)
                    .HasMaxLength(130)
                    .HasColumnName("Mo_6_12");

                entity.Property(e => e.Sa1218)
                    .HasMaxLength(130)
                    .HasColumnName("Sa_12_18");

                entity.Property(e => e.Sa1824)
                    .HasMaxLength(130)
                    .HasColumnName("Sa_18_24");

                entity.Property(e => e.Sa246)
                    .HasMaxLength(130)
                    .HasColumnName("Sa_24_6");

                entity.Property(e => e.Sa612)
                    .HasMaxLength(130)
                    .HasColumnName("Sa_6_12");

                entity.Property(e => e.Su1218)
                    .HasMaxLength(130)
                    .HasColumnName("Su_12_18");

                entity.Property(e => e.Su1824)
                    .HasMaxLength(130)
                    .HasColumnName("Su_18_24");

                entity.Property(e => e.Su246)
                    .HasMaxLength(130)
                    .HasColumnName("Su_24_6");

                entity.Property(e => e.Su612)
                    .HasMaxLength(130)
                    .HasColumnName("Su_6_12");

                entity.Property(e => e.Th1218)
                    .HasMaxLength(130)
                    .HasColumnName("Th_12_18");

                entity.Property(e => e.Th1824)
                    .HasMaxLength(130)
                    .HasColumnName("Th_18_24");

                entity.Property(e => e.Th246)
                    .HasMaxLength(130)
                    .HasColumnName("Th_24_6");

                entity.Property(e => e.Th612)
                    .HasMaxLength(130)
                    .HasColumnName("Th_6_12");

                entity.Property(e => e.Tu1218)
                    .HasMaxLength(130)
                    .HasColumnName("Tu_12_18");

                entity.Property(e => e.Tu1824)
                    .HasMaxLength(130)
                    .HasColumnName("Tu_18_24");

                entity.Property(e => e.Tu246)
                    .HasMaxLength(130)
                    .HasColumnName("Tu_24_6");

                entity.Property(e => e.Tu612)
                    .HasMaxLength(130)
                    .HasColumnName("Tu_6_12");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.We1218)
                    .HasMaxLength(130)
                    .HasColumnName("We_12_18");

                entity.Property(e => e.We1824)
                    .HasMaxLength(130)
                    .HasColumnName("We_18_24");

                entity.Property(e => e.We246)
                    .HasMaxLength(130)
                    .HasColumnName("We_24_6");

                entity.Property(e => e.We612)
                    .HasMaxLength(130)
                    .HasColumnName("We_6_12");
            });

            modelBuilder.Entity<Client>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientLogo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ContactPersonName).HasMaxLength(100);

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsTrial)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.MobileNo).HasMaxLength(50);

                entity.Property(e => e.OrganizationName).HasMaxLength(100);
            });

            modelBuilder.Entity<ClientDeviceMapping>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ClientDeviceMapping");

                entity.Property(e => e.DeviceNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("ID");

                entity.Property(e => e.IsHostel)
                    .HasColumnName("isHostel")
                    .HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<ConnectedUser>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ConnectedUser");

                entity.Property(e => e.ConnectionId)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("ConnectionID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<DemoRequest>(entity =>
            {
                entity.HasKey(e => e.RequestId);

                entity.ToTable("DemoRequest");

                entity.Property(e => e.Date)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.EmailId)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("EmailID");

                entity.Property(e => e.MobileNumber).HasMaxLength(20);

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.Org)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PageSource).HasMaxLength(500);

                entity.Property(e => e.Time)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EmailEvent>(entity =>
            {
                entity.ToTable("EmailEvent");

                entity.Property(e => e.EmailEventId).HasColumnName("EmailEventID");

                entity.Property(e => e.EventType)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.From)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MsgId)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("MsgID");

                entity.Property(e => e.OpenByIpaddress)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("OpenByIPAddress");

                entity.Property(e => e.Timestamp).HasColumnType("datetime");

                entity.Property(e => e.To)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FailedNotice>(entity =>
            {
                entity.HasKey(e => e.Fnid)
                    .HasName("FailedNotices_pk");

                entity.Property(e => e.Fnid).HasColumnName("FNID");

                entity.Property(e => e.Channel)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Error).IsUnicode(false);

                entity.Property(e => e.ProcessedDate).HasColumnType("datetime");

                entity.Property(e => e.Unid).HasColumnName("UNID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<FailedNoticesArchive>(entity =>
            {
                entity.HasKey(e => e.Fnaid)
                    .HasName("FailedNoticesA_pk");

                entity.ToTable("FailedNoticesArchive");

                entity.Property(e => e.Fnaid).HasColumnName("FNAID");

                entity.Property(e => e.Channel)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Error).IsUnicode(false);

                entity.Property(e => e.ProcessedDate).HasColumnType("datetime");

                entity.Property(e => e.Unid).HasColumnName("UNID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<Group>(entity =>
            {
                entity.ToTable("Group");

                entity.Property(e => e.GroupId).HasColumnName("GroupID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Description)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.GroupImage)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsPrivate).HasDefaultValueSql("((0))");

                entity.Property(e => e.LastModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GroupList>(entity =>
            {
                entity.ToTable("GroupList");

                entity.Property(e => e.GroupListId).HasColumnName("GroupListID");

                entity.Property(e => e.GroupId).HasColumnName("GroupID");

                entity.Property(e => e.ListId).HasColumnName("ListID");
            });

            modelBuilder.Entity<GroupUserAdmin>(entity =>
            {
                entity.ToTable("GroupUserAdmin");

                entity.Property(e => e.GroupUserAdminId).HasColumnName("GroupUserAdminID");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.GroupId).HasColumnName("GroupID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<LinkedHelperDatum>(entity =>
            {
                entity.HasKey(e => e.LinkedHelperDataId);

                entity.Property(e => e.LinkedHelperDataId).HasColumnName("LinkedHelperDataID");

                entity.Property(e => e.Address)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("address");

                entity.Property(e => e.Birthday)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("birthday");

                entity.Property(e => e.CampaignId)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("campaign_id");

                entity.Property(e => e.CampaignName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("campaign_name");

                entity.Property(e => e.CurrentCompany)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("current_company");

                entity.Property(e => e.CurrentCompanyCustom)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("current_company_custom");

                entity.Property(e => e.CurrentCompanyCustomPosition)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("current_company_custom_position");

                entity.Property(e => e.CurrentCompanyPosition)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("current_company_position");

                entity.Property(e => e.Education1)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("education_1");

                entity.Property(e => e.EducationDegree1)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("education_degree_1");

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("email");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("first_name");

                entity.Property(e => e.FullName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("full_name");

                entity.Property(e => e.Headline)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("headline");

                entity.Property(e => e.Industry)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("industry");

                entity.Property(e => e.LastName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("last_name");

                entity.Property(e => e.MemberId)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("member_id");

                entity.Property(e => e.Messenger1)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("messenger_1");

                entity.Property(e => e.MessengerProvider1)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("messenger_provider_1");

                entity.Property(e => e.Organization1)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("organization_1");

                entity.Property(e => e.Organization2)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("organization_2");

                entity.Property(e => e.OrganizationLocation1)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("organization_location_1");

                entity.Property(e => e.OrganizationTitle1)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("organization_title_1");

                entity.Property(e => e.OrganizationTitle2)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("organization_title_2");

                entity.Property(e => e.OrganizationUrl1)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("organization_url_1");

                entity.Property(e => e.OrganizationWebsite1)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("organization_website_1");

                entity.Property(e => e.Phone1)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("phone_1");

                entity.Property(e => e.Phone2)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("phone_2");

                entity.Property(e => e.PhoneType1)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("phone_type_1");

                entity.Property(e => e.PhoneType2)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("phone_type_2");

                entity.Property(e => e.ProfileUrl)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("profile_url");

                entity.Property(e => e.ThirdPartyEmail1)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("third_party_email_1");

                entity.Property(e => e.Website1)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("website_1");
            });

            modelBuilder.Entity<List>(entity =>
            {
                entity.ToTable("List");

                entity.Property(e => e.ListId).HasColumnName("ListID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.IsPrivate).HasDefaultValueSql("((0))");

                entity.Property(e => e.LastModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Log>(entity =>
            {
                entity.Property(e => e.LogId).HasColumnName("LogID");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Details).IsUnicode(false);

                entity.Property(e => e.Module)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ModuleId).HasColumnName("ModuleID");

                entity.Property(e => e.Type)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Notice>(entity =>
            {
                entity.ToTable("Notice");

                entity.Property(e => e.NoticeId).HasColumnName("NoticeID");

                entity.Property(e => e.FileName)
                    .HasMaxLength(150)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.FileType).HasDefaultValueSql("((0))");

                entity.Property(e => e.GroupId).HasColumnName("GroupID");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsReply).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsSms).HasDefaultValueSql("((0))");

                entity.Property(e => e.NoticeDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.NoticeDetail).HasDefaultValueSql("('')");

                entity.Property(e => e.NoticeDetailHtml)
                    .HasColumnType("text")
                    .HasColumnName("NoticeDetailHTML")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.NoticeTitle).HasMaxLength(100);

                entity.Property(e => e.ParentId)
                    .HasColumnName("ParentID")
                    .HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<NoticeScheduler>(entity =>
            {
                entity.ToTable("NoticeScheduler");

                entity.Property(e => e.NoticeSchedulerId).HasColumnName("NoticeSchedulerID");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Days)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EndDate).HasColumnType("date");

                entity.Property(e => e.NoticeId).HasColumnName("NoticeID");
            });

            modelBuilder.Entity<Quotum>(entity =>
            {
                entity.HasKey(e => e.QuotaId);

                entity.Property(e => e.QuotaId).HasColumnName("QuotaID");

                entity.Property(e => e.Channel)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ReceiveEmail>(entity =>
            {
                entity.ToTable("ReceiveEmail");

                entity.Property(e => e.ReceiveEmailId).HasColumnName("ReceiveEmailID");

                entity.Property(e => e.Content).HasColumnType("text");

                entity.Property(e => e.EventType)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.From)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MsgId)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("MsgID");

                entity.Property(e => e.ReferenceMsgId)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("ReferenceMsgID");

                entity.Property(e => e.Timestamp).HasColumnType("datetime");

                entity.Property(e => e.To)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ReceiveWhatsApp>(entity =>
            {
                entity.ToTable("ReceiveWhatsApp");

                entity.Property(e => e.ReceiveWhatsAppId).HasColumnName("ReceiveWhatsAppID");

                entity.Property(e => e.From).HasMaxLength(50);

                entity.Property(e => e.MsgId)
                    .HasMaxLength(50)
                    .HasColumnName("MsgID");

                entity.Property(e => e.Timestamp).HasColumnType("datetime");

                entity.Property(e => e.Type).HasMaxLength(50);
            });

            modelBuilder.Entity<Smsevent>(entity =>
            {
                entity.ToTable("SMSEvent");

                entity.Property(e => e.SmseventId).HasColumnName("SMSEventID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.ErrCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EventType)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.From)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MsgId)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("MsgID");

                entity.Property(e => e.TagName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.To)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Udf1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Udf2)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Dob)
                    .HasColumnType("datetime")
                    .HasColumnName("DOB");

                entity.Property(e => e.EmailId)
                    .HasMaxLength(100)
                    .HasColumnName("EmailID");

                entity.Property(e => e.FacebookId)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("FacebookID")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.FirstName).HasMaxLength(100);

                entity.Property(e => e.IsIntroHappened).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsOnEcho).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsOnEchoMobileApp).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsOnEmail).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsOnPushNoti).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsOnSms)
                    .HasColumnName("IsOnSMS")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IsOnTelegram).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsOnWhatsApp).HasDefaultValueSql("((0))");

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.MobileNo).HasMaxLength(20);

                entity.Property(e => e.Occupation).HasMaxLength(50);

                entity.Property(e => e.Password).HasMaxLength(100);

                entity.Property(e => e.Session).HasMaxLength(10);

                entity.Property(e => e.WhatsAppId)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("WhatsAppID")
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<UserDeviceTokenMapping>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("UserDeviceTokenMapping");

                entity.Property(e => e.UserId)
                    .ValueGeneratedNever()
                    .HasColumnName("UserID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Custom1)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Custom2)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Custom3)
                    .IsRequired()
                    .HasMaxLength(150)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DeviceToken)
                    .IsRequired()
                    .HasMaxLength(300)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Platform)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<UserList>(entity =>
            {
                entity.ToTable("UserList");

                entity.Property(e => e.UserListId).HasColumnName("UserListID");

                entity.Property(e => e.ListId).HasColumnName("ListID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<UserNotice>(entity =>
            {
                entity.ToTable("UserNotice");

                entity.Property(e => e.UserNoticeId).HasColumnName("UserNoticeID");

                entity.Property(e => e.AppNotifyDate).HasColumnType("datetime");

                entity.Property(e => e.AppReadDate).HasColumnType("datetime");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.EmailMsgId)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("EmailMsgID");

                entity.Property(e => e.GroupId).HasColumnName("GroupID");

                entity.Property(e => e.IsNotified).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsRead).HasDefaultValueSql("((0))");

                entity.Property(e => e.NoticeId).HasColumnName("NoticeID");

                entity.Property(e => e.SmsmsgId)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("SMSMsgID");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.WamsgId)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("WAMsgID");
            });

            modelBuilder.Entity<UserRule>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Role)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwChannelActivityLevel>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwChannelActivityLevel");

                entity.Property(e => e.GroupId).HasColumnName("GroupID");
            });

            modelBuilder.Entity<VwChannelInsightReport>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwChannelInsightReport");

                entity.Property(e => e.ChannelName)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.EventId).HasColumnName("EventID");

                entity.Property(e => e.EventType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GroupId).HasColumnName("GroupID");

                entity.Property(e => e.ParentId).HasColumnName("ParentID");

                entity.Property(e => e.Timestamp).HasColumnType("datetime");
            });

            modelBuilder.Entity<VwChannelReadAndDeliveryPer>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwChannelReadAndDeliveryPer");

                entity.Property(e => e.Channel)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.DratePer).HasColumnName("DRatePer");

                entity.Property(e => e.GroupId).HasColumnName("GroupID");

                entity.Property(e => e.NoticeId).HasColumnName("NoticeID");

                entity.Property(e => e.RratePer).HasColumnName("RRatePer");
            });

            modelBuilder.Entity<WhatsAppEvent>(entity =>
            {
                entity.ToTable("WhatsAppEvent");

                entity.Property(e => e.WhatsAppEventId).HasColumnName("WhatsAppEventID");

                entity.Property(e => e.MsgId)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("MsgID");

                entity.Property(e => e.RecipientId)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("RecipientID");

                entity.Property(e => e.Status)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Timestamp).HasColumnType("datetime");
            });

            modelBuilder.Entity<WhatsAppParameter>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.MobileNo).HasMaxLength(50);

                entity.Property(e => e.TemplateName).HasMaxLength(150);

                entity.Property(e => e.Value1).HasMaxLength(500);

                entity.Property(e => e.Value10).HasMaxLength(500);

                entity.Property(e => e.Value2).HasMaxLength(500);

                entity.Property(e => e.Value3).HasMaxLength(500);

                entity.Property(e => e.Value4).HasMaxLength(500);

                entity.Property(e => e.Value5).HasMaxLength(500);

                entity.Property(e => e.Value6).HasMaxLength(500);

                entity.Property(e => e.Value7).HasMaxLength(500);

                entity.Property(e => e.Value8).HasMaxLength(500);

                entity.Property(e => e.Value9).HasMaxLength(500);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
