﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class UserGroupMapping
    {
        public int GroupMappingId { get; set; }
        public Guid UserId { get; set; }
        public int UserGroupId { get; set; }
        public bool IsAdmin { get; set; }
        public string SerialNoForGroup { get; set; }
    }
}
