﻿/****** Object:  StoredProcedure [dbo].[usp_GetGroupMappingUsers]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetGroupMappingUsers]
	@GroupID INT, @ParentID INT =0, @NoticeDetail nvarchar(max) = ''
AS
	BEGIN
		SET NOCOUNT ON;
		IF(@ParentID <> 0)
		BEGIN
			Update NoticeDetails Set LatestMessage=@NoticeDetail where NoticeID=@ParentID
		END

		SELECT UL.UserID, ISNULL(U.FirstName,'') + ' ' + ISNULL(U.LastName,'') as UserName ,Cu.ConnectionID as ConnectionID , U.EmailID , U.MobileNo, IsNull(U.FacebookID, '') FacebookID, IsNull(UDT.DeviceToken,'') AS DeviceToken
				FROM [dbo].[UserList] AS UL
				inner join [dbo].[GroupList] GL on UL.ListID = GL.ListID
				JOIN Users AS U ON UL.UserID = U.UserID
				LEFT JOIN ConnectedUser AS CU ON UL.UserID = CU.UserID
				left join [UserDeviceTokenMapping] AS UDT ON U.UserID = UDT.UserID					
				WHERE GL.GroupID = @GroupID
				AND U.IsDeleted = 0
	END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetGroupMappingUsersList]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	CREATE PROCEDURE [dbo].[usp_GetGroupMappingUsersList]
	@NoticeID INT = 0
AS
	BEGIN
	  --it's reply message 
		SET NOCOUNT ON;
		select U.UserID, U.EmailID, N.ID, U.MobileNo, IsNull(U.FacebookID, '') FacebookID, IsNull(UDT.DeviceToken,'') AS DeviceToken  from Notice as N
		JOIN Users AS U ON N.UserID = U.UserID
		left outer join [UserDeviceTokenMapping] AS UDT ON U.UserID = UDT.UserID
		where NoticeID= @NoticeID AND U.IsDeleted = 0
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetGroupMappingUsersTokens]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetGroupMappingUsersTokens]
	@GroupID INT,@NoticeID INT
AS
	BEGIN
		BEGIN

				SELECT UDT.DeviceToken
				FROM [UserDeviceTokenMapping] AS UDT
				WHERE USERID IN (select distinct UserID from Notice where NoticeID =@NoticeID
				AND GroupID = @GroupID)
				AND UDT.DeviceToken IS NOT NULL
		END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetListByClientID]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetListByClientID]
	 @ClientID int, @FreeText varchar(100) = null
AS
	BEGIN		
		Select l.ListID, l.Name
		,TotalMembers = (select count(1) from [dbo].[UserList] ul where ul.ListID = l.ListID)
		, [CreatedByUser] = (select top 1 isnull(u.firstname,'') + ' ' + isnull(u.lastname,'') from users u where u.userid = l.CreatedBy)
		,CreatedDate = CONVERT(varchar,l.CreatedDate,1) + ' '  + convert(VARCHAR(8), l.CreatedDate, 14)
		from [dbo].[List] l
		inner join [dbo].[GroupList] GL on l.ListID = GL.GroupID
		where l.ClientID = @ClientID
		AND l.IsActive = 1
		AND l.IsDeleted = 0	
		and l.Name like (case when @FreeText is null then l.Name else '%'+ @FreeText +'%' END)
		order by l.CreatedDate desc
	END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetNoticesList]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetNoticesList]
	@GroupID INT, @UserID uniqueidentifier, @PageSize INT = 2000, @PageNo INT =1, @FreeText VARCHAR(100) = null
AS
	BEGIN

	IF @PageSize = 0
	BEGIN
		SET @PageSize = 2000
	END

	IF @PageNo = 0
	BEGIN
		SET @PageNo = 1
	END

		SET NOCOUNT ON;
		select ND.NoticeID     
		,ND.CreatedBy as UserID    
		,(select top 1 concat(u.FirstName,coalesce(u.LastName,'')) from users u where u.UserID=ND.CreatedBy) as UserName          
		,NoticeTitle =ISNULL(ND.NoticeTitle,'')
		--,NoticeDetail = SUBSTRING(ISNULL(ND.NoticeDetail,''), 0, 15) + '...'
		,NoticeDetail = ISNULL(ND.NoticeDetail,'')          
		,NoticeDate = FORMAT(ND.NoticeDate,'MM/dd/yyyy hh:mm tt') 
		,LastReplyDate =(select TOP 1 nd1.NoticeDate from NoticeDetails as nd1 Where ND.NoticeID = nd1.ParentID ORDER BY nd1.NoticeDate DESC)         
		
		,[FileName] = ISNULL(ND.[FileName] ,'')
		,[FileType] = ISNULL(ND.[FileType],'')
		,ISNULL(CAST(ND.IsReply as INT),0) AS IsReply
		,LatestMessage = ISNULL(ND.LatestMessage,'')
		,FilePath = ''
		from NoticeDetails ND
		where ND.GroupID = @GroupID
		and ND.ParentID = 0
		and ND.NoticeTitle like (case when @FreeText is null then ND.NoticeTitle else '%'+ @FreeText +'%' END)
		order by LastReplyDate desc
		OFFSET @PageSize * (@PageNo-1) ROWS
		FETCH NEXT @PageSize  Rows ONLY
	END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetNoticesList_Replies]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetNoticesList_Replies]
	@ParentID INT =0, @UserID uniqueidentifier, @PageSize INT = 1000 ,@PageNo INT =1, @FreeText VARCHAR(100) = null
AS
	BEGIN


	IF @PageSize = 0
	BEGIN
		SET @PageSize = 2000
	END

	IF @PageNo = 0
	BEGIN
		SET @PageNo = 1
	END



		SET NOCOUNT ON;
		select 
		ND.NoticeID            
		,ND.GroupID            
		,ND.CreatedBy as UserID    
		,(select top 1 concat(u.FirstName,coalesce(u.LastName,'')) from users u where u.UserID = ND.CreatedBy) as UserName           
		,NoticeTitle =ISNULL(ND.NoticeTitle,'')       
		,NoticeDetail = ISNULL(ND.NoticeDetail,'')          
		,NoticeDate = FORMAT(ND.NoticeDate,'MM/dd/yyyy hh:mm tt') 
		,[FileName] = ISNULL(ND.[FileName] ,'')
		,[FileType] = ISNULL(ND.[FileType],'')       
		,ND.ParentID  
		,ISNULL(CAST(ND.IsReply as INT),0) AS IsReply
		,LatestMessage = ISNULL(ND.LatestMessage,'')
		,FilePath = ''
		from NoticeDetails ND		
		where ND.ParentID = @ParentID
		and (ND.NoticeTitle like (case when @FreeText is null then ND.NoticeTitle else '%'+ @FreeText +'%' END)
		or ND.NoticeDetail like (case when @FreeText is null then ND.NoticeDetail else '%'+ @FreeText +'%' END))
		order by ND.NoticeID 
		OFFSET @PageSize * (@PageNo-1) ROWS
		FETCH NEXT @PageSize  RoWS ONLY

	END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetPendingNotices]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- exec usp_GetPendingNotices 124330 ,'e85b0e11-025e-4798-87d9-556ce24bec60'
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetPendingNotices]
	@GroupId INT, @UserID nvarchar(50)
AS
	BEGIN
		SET NOCOUNT ON;

		select N.NoticeID            
		,N.GroupID            
		,ND.CreatedBy as UserID    
		,(select top 1 concat(u.FirstName,coalesce(u.LastName,'')) from users u where u.UserID=ND.CreatedBy) as UserName  
		,ISNULL(N.IsRead,0) AS IsRead            
		,NoticeTitle =ISNULL(ND.NoticeTitle,'')       
		,NoticeDetail = ISNULL(ND.NoticeDetail,'')          
		,NoticeDate = FORMAT(ND.NoticeDate,'dd-MM-yyyy hh:mm tt')            
		,CreatedBy = ISNULL(ND.CreatedBy,'')
		,[FileName] = ISNULL(ND.[FileName] ,'')
		,[FileType] = ISNULL(ND.[FileType],'')       
		,(select sum(CAST(IsNotified AS INT)) from notice n1 where n1.NoticeID=N.NoticeID) as DeliveryCount  
		,(select sum(CAST(IsRead AS INT)) from notice n1 where n1.NoticeID=N.NoticeID and UserID!=@UserID) as ReadCount  
		,(select COUNT(*) from NoticeDetails n1 where n1.ParentId=ND.NoticeID) as ReplyCount  
		,(select coalesce(sum(CAST((~IsRead) AS INT)),0) from Notice
			where NoticeID in (select NoticeID from NoticeDetails n1
			where n1.ParentId=ND.NoticeID) and UserID=@UserID) as ReplyReadPendingCount     
		,ND.ParentId  
		,ISNULL(CAST(ND.IsReply as INT),0) AS IsReply
		,LatestMessage = ISNULL(N.LatestMessage,'')
		from Notice N inner Join NoticeDetails ND            
		on n.NoticeID=ND.NoticeID            
		where GroupID=@GroupID and UserID=@UserID      
		and N.IsRead = 0
		order by N.NoticeID asc   
	END



GO
/****** Object:  StoredProcedure [dbo].[usp_GetScheduledNoticeList]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetScheduledNoticeList]
	@FromDate DATETIME,
	@ToDate DATETIME
AS
	BEGIN
	 SELECT nd.NoticeTitle as [Subject]
	 , '' as [Description]
	 ,'' as [Location]
	 ,CONVERT(DATETIME, CONVERT(nvarchar(20),CONVERT(DATE, ns.date)) + ' ' + CONVERT(nvarchar(20),CONVERT(TIME(0), ns.Time))) as StartTime
	 ,'' as StartTimezone
	 ,CASE WHEN ns.EndDate is null THEN CONVERT(DATETIME, CONVERT(nvarchar(20),CONVERT(DATE, ns.date)) + ' ' + CONVERT(nvarchar(20),CONVERT(TIME(0), ns.Time))) ELSE (CONVERT(DATETIME, CONVERT(nvarchar(20),CONVERT(DATE, ns.enddate)) + ' ' + CONVERT(nvarchar(20),CONVERT(TIME(0), ns.Time))))  END as EndTime	 
	 ,'' as EndTimezone
	 ,CASE when ns.Days = 'AllDay' THEN 1 ELSE 0 END as IsAllDay
	 ,CASE when ns.IsRecursive = 1 THEN 'FREQ=WEEKLY;BYDAY='+ ns.Days +';UNTIL='+ CONVERT(nvarchar(20), DATEADD(day, DATEDIFF(day, 0, CONVERT(DATETIME, ns.date)), ns.Time)) ELSE '' END AS RecurrenceRule
	 FROM [dbo].[NoticeScheduler] ns
	 INNER JOIN [dbo].[NoticeDetails] nd ON ns.NoticeID = nd.NoticeID 
	 where ns.Date between @FromDate and @ToDate
END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetUserGroups]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- exec usp_GetUserGroups 'e7ca2222-182c-4374-86e7-fb1130aee5ab'
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetUserGroups]
	 @UserID uniqueidentifier, @FreeText varchar(100) = null
AS
	BEGIN		
		Select G.GroupID,G.Name,G.Description
		,TotalMembers = 0
				,LastRead = ''
				,ReplyReadPendingCount = 0
		from [dbo].[Group] G
		inner join [dbo].[GroupList] GL on G.GroupID = GL.GroupID
		inner join [dbo].[UserList] AS UL  on GL.ListID = UL.ListID		
		where UL.UserID = @UserID
		AND G.IsActive = 1
		AND G.IsDeleted = 0
		and (G.Name like (case when @FreeText is null then G.Name else '%'+ @FreeText +'%' END)
		OR G.Description like (case when @FreeText is null then G.Description else '%'+ @FreeText +'%' END))
		order by g.CreatedDate desc
	END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetUserLogin]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetUserLogin]
	@UserID nvarchar(20), @Password varchar(100)
AS
	BEGIN
		
		SELECT UserID
		,UserName=ISNULL(Firstname,'') + ' ' + ISNULL(LastName,'')
		,EmailID = ISNULL(EmailID,'')
		,MobileNo = ISNULL(MobileNo,'')
		,ClientID
		,IsAdmin = CONVERT(bit, (CASE WHEN EXISTS (SELECT ID
                        FROM dbo.Clients
                        WHERE Clients.MobileNo = u.MobileNo and Clients.ID = u.ClientID) 
             THEN 1
             ELSE 0 END))
		,ClientName = (select TOP 1 OrganizationName from Clients where ID = u.ClientID)
		,ClientLogo = (select TOP 1 ClientLogo from Clients where ID = u.ClientID)
		,ProfilePic = ProfilePhoto
		,IsPasswordUpdated
		,ClientEmail = (select TOP 1 Email from Clients where ID = ClientID)
		from Users u
		Where MobileNo = @UserID
		AND Password = @Password
		AND IsDeleted = 0
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetUserNoticesList_App]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetUserNoticesList_App]
	@UserID nvarchar(50), @PageSize INT = 100 ,@PageNumber INT =10
AS
	BEGIN
		SET NOCOUNT ON;
		select N.NoticeID            
		,ND.GroupID            
		,ND.CreatedBy as UserID    
		,(select top 1 concat(u.FirstName,coalesce(u.LastName,'')) from users u where u.UserID=ND.CreatedBy) as UserName  
		,ISNULL(N.IsRead,0) AS IsRead            
		,NoticeTitle =ISNULL(ND.NoticeTitle,'')       
		,NoticeDetail = ISNULL(ND.NoticeDetail,'')          
		,NoticeDate = FORMAT(ND.NoticeDate,'dd-MM-yyyy hh:mm:ss tt') 
		,LastReplyDate =(select TOP 1 NoticeDate from NoticeDetails as N Where N.ParentId = ND.NoticeID ORDER BY NoticeID DESC)         
		,CreatedBy = ISNULL(ND.CreatedBy,'')
		,[FileName] = ISNULL(ND.[FileName] ,'')
		,[FileType] = ISNULL(ND.[FileType],'')       
		,(select sum(CAST(IsNotified AS INT)) from notice n1 (NOLOCK) where n1.NoticeID=N.NoticeID) as DeliveryCount  
		,(select sum(CAST(IsRead AS INT)) from notice (NOLOCK) n1 where n1.NoticeID=N.NoticeID and UserID!=@UserID) as ReadCount  
		,(select COUNT(*) from NoticeDetails (NOLOCK) n1 where n1.ParentId=ND.NoticeID) as ReplyCount  
		,(select coalesce(sum(CAST((~IsRead) AS INT)),0) from Notice (NOLOCK)
			where NoticeID in (select NoticeID from NoticeDetails n1 (NOLOCK)
			where n1.ParentId=ND.NoticeID) and UserID=@UserID) as ReplyReadPendingCount     
		,ND.ParentId  
		,ISNULL(CAST(ND.IsReply as INT),0) AS IsReply
		,LatestMessage = ISNULL(ND.LatestMessage,'')
		from Notice N (NOLOCK)
			INNER JOIN NoticeDetails ND (NOLOCK)           
		on n.NoticeID=ND.NoticeID            
		where n.UserID=@UserID      
		and ND.ParentId = 0
		order by LastReplyDate
		OFFSET @PageSize * (@PageNumber-1) ROWS
		FETCH NEXT @PageSize  RoWS ONLY
	END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetUserNoticesListReplies_App]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetUserNoticesListReplies_App]
	@UserID nvarchar(50), @PageSize INT = 100 ,@PageNumber INT =1
AS
	BEGIN
		SET NOCOUNT ON;
		select Total = count(N.NoticeID)
		from Notice N (NOLOCK)
			INNER JOIN NoticeDetails ND (NOLOCK)           
		on n.NoticeID=ND.NoticeID            
		where UserID=@UserID      
		and ND.ParentID != 0

		select N.NoticeID            
		,N.GroupID            
		,ND.CreatedBy as UserID    
		,(select top 1 concat(u.FirstName,coalesce(u.LastName,'')) from users u where u.UserID=ND.CreatedBy) as UserName  
		,ISNULL(N.IsRead,0) AS IsRead            
		,NoticeTitle =ISNULL(ND.NoticeTitle,'')       
		,NoticeDetail = ISNULL(ND.NoticeDetail,'')          
		,NoticeDate = FORMAT(ND.NoticeDate,'dd-MM-yyyy hh:mm:ss tt') 
		,CreatedBy = ISNULL(ND.CreatedBy,'')
		,[FileName] = ISNULL(ND.[FileName] ,'')
		,[FileType] = ISNULL(ND.[FileType],'')       
		,ND.ParentID  
		,ISNULL(CAST(ND.IsReply as INT),0) AS IsReply
		,LatestMessage = ISNULL(ND.LatestMessage,'')
		from Notice N (NOLOCK)
			INNER JOIN NoticeDetails ND (NOLOCK)           
		on n.NoticeID=ND.NoticeID            
		where UserID=@UserID      
		and ND.ParentID != 0
		order by N.NoticeID
		OFFSET @PageSize * (@PageNumber-1) ROWS
		FETCH NEXT @PageSize  RoWS ONLY
	END
GO
/****** Object:  StoredProcedure [dbo].[usp_InserMsgWA]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- exec usp_InserMsgWA '918655750135','8512220','Hi','2020-10-10','text'
-- =============================================
CREATE PROCEDURE [dbo].[usp_InserMsgWA]
	@from nvarchar(50),
	@MsgId nvarchar(50),
	@text nvarchar(max),
	@timestamp DateTime,
	@type nvarchar(50)
AS
BEGIN
	SET NOCOUNT ON;

	insert into WhatsappMsg([from],MsgId,[text],[timestamp],[type])
	values(@from,@MsgId,@text,@timestamp,@type)

	DECLARE @UserId uniqueIdentifier , @NoticeID INT ,@GroupID INT

	select TOP 1 @UserId = UserId from [users] where MobileNo = STUFF(@from,1,2,'')

	select TOp 1  @NoticeID = ND.NoticeID,@GroupID = GroupID from Notice as N
	JOIn NoticeDetails as ND
		ON N.NoticeID = ND.NoticeID
	WHERE ND.IsWhatsapp = 1 AND ND.ParentId = 0
	AND N.UserID = @UserId
	order by N.NoticeID desc

	select NoticeID = @NoticeID,GroupID = @GroupID,UserId=CAST(@UserId as varchar(50))

	
END




GO
/****** Object:  StoredProcedure [dbo].[usp_InsertLinkedHelperData]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- exec usp_InsertWA '8655750135','12330','read',null,'UpdateID',15224
-- =============================================
CREATE PROCEDURE [dbo].[usp_InsertLinkedHelperData]
	@member_id varchar(100)   = NULL,
@profile_url varchar(100)  = NULL,
@email varchar(100)  = NULL,
@full_name varchar(100)  = NULL,
@first_name varchar(100)  = NULL,
@last_name varchar(100)  = NULL,
@industry varchar(100)  = NULL,
@address varchar(100)  = NULL,
@birthday varchar(100)  = NULL,
@headline varchar(100)  = NULL,
@current_company varchar(100)  = NULL,
@current_company_custom varchar(100)  = NULL,
@current_company_position varchar(100)  = NULL,
@current_company_custom_position varchar(100)  = NULL,
@organization_1 varchar(100)  = NULL,
@organization_url_1 varchar(100)  = NULL,
@organization_title_1 varchar(100)  = NULL,
@organization_location_1 varchar(100)  = NULL,
@organization_website_1 varchar(100)  = NULL,
@organization_2 varchar(100)  = NULL,
@organization_title_2 varchar(100)  = NULL,
@education_1 varchar(100)  = NULL,
@education_degree_1 varchar(100)  = NULL,
@phone_1 varchar(100)  = NULL,
@phone_type_1 varchar(100)  = NULL,
@phone_2 varchar(100)  = NULL,
@phone_type_2 varchar(100)  = NULL,
@messenger_1 varchar(100)  = NULL,
@messenger_provider_1 varchar(100)  = NULL,
@website_1 varchar(100)  = NULL,
@third_party_email_1 varchar(100)  = NULL,
@campaign_id varchar(100)  = NULL,
@campaign_name varchar(100)  = NULL
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [dbo].[LinkedHelperData]
           ([member_id]
           ,[profile_url]
           ,[email]
           ,[full_name]
           ,[first_name]
           ,[last_name]
           ,[industry]
           ,[address]
           ,[birthday]
           ,[headline]
           ,[current_company]
           ,[current_company_custom]
           ,[current_company_position]
           ,[current_company_custom_position]
           ,[organization_1]
           ,[organization_url_1]
           ,[organization_title_1]
           ,[organization_location_1]
           ,[organization_website_1]
           ,[organization_2]
           ,[organization_title_2]
           ,[education_1]
           ,[education_degree_1]
           ,[phone_1]
           ,[phone_type_1]
           ,[phone_2]
           ,[phone_type_2]
           ,[messenger_1]
           ,[messenger_provider_1]
           ,[website_1]
           ,[third_party_email_1]
           ,[campaign_id]
           ,[campaign_name])
     VALUES
           (@member_id ,
@profile_url ,
@email ,
@full_name ,
@first_name ,
@last_name ,
@industry ,
@address ,
@birthday ,
@headline ,
@current_company ,
@current_company_custom ,
@current_company_position ,
@current_company_custom_position ,
@organization_1 ,
@organization_url_1 ,
@organization_title_1 ,
@organization_location_1 ,
@organization_website_1 ,
@organization_2 ,
@organization_title_2 ,
@education_1 ,
@education_degree_1 ,
@phone_1 ,
@phone_type_1 ,
@phone_2 ,
@phone_type_2 ,
@messenger_1 ,
@messenger_provider_1 ,
@website_1 ,
@third_party_email_1 ,
@campaign_id ,
@campaign_name)
	
END



GO
/****** Object:  StoredProcedure [dbo].[usp_InsertReceiveEmail]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_InsertReceiveEmail]
	@EventType varchar(20) =null
	,@MsgID varchar(100) = null
	,@ReferenceMsgID varchar(100) = null
	,@From varchar(50) = null
	,@To varchar(50) = null
	,@Timestamp [datetime] = NULL
	,@Content text = null
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [dbo].[ReceiveEmail]
           ([EventType]
           ,[MsgID]
           ,[ReferenceMsgID]
           ,[From]
           ,[To]
           ,[Timestamp]
           ,[Content])
     VALUES
           (@EventType
           ,@MsgID
           ,@ReferenceMsgID
           ,@From
           ,@To
           ,@Timestamp
		   ,@Content)





		   DECLARE @UserID uniqueIdentifier , @NoticeID INT ,@GroupID INT

	select Top 1  @UserID = UserID, @NoticeID = ND.NoticeID,@GroupID = GroupID from Notice as N
	join NoticeDetails as ND
		ON N.NoticeID = ND.NoticeID
	WHERE ND.IsEmail = 1 AND ND.ParentId = 0
	AND N.EmailMsgID = @ReferenceMsgID
	order by N.NoticeID desc

    select NoticeID = @NoticeID,GroupID = @GroupID,UserID=CAST(@UserId as varchar(50))
END



/****** Object:  StoredProcedure [dbo].[usp_InsertSESEvents]    Script Date: 05/07/2021 15:31:37 ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertSESEvents]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_InsertSESEvents]
	@EventType varchar(20) =null
	,@MsgID varchar(100) = null
	,@From varchar(50) = null
	,@To varchar(50) = null
	,@OpenByIPAddress varchar(50) = null
	,@Timestamp [datetime] = NULL
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [dbo].[EmailEvent]
           ([EventType]
           ,[MsgID]
           ,[From]
           ,[To]
           ,[OpenByIPAddress]
           ,[Timestamp])
     VALUES
           (@EventType
           ,@MsgID
           ,@From
           ,@To
           ,@OpenByIPAddress
           ,@Timestamp)
END

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertUpdateUserConnection]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_InsertUpdateUserConnection]
	@ConnectionID nvarchar(100), @UserID UniqueIdentifier = null
AS
BEGIN
	SET NOCOUNT ON;
	IF(@UserID is null)
	BEGIN
		DELETE FROM ConnectedUser WHERE ConnectionID = @ConnectionID		
	END
	ELSE
	BEGIN
		INSERT INTO ConnectedUser([UserID], ConnectionID, CreatedDate) values (@UserID, @ConnectionID, GETUTCDATE());
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertWA]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- exec usp_InsertWA '8655750135','12330','read',null,'UpdateID',15224
-- =============================================
CREATE PROCEDURE [dbo].[usp_InsertWA]
	@RecipientId varchar(20) =null
	,@MsgId varchar(100) = null
	,@status varchar(50) = null
	,@timestamp DateTime =null
	,@Type varchar(50) = 'InsertID'
	,@ID INT= NULL
AS
BEGIN
	SET NOCOUNT ON;
	IF(@Type = 'UpdateID')
		BEGIN
			UPDATE NOTICE SET WA_id = @MsgId WHERE ID = @ID
		END
	ELSE
		BEGIN
			INSERT INTO [WhatsappDetails] (RecipientId,MsgId,[status],[timestamp])
			VALUES (@RecipientId,@MsgId,@status,@timestamp)
		END
	
END




GO
/****** Object:  StoredProcedure [dbo].[usp_NoticeReport]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- exec [usp_NoticeReport] 325
-- =============================================
CREATE PROCEDURE [dbo].[usp_NoticeReport]
	@NoticeId INT
AS
BEGIN
	SET NOCOUNT ON;	

	select U.FirstName
	,UGM.isAdmin
	,CreateDate = FORMAT(N.CreateDate,'dd-MM hh:mm tt')
	,AppReadDate = FORMAT(N.AppReadDate,'dd-MM hh:mm tt')
	,MsgId = N.WA_id
	,IsNull(WaId,'')
	,IsNull(MsgId,'')
	,IsNull([Status],'')
	,[timestamp] = (case when [timestamp] is not null then FORMAT([timestamp],'dd-MM hh:mm tt') else '' end)
	 from NoticeDetails AS ND
	JOIN Notice AS N
	On ND.NoticeID = N.NoticeID
	JOIN [Users] as U
	ON U.UserID = N.UserID
	JOIN UserGroupMapping AS UGM
	ON UGM.UserID = U.UserID AND N.GroupId = UGM.UserGroupID
	left outer join WhatsappDetails wd
	on N.WA_id = wd.MsgId
	WHERE ND.NoticeID = @NoticeId 
END




GO
/****** Object:  StoredProcedure [dbo].[usp_Report_GetCampaignReportDetails]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Report_GetCampaignReportDetails]
	@GroupID INT,
	@FreeText varchar(100) = null,
	@FromDate datetime = null,
	@ToDate datetime = null
AS
	BEGIN

	IF @FromDate IS NULL
	BEGIN
		SET @FromDate = '2020-03-10 10:14:55.030';
	END

	IF @ToDate IS NULL
	BEGIN
		SET @FromDate = GETUTCDATE();
	END
	
	DECLARE @CampaignReport TABLE
	(
		NoticeID int, 
		[Message] varchar(50),
		Sender varchar(100), 
		NoticeDate varchar(20),
		FirstChannel varchar(25),
		FirstPercentage varchar(20),
		SecondChannel varchar(25),
		SecondPercentage varchar(20),
		ThirdChannel varchar(25),
		ThirdPercentage varchar(20)
	)
	
	DECLARE @ChannelPer TABLE
	(
		RowNo int,
		Channel varchar(25), 
		Per varchar(20)
	)

	DECLARE @NoticeData TABLE
	(
		NoticeID int, 
		GroupID int, 
		Channel varchar(20),
		DRatePer decimal(5,2),
		RRatePer decimal(5,2)
	)

	INSERT INTO @NoticeData SELECT * FROM vwChannelReadAndDeliveryPer vw WHERE vw.GroupID = @GroupID 

DECLARE @NoticeID int;
DECLARE @NoticeDate DATETIME;
DECLARE @CreatedBy UNIQUEIDENTIFIER;
DECLARE @Msg varchar(max);
 
-- declare cursor
  DECLARE Cur_Notice CURSOR FOR
  SELECT NoticeID, NoticeDate, CreatedBy
  FROM NoticeDetails  WHERE GroupID = @GroupID AND ParentID = 0
  AND NoticeTitle like (CASE WHEN (@FreeText is null OR @FreeText = '') THEN NoticeTitle ELSE '%' + @FreeText + '%' END)
  --AND NoticeDate between @fromDate AND @ToDate
  order by NoticeDate desc
 
-- open cursor
OPEN Cur_Notice;
 
-- loop through a cursor
FETCH NEXT FROM Cur_Notice INTO @NoticeID, @NoticeDate, @CreatedBy
	WHILE @@FETCH_STATUS = 0
    BEGIN	

		INSERT INTO @ChannelPer SELECT TOP 3 RowNo = ROW_NUMBER() OVER (ORDER BY nd.RRatePer DESC), nd.Channel, CONVERT(varchar, nd.RRatePer) + '% / ' + CONVERT(varchar, nd.DRatePer) + '%' FROM @NoticeData nd WHERE nd.NoticeID = @NoticeID ORDER BY nd.RRatePer DESC

		SET @Msg = (SELECT TOP 1 NoticeTitle FROM NoticeDetails where NoticeID = @NoticeID)

		INSERT INTO @CampaignReport
		SELECT
		@NoticeID,
		SUBSTRING(@Msg, 0, 15),
		(SELECT Top 1 IsNull(FirstName,'') + ' ' + IsNull(LastName,'') from Users WHERE UserID = @CreatedBy),
		CONVERT(varchar, @NoticeDate, 1) + ' '  + convert(VARCHAR(8), MAX(@NoticeDate), 14),
		(SELECT TOP 1 Channel From @ChannelPer WHERE RowNo = 1),
		(SELECT TOP 1 Per From @ChannelPer WHERE RowNo = 1),
		(SELECT TOP 1 Channel From @ChannelPer WHERE RowNo = 2),
		(SELECT TOP 1 Per From @ChannelPer WHERE RowNo = 2),
		(SELECT TOP 1 Channel From @ChannelPer WHERE RowNo = 3),
		(SELECT TOP 1 Per From @ChannelPer WHERE RowNo = 3)

		DELETE from @ChannelPer

		FETCH NEXT FROM Cur_Notice INTO @NoticeID, @NoticeDate, @CreatedBy;
    END;
 
-- close and deallocate cursor
CLOSE Cur_Notice;
DEALLOCATE Cur_Notice;
DELETE from @NoticeData
SELECT * FROM @CampaignReport;


END

GO
/****** Object:  StoredProcedure [dbo].[usp_Report_GetCampaignReportHeading]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Report_GetCampaignReportHeading]
	@GroupID INT
AS
	BEGIN	

	SELECT TOP 3 Channel From(

	SELECT 'EchoApp' as Channel,
	ISNULL(SUM(ISNULL(CASE WHEN n.IsRead = 1 THEN 1 ELSE 0 END, 0)),0) as ReadCount
	  FROM [Group] g	
	 INNER JOIN Notice n ON g.GroupID = n.GroupID
	 WHERE g.GroupID = @GroupID

	 UNION

	 SELECT 'SMS' as Channel,
	 COUNT(se.SMSEventID) as ReadCount
	  FROM [Group] g	 
	 INNER JOIN Notice n ON g.GroupID = n.GroupID
	 LEFT OUTER JOIN [dbo].[SMSEvent] se  ON n.SMSMsgID = se.MsgID
	 WHERE g.GroupID = @GroupID

	 UNION

	 SELECT 'Email' as Channel,
	 COUNT(ee.EmailEventID) as ReadCount
	  FROM [Group] g	 
	 INNER JOIN Notice n ON g.GroupID = n.GroupID
	 LEFT OUTER JOIN [dbo].[EmailEvent] ee  ON n.EmailMsgID = ee.MsgID
	 WHERE g.GroupID = @GroupID

	 UNION

	 SELECT 'WhatsApp' as Channel,
	 COUNT(wa.WaId) as ReadCount
	  FROM [Group] g	 
	 INNER JOIN Notice n ON g.GroupID = n.NoticeID
	 LEFT OUTER JOIN [dbo].[WhatsappDetails] wa  ON n.WAMsgID = wa.MsgID
	 WHERE g.GroupID = @GroupID) as ts
	 ORDER BY ts.ReadCount DESC	 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Report_GetGroupReportDetails]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Report_GetGroupReportDetails]
	@ClientID INT,
	@FreeText varchar(100) = null,
	@FromDate datetime = null,
	@ToDate datetime = null
AS
BEGIN

IF @FromDate IS NULL
	BEGIN
		SET @FromDate = '2020-03-10 10:14:55.030';
	END

	IF @ToDate IS NULL
	BEGIN
		SET @ToDate = GETUTCDATE();
	END

DECLARE @GroupReport TABLE
	(
		GroupID int, 
		[Name] varchar(500), 
		MembersCount int,
		CreatedDate varchar(20),
		LastMsgDate varchar(20),
		MsgCount int,
		ActivityLevel varchar(15)
	)

DECLARE @GroupID int;
DECLARE @GroupName VARCHAR(100);
DECLARE @CreatedDate DATETIME;
DECLARE @ActivityLevel VARCHAR(15);
DECLARE @MCount int;
DECLARE @DCount int;
DECLARE @RCount int;
DECLARE @Activity decimal(5,2);
 
-- declare cursor
  DECLARE Cur_Group CURSOR FOR
  SELECT top 20 GroupID, [Name], CreatedDate
  FROM [Group]  WHERE ClientID = @ClientID
  AND [Name] like (CASE WHEN (@FreeText is null OR @FreeText = '') THEN [Name] ELSE '%' + @FreeText + '%' END)
  AND CreatedDate between @fromDate AND @ToDate
  AND [Name] <> ''
  order by CreatedDate desc
 
-- open cursor
OPEN Cur_Group;
 
-- loop through a cursor
FETCH NEXT FROM Cur_Group INTO @GroupID, @GroupName, @CreatedDate
	WHILE @@FETCH_STATUS = 0
    BEGIN	

		SELECT 
		@DCount = vw.DeliveryCount, @RCount = vw.ReadCount 
		FROM dbo.vwChannelActivityLevel vw WHERE vw.GroupID = @GroupID

		SET @MCount = (SELECT COUNT(n.NoticeID) from Notice n where n.GroupID = @GroupID)

		IF @DCount > 0 AND @MCount > 0
		BEGIN

		SET @Activity = convert(decimal(5,2),(100 * convert(float,@RCount)/convert(float,@DCount)));
		
		IF @Activity > convert(decimal(5,2),10)
		BEGIN
		 SET @ActivityLevel = 'High';
		END
		ELSE IF @Activity > convert(decimal(5,2),5)
		BEGIN
		 SET @ActivityLevel = 'Medium';
		END
		ELSE
		BEGIN
		 SET @ActivityLevel = 'Low';
		END
		END
		ELSE
		BEGIN
		 SET @ActivityLevel = 'Not Specified';
		END

		INSERT INTO @GroupReport 
		SELECT 
		@GroupID
		,@GroupName
		,Isnull((select COUNT(ul.UserID) from GroupList gl INNER JOIN UserList ul ON gl.ListID = ul.ListID where gl.GroupID = @GroupID),0)
		,CONVERT(varchar,@CreatedDate,1)
		,(SELECT CASE WHEN MAX(nd.NoticeDate) is null THEN '' ELSE CONVERT(varchar,MAX(nd.NoticeDate),1) + ' '  + convert(VARCHAR(8), MAX(nd.NoticeDate), 14) END from NoticeDetails nd where nd.GroupID = @GroupID)
		,(SELECT COUNT(nd.NoticeID) from NoticeDetails nd where nd.GroupID = @GroupID)
		,@ActivityLevel

		FETCH NEXT FROM Cur_Group INTO @GroupID, @GroupName, @CreatedDate;
    END;
 
-- close and deallocate cursor
CLOSE Cur_Group;
DEALLOCATE Cur_Group;

SELECT * FROM @GroupReport;
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Report_GetGroupReportHeading]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- exec usp_GetGroupMappingUsers 124330 ,'e85b0e11-025e-4798-87d9-556ce24bec60',512,'Hi'
-- =============================================
CREATE PROCEDURE [dbo].[usp_Report_GetGroupReportHeading]
	@ClientID INT
AS
	BEGIN
	 SELECT 
		TotalGroups = (select COUNT(ClientID) from [Group] where ClientID = @ClientID AND IsDeleted = 0 AND IsActive = 1),
		TotalMembers = (select  COUNT(ClientID) from [Users] where ClientID = @ClientID AND IsDeleted = 0),
		TotalNotices = (select  COUNT(NoticeID) from NoticeDetails WHERE ParentID = 0 AND GroupID IN (select distinct GroupID from [Group] where ClientID = @ClientID))
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Report_GetReadReportDetails]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Report_GetReadReportDetails]
	@NoticeID INT,
	@FreeText varchar(100) = null
AS
	BEGIN

		SELECT 
isnull(u.FirstName,'') + ' ' + isnull(u.LastName,'') as Member
, CASE WHEN u.IsPasswordUpdated = 1 THEN ISNULL(CONVERT(varchar,n.CreateDate,1) + ' '  + convert(VARCHAR(8), n.CreateDate, 14),'') ELSE '' END EchoDate 
, CASE WHEN u.IsPasswordUpdated = 1 THEN 'Delivered' ELSE 'Not Sent' END EchoStatus

,CASE WHEN nd.IsSms = 1 THEN ISNULL((SELECT TOP 1 ISNULL(CONVERT(varchar,se.CreateDate,1) + ' '  + convert(VARCHAR(8), se.CreateDate, 14),'') FROM [dbo].[SMSEvent] se where se.MsgID = n.SMSMsgID order by se.SMSEventID desc),'') ELSE '' END SMSDate
,CASE WHEN nd.IsSms = 1 THEN ISNULL((SELECT TOP 1 CASE se.EventType  WHEN 'Delivery' THEN 'Delivered' WHEN 'Open' THEN 'Seen' WHEN 'Send' THEN 'Sent' ELSE 'Not Sent' END FROM [dbo].[SMSEvent] se where se.MsgID = n.SMSMsgID order by se.SMSEventID desc),'') ELSE 'Not Sent' END SMSStatus

,CASE WHEN nd.IsEmail = 1 THEN ISNULL((SELECT TOP 1 ISNULL(CONVERT(varchar,ee.[Timestamp],1) + ' '  + convert(VARCHAR(8), ee.[Timestamp], 14),'') FROM [dbo].[EmailEvent] ee where ee.MsgID = n.EmailMsgID order by ee.[Timestamp] desc),'') ELSE '' END EmailDate
,CASE WHEN nd.IsEmail = 1 THEN ISNULL((SELECT TOP 1 CASE ee.EventType WHEN 'Delivery' THEN 'Delivered' WHEN 'Open' THEN 'Seen' WHEN 'Send' THEN 'Sent' ELSE 'Not Sent' END FROM [dbo].[EmailEvent] ee where ee.MsgID = n.EmailMsgID order by ee.[Timestamp] desc),'') ELSE 'Not Sent' END EmailStatus

,CASE WHEN nd.IsWhatsapp = 1 THEN ISNULL((SELECT TOP 1 ISNULL(CONVERT(varchar,wa.[Timestamp],1) + ' '  + convert(VARCHAR(8), wa.[Timestamp], 14),'') FROM [dbo].[WhatsappDetails] wa where wa.MsgID = n.WAMsgID order by wa.[Timestamp] desc),'') ELSE '' END WhatsAppDate
,CASE WHEN nd.IsWhatsapp = 1 THEN ISNULL((SELECT TOP 1 CASE wa.[status]  WHEN 'delivered' THEN 'Delivered' WHEN 'read' THEN 'Seen' WHEN 'sent' THEN 'Sent' ELSE 'Not Sent' END FROM [dbo].[WhatsappDetails] wa where wa.MsgID = n.WAMsgID order by wa.[Timestamp] desc),'Not Sent') ELSE 'Not Sent' END WhatsAppStatus

	  FROM NoticeDetails nd
	 INNER JOIN Notice n ON nd.NoticeID = n.NoticeID
	INNER JOIN Users u ON n.UserID = u.UserID
	 where nd.NoticeID = @NoticeID AND nd.ParentID = 0

END
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateIsRead]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- exec usp_ManageUser 1,1,'111111',1
-- =============================================
CREATE PROCEDURE [dbo].[usp_UpdateIsRead]
	 @UserID UniqueIdentifier, @NoticeID int, @Type varchar(20)= 'Read'
AS
	BEGIN
		If(@Type = 'Read')
		BEGIN
			UPDATE Notice set IsRead = 1,AppReadDate=GETUTCDATE()
			 WHERE UserID = @UserID AND NoticeID = @NoticeID

			 UPDATE Notice set IsRead = 1, AppReadDate=GETUTCDATE()
			  WHERE NoticeID in(Select NoticeID from NoticeDetails where ParentID = @NoticeID)
		END
		ELSE If(@Type = 'Notify')
		BEGIN
			UPDATE Notice set IsNotified = 1, [AppNotifyDate] = GETUTCDATE() 
			WHERE UserID = @UserID AND NoticeID = @NoticeID

			UPDATE Notice set IsNotified = 1, [AppNotifyDate] = GETUTCDATE()
			 WHERE NoticeID in(Select NoticeID from NoticeDetails where ParentID = @NoticeID)
		END
	END
GO
/****** Object:  StoredProcedure [dbo].[usp_User_SelectByGroupID]    Script Date: 06/04/2021 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_User_SelectByGroupID]
(                          
@GroupID int,
@PageSize INT = 2000,
@PageNo INT =1,
@FreeText varchar(100) = null,
@Total int OUT
)                          
as
BEGIN

IF @PageSize = 0
	BEGIN
		SET @PageSize = 2000
	END

	IF @PageNo = 0
	BEGIN
		SET @PageNo = 1
	END

	SELECT @Total = (SELECT COUNT(1)
  FROM Users inner join  
  [dbo].[UserList] AS UL on Users.[UserID] = UL.[UserID]
  inner join [dbo].[GroupList] GL on UL.ListID = GL.ListID    
  left outer join  [dbo].[GroupUserAdmin] gua on gl.GroupID = gua.GroupID  and gua.UserID = Users.UserID                
  where GL.GroupID= @GroupID and
  (Users.FirstName = (case when @FreeText is null OR @FreeText = '' then Users.FirstName else @FreeText end) or
  Users.[EmailID] = (case when @FreeText is null OR @FreeText = '' then Users.[EmailID] else @FreeText end) or
  Users.[MobileNo]  = (case when @FreeText is null OR @FreeText = '' then Users.[MobileNo]  else @FreeText end)
  ))

SELECT Users.[UserID]                            
      ,Users.[FirstName]                              
      ,Users.[LastName]                              
	  ,DOB = CONVERT(varchar(10),USERs.DOB,103)   
      ,Users.[EmailID]                              
      ,[Password] = ''                             
      ,Users.[IsDeleted]                             
      ,Users.[CreatedBy]                    
      ,Users.[CreateDate]                            
      ,Users.[MobileNo]                    
      ,Users.[ProfilePhoto] 
	  ,Users.[Session]
	  ,Users.ClientID           
      --,(case when EXISTS(SELECT GUA.GroupUserAdminID FROM [dbo].[GroupUserAdmin] GUA WHERE GUA.UserID = Users.[UserID] and GUA.GroupID = @GroupID) then 1 else 0 end  ) as IsAdmin 
      ,case when gua.UserID is null then 0 else 1 end as IsAdmin
  FROM Users inner join  
  [dbo].[UserList] AS UL on Users.[UserID] = UL.[UserID]
  inner join [dbo].[GroupList] GL on UL.ListID = GL.ListID    
  left outer join  [dbo].[GroupUserAdmin] gua on gl.GroupID = gua.GroupID  and gua.UserID = Users.UserID                
  where GL.GroupID= @GroupID and
  (Users.FirstName = (case when @FreeText is null OR @FreeText = '' then Users.FirstName else @FreeText end) or
  Users.[EmailID] = (case when @FreeText is null OR @FreeText = '' then Users.[EmailID] else @FreeText end) or
  Users.[MobileNo]  = (case when @FreeText is null OR @FreeText = '' then Users.[MobileNo]  else @FreeText end)
  )
  order by Users.FirstName
  OFFSET @PageSize * (@PageNo-1) ROWS
  FETCH NEXT @PageSize  Rows ONLY

END
GO