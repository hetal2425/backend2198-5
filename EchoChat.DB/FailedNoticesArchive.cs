﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class FailedNoticesArchive
    {
        public int? Unid { get; set; }
        public string Channel { get; set; }
        public string Error { get; set; }
        public bool? IsProcessed { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ProcessedDate { get; set; }
        public Guid? UserId { get; set; }
        public int? ClientId { get; set; }
        public int Fnaid { get; set; }
    }
}
